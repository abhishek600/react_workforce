import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import styles from './style'

class Button extends React.Component {

    render() {
        const { text, onPress } = this.props;
        return (
            <TouchableOpacity style={styles.button} onPress={onPress}>
                <Text style={styles.text}>{text}</Text>
            </TouchableOpacity>
        )
    }
}

export default Button