import { StyleSheet } from 'react-native'
import { Colors } from '@common'

export default StyleSheet.create({
    button: {
        marginLeft: 10,
        marginRight: 10,
        height: 50,
        borderRadius: 5,
        backgroundColor: Colors.appColor,
        alignItems: 'center',
        justifyContent: 'center'
    },

    text: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 15
    }
})