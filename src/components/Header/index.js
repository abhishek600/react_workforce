import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Colors, Icons } from '@common'
import QRCode from 'react-native-qrcode-generator'

class Header extends React.Component {

  static defaultProps = {
    isQrCodeData: true
  }

  render() {
    const { backIcon, goBack, sName, qrCodeData, isQrCodeData, qrCodePress, onChatPress } = this.props;
    return (
      <View style={{ height: 50, backgroundColor: Colors.appColor }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginRight: 10, height: 50, justifyContent: 'space-between' }}>
          {backIcon ?
            <TouchableOpacity style={{ width: 30, alignItems: 'center' }} onPress={goBack}>
              <Image source={Icons.leftArrow} style={{ height: 20, width: 20, resizeMode: 'contain' }} />
            </TouchableOpacity>
            :
            <View />
          }

          <Text style={{ color: 'white', fontWeight: 'bold' }}>{sName}</Text>
          
          {isQrCodeData ?
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity style={{ height: 30, width: 30, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }} onPress={qrCodePress}>
                <QRCode
                  value={qrCodeData}
                  size={25}
                  bgColor='white'
                  fgColor='black' />
              </TouchableOpacity>

              <View style={{ width: 15 }} />

              <TouchableOpacity onPress={onChatPress}>
                <Image source={Icons.whiteHeadset} style={{ height: 25, width: 25, resizeMode: 'contain' }} />
              </TouchableOpacity>
            </View>
            :
            <View />
          }

        </View>
      </View>
    )
  }
  
}

export default Header