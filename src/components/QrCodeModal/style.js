import { StyleSheet } from 'react-native'


export default StyleSheet.create({

  modalBackground: {
    flex: 1,
    alignItems: 'center',
    //flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: 'transparent'
  },

  modal: {
    backgroundColor: 'white',
    height: 360,
    width: 320,
    borderRadius: 10,
    borderWidth: 1,
    elevation: 1,
    borderColor: '#fff',

  },
  
})