import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import Modal from 'react-native-modal'
import styles from './style'
import QRCode from 'react-native-qrcode-generator'

class QrCodeModal extends React.Component {

    state = {
        isVisible: true
    }


    render() {
        let { qrCodeData, qrModalVisible, backPress } = this.props;

        return (
            <View style={{ height: qrModalVisible ? '100%' : undefined, width: qrModalVisible ? '100%' : undefined, backgroundColor: qrModalVisible ? 'rgba(0,0,0,0.5)' : 'transparent' }}>
                <Modal
                    animationType={"fade"}
                    transparent={true}
                    visible={qrModalVisible}
                    onBackdropPress={() => this.props.backPress()}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>

                    <TouchableOpacity style={styles.modalBackground} onPress={backPress}>
                        <View style={[styles.modal, { alignItems: 'center', justifyContent: 'center' }]}>
                            <QRCode
                                value={qrCodeData}
                                size={220}
                                bgColor='black'
                                fgColor='white' />
                        </View>
                    </TouchableOpacity>

                </Modal>
            </View>
        )
    }
}

export default QrCodeModal