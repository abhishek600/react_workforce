import React from 'react'
import {
  View,
  Modal,
  ActivityIndicator
} from 'react-native'
import styles from './style'
import { Colors } from '@common'


class Loader extends React.Component {

  render() {
    let { loading } = this.props
    return (
      <Modal
        transparent={true}
        animationType={'none'}
        visible={loading}
        onRequestClose={() => { console.log('close modal') }}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator
              animating={loading}
              color={Colors.appColor}
            />
          </View>
        </View>
      </Modal>
    )
  }

}

export default Loader
