import {StyleSheet} from 'react-native'


export default StyleSheet.create({
  
    container: {
        height: '100%',
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%',
        zIndex: 10
      }
})