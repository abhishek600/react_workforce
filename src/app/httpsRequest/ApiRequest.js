import { Config } from '@common';
import { Platform } from 'react-native';

const createFormData = (body) => {
    let formdata = new FormData();
  
    Object.keys(body).forEach((key) => {
      formdata.append(key, body[key]);
    });
  
    console.log('data', formdata);
    return formdata;
  };

class ApiRequest {

    static httpsGet(action, params) {
        let url = Config.url + action + params;
        let url2 = Config.devUrl + action + params;

        console.log('httpsGetUrl', url);

        return fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            })
            .catch((error) => {
                console.log(error);
            });
    }

    static httpsGetMaster(action, params) {
        let url = Config.url2 + action + params;
        let url2 = Config.devUrl2 + action + params;

        console.log('httpsGetMasterUrl', url);

        return fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            })
            .catch((error) => {
                console.log(error);
            });
    }

    static httpsPost(action, params, token) {
        let url = Config.url + action + params;
        let url2 = Config.devUrl + action + params;
        let authUrl = Config.authUrl + token;

        console.log('httpPostUrl', authUrl);

        let options = {
            method: 'POST',
            headers: {
                'content-type' : 'multipart/form-data'
            },
            body: createFormData(params)
        };

        return fetch(authUrl, options)
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            })
            .catch((error) => {
                console.log(error);
            });

    }   

    static httpsPostMaster(action, params, token) {
        let url = Config.url2 + action + params;
        let url2 = Config.devUrl2 + action + params;
        let authUrl = Config.authUrl + token;

        console.log('httpPostMasterUrl', authUrl);

        let options = {
            method: 'POST',
            headers: {
                'content-type' : 'multipart/form-data'
            },
            body: createFormData(params)
        };

        return fetch(authUrl, options)
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            })
            .catch((error) => {
                console.log(error);
            });
    }

    static getAuthorizeToken() {
        let url = Config.tokenUrl;
       
        console.log('url', url);

        let formdata = new FormData();
        formdata.append('client_id', 'Townlabour');
        formdata.append('client_secret', '26a8625ff70b8ee44e60a8e0bcbe0844')
        formdata.append('grant_type', 'client_credentials')

        let options = {
            method: 'POST',
            body: formdata
        };

        return fetch(url, options)
            .then(response => response.json())
            .then((responseJson) => {
                return responseJson;
            })
            .catch(error => console.log(error))
    }

    static getTestToken() {
        let testUrl = Config.testTokenUrl;
        console.log('url', testUrl);

        let formdata = new FormData();
        formdata.append('client_id', 'Townlabour');
        formdata.append('client_secret', '26a8625ff70b8ee44e60a8e0bcbe0844')
        formdata.append('grant_type', 'client_credentials')

        let options = {
            method: 'POST',
            body: formdata
        };

        return fetch(testUrl, options)
            .then(response => response.json())
            .then((responseJson) => {
                return responseJson;
            })
            .catch(error => console.log(error))
    }

}

export default ApiRequest