import React from 'react'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'
import {persistStore} from 'redux-persist'
import {PersistGate} from 'redux-persist/es/integration/react'
import rootReducer from '../reducers/reducers'
import Location from './Location';


const middleware = [thunkMiddleware]
const store = compose(applyMiddleware(...middleware))(createStore)(rootReducer)

let persistor = persistStore(store)

class Root extends React.Component {

    render() {
        return (
            <Provider store={store}>
              <Location />   
            </Provider>
        )
    }
}

export default Root