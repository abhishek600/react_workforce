import React from 'react'
import { Platform } from 'react-native'
import { Config } from '@common'
import AsyncStorage from '@react-native-community/async-storage'
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import App from './App';

Geocoder.init(Config.google_api_key);
export default class Location extends React.Component {

  componentDidMount() {
    if (Platform.OS == 'android') {
      this.getLocation();
    } else {
      this.getLatLng();
    }
  }

  getLocation = () => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
      .then((data) => {
        console.log('locationData', data);
        this.getLatLng();

      }).catch((err) => {
        console.log('locationErr', err);
      });
  }

  getLatLng = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        this.getLocationDetails(position.coords.latitude, position.coords.longitude)

      },
      (error) => { console.log(error); },
      { enableHighAccuracy: false, timeout: 5000 }
    )
  }

  getLocationDetails = (latitude, longitude) => {

    Geocoder.from(latitude, longitude)
      .then(json => {
        var location = json;
        console.log('location ', JSON.stringify(location.results[0]))
        location.results[0].address_components.forEach(component => {
          if (component.types.indexOf('country') !== -1) {
            console.log('country', component.short_name);
            AsyncStorage.setItem('country_code', component.short_name);
          }
        })

      })
      .catch(error => console.warn(error));
  }

  render() {
    return (
      <App />
    )
  }
}