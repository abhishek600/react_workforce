import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import { Constants } from '@common'
import Splash from './Screens/Splash'
import Login from './Screens/Login'
import ForgotPassword from './Screens/ForgotPassword';
import HomeScreen from './Screens/HomeScreen';
import HistoryDetail from './Screens/HistoryDetail';
import StartTask from './Screens/StartTask';
import EndTask from './Screens/EndTask';
import MapActivity from './Screens/MapActivity';
import StartAssetInspection from './Screens/StartAssetInspection';
import StopAssetInspection from './Screens/StopAssetInspection';
import AdminChat from './Screens/AdminChat';
import Chat from './Screens/chat';
import ViewRoutes from './Screens/ViewRoutes';
import AcceptRejectTask from './Screens/AcceptRejectTask';
import ScanQRCode from './Screens/ScanQRCode';
import CheckSpeed from './Screens/CheckSpeed';
import SignUp from './Screens/SignUp';
import SelectCountry from './Screens/SelectCountry';
import SelectCity from './Screens/SelectCity';
import Vendors from './Screens/Vendors';
import SelectCategory from './Screens/SelectCategory';
import ShowMessage from './Screens/ShowMessage';
import UploadDocument from './Screens/UploadDocument';
import OtherCategory from './Screens/OtherCategory';
import BillScreen from './Screens/BillScreen';
import {CallScreen} from './Screens/CallScreen';
import MedicalForm from './Screens/MedicalForm';

const stackConfiguration = {

}


const screens = {}
screens[Constants.Screens.Splash] = { screen: Splash }
screens[Constants.Screens.Login]  = { screen: Login }
screens[Constants.Screens.ForgotPassword] = { screen: ForgotPassword }
screens[Constants.Screens.HomeScreen] = { screen: HomeScreen }
screens[Constants.Screens.HistoryDetail] = { screen: HistoryDetail }
screens[Constants.Screens.StartTask] = { screen: StartTask }
screens[Constants.Screens.EndTask] = { screen: EndTask }
screens[Constants.Screens.MapActivity] = { screen: MapActivity }
screens[Constants.Screens.StartAssetInspection] = { screen: StartAssetInspection }
screens[Constants.Screens.StopAssetInspection] = { screen: StopAssetInspection }
screens[Constants.Screens.AdminChat] = { screen: AdminChat }
screens[Constants.Screens.Chat] = { screen: Chat }
screens[Constants.Screens.ViewRoutes] = { screen: ViewRoutes }
screens[Constants.Screens.AcceptRejectTask] = { screen: AcceptRejectTask }
screens[Constants.Screens.ScanQRCode] = { screen: ScanQRCode }
screens[Constants.Screens.CheckSpeed] = { screen: CheckSpeed }
screens[Constants.Screens.SignUp] = { screen: SignUp }
screens[Constants.Screens.SelectCountry] = { screen: SelectCountry }
screens[Constants.Screens.SelectCity] = { screen: SelectCity }
screens[Constants.Screens.Vendors] = { screen: Vendors }
screens[Constants.Screens.SelectCategory] = { screen: SelectCategory }
screens[Constants.Screens.ShowMessage] = { screen: ShowMessage }
screens[Constants.Screens.UploadDocument] = { screen: UploadDocument }
screens[Constants.Screens.OtherCategory] = { screen: OtherCategory }
screens[Constants.Screens.BillScreen] = { screen: BillScreen }
screens[Constants.Screens.CallScreen] = { screen: CallScreen }
screens[Constants.Screens.MedicalForm] = { screen: MedicalForm }

const AppNavigator = createStackNavigator({
    ...screens,
 },
 {
    headerMode:'none',
    initialRouteName: 'Splash',
    navigationOptions: {
              gesturesEnabled: false,
              headerVisible: false,
    },
 });

 export default createAppContainer(AppNavigator);