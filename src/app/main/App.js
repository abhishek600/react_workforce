import React from 'react';
import {LogBox} from 'react-native'
import AppNavigator from './Router';

//LogBox.ignoreAllLogs(true)
console.disableYellowBox = true;

class App extends React.Component {
  
  render() {
    return (
        <AppNavigator />
    )
  }

}

export default App;