import React, { Component } from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { Colors, DataModel, Icons, Messages } from '@common';
import { Header, Loader, Button } from '@components';
import { connect } from 'react-redux';

class ShowMessage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            successMsg: 'Thanks for reaching out to us and showing your interest to become a vendor with us. We will reach out to you in 5 business days to discuss further.'
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    sName={''}
                    backIcon={true}
                    isQrCodeData={false}
                    goBack={() => this.props.navigation.goBack(null)}
                />

                <View style={{ flex: 0.95, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>{this.state.successMsg}</Text>
                </View>

                 <View style={{ marginLeft: 20, marginRight: 20 }}>
                    <Button
                        text={'Go to Home'}
                        onPress={() => this.props.navigation.navigate('HomeScreen')}
                    />
                </View>

            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(ShowMessage);
