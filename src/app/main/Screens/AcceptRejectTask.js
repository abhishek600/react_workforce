import React from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, TouchableOpacity, Text, Image, View, TextInput, Alert, ScrollView, Modal, FlatList } from 'react-native'
import { Icons, Colors, Messages, DataModel } from '@common'
import { Button, Loader, Header } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { ApiRequest } from '@httpsRequest'

let dataModel;
class AcceptRejectTask extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            params: this.props.navigation.getParam('params'),
            society: '',
            name: '',
            mobile: '',
            address: '',
            pickupAddress: '',
            dropoffAddress: '',
            timeSlot: ''
        };
        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let society = await AsyncStorage.getItem('isSocietyUser');
        let params = this.state.params;

        if (society) {
            this.setState({ society: society });
        }

        if (params) {
            this.assignID = params.assignID;
            this.getOrderInfo(this.assignID);
        }

    }

    getOrderInfo = (assignId) => {
        let society = this.state.society;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        this.setState({ loading: true });

        let params = { "action": "getOrderinfo", "assignID": assignId, "societyid": society };
        //let params = "&assignID=" + assignId + "&societyid=" + society + "&access_token=" + testToken;
        ApiRequest.httpsPost("getOrderinfo", params, authToken).then((responseJson) => {
            console.log("response", responseJson);
            if (responseJson.status === 200) {
                let res = responseJson.result;
                this.setState({
                    loading: false,
                    name: res.full_name,
                    mobile: res.mobile,
                    address: res.address,
                    pickupAddress: res.pickup_address_location,
                    dropoffAddress: res.dropoff_address_location,
                    timeSlot: res.timeSlot
                });
            } else {
                this.setState({ loading: false });
                Alert.alert(responseJson.message);
            }

        })
    }

    acceptOrRejectJob = (is_accept) => {
        let society = this.state.society;
        let assignId = this.assignID;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        this.setState({ loading: true });
        let params = { "action": "acceptOrdertask", "assignID": assignId, "is_accept": is_accept, "societyid": society };
        //let params = "&assignID=" + assignId + "&is_accept=" + is_accept + "&societyid=" + society + "&access_token=" + testToken;
        ApiRequest.httpsPost("acceptOrdertask", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ loading: false });
                this.props.navigation.navigate('HomeScreen');
            } else {
                this.setState({ loading: false });
                Alert.alert(responseJson.message);
            }
        })

    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader
                    loading={this.state.loading} />

                <Header
                    sName={'New Task'}
                    backIcon={true}
                    isQrCodeData={false}
                    goBack={() => this.props.navigation.goBack(null)}
                />

                <View style={{ height: '80%', justifyContent: 'center', marginLeft: 10, marginRight: 10 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>Name :</Text>
                        <Text style={{ marginLeft: 5 }}>{this.state.name}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                        <Text>Phone no :</Text>
                        <Text style={{ marginLeft: 5 }}>{this.state.mobile}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                        <Text>Date & Time :</Text>
                        <Text style={{ marginLeft: 5 }}>{this.state.timeSlot}</Text>
                    </View>

                    {this.state.address != '' ?
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text>Address :</Text>
                            <Text style={{ marginLeft: 5 }}>{this.state.address}</Text>
                        </View>
                        :
                        null
                    }

                    {this.state.pickupAddress != '' ?
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text>Pickup Address :</Text>
                            <Text style={{ marginLeft: 5 }}>{this.state.pickupAddress}</Text>
                        </View>
                        :
                        null
                    }

                    {this.state.dropoffAddress != '' ?
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Text>Dropoff Address :</Text>
                            <Text style={{ marginLeft: 5 }}>{this.state.dropoffAddress}</Text>
                        </View>
                        :
                        null
                    }

                </View>

                <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity style={{ height: 50, flex: 0.48, backgroundColor: Colors.appColor, borderRadius: 6, overflow: 'hidden', alignItems: 'center', justifyContent: 'center' }} onPress={() => this.acceptOrRejectJob("1")}>
                        <Text style={{ color: 'white' }}>Accept</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ height: 50, flex: 0.48, backgroundColor: Colors.appColor, borderRadius: 6, overflow: 'hidden', alignItems: 'center', justifyContent: 'center' }} onPress={() => this.acceptOrRejectJob("2")}>
                        <Text style={{ color: 'white' }}>Reject</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }

}


const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});


export default connect(mapStateToProps, mapDispatchToProps)(AcceptRejectTask);