import React, { Component } from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, Image, StyleSheet, TextInput, Platform, Alert, FlatList, Keyboard } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Colors, Icons, DataModel } from '@common';
import { connect } from 'react-redux';
import { KeyboardShift } from '@components';
import moment from 'moment';
import { isIphoneX } from './is-iphone-x';
import { ApiRequest } from '@httpsRequest';

let dataModel;
class AdminChat extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            userData: [],
            labourId: '',
            societyId: '',
        };
        this.data = [];
        this.message = '';
        this.intervalID;

        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getData();
    }

    componentWillUnmount() {
        clearTimeout(this.intervalID);
    }

    getData = async () => {
        let society = await AsyncStorage.getItem('isSocietyUser')
        let loginData = await AsyncStorage.getItem('loginData')
        let societyId = ""

        if (society == "1") {
            societyId = await AsyncStorage.getItem('societyID');
        } else {
            societyId = "0";
        }

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({
                    labourId: this.state.userData[0].id,
                    societyId: societyId
                }, () => { this.getMessages(true); })
            });
        }
    }

    clearInputValue = () => {
        this.textInput.clear();
    }

    getMessages = (value) => {
        let societyId = this.state.societyId;
        let labourId = this.state.labourId;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        this.setState({ loading: value });

        let params = { "action": "GetlabourChatMessageSociety", "society_id": societyId, "labourid": labourId };
        //let params = "&society_id=" + societyId + "&labourid=" + labourId + "&access_token=" + testToken;
        ApiRequest.httpsPostMaster("GetlabourChatMessageSociety", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ loading: false });
                this.data = responseJson.result.reverse();
                this.intervalID = setTimeout(() => { this.getMessages(false); }, 2000);
            } else {
                this.setState({ loading: false });
                //Alert.alert(responseJson.message);
            }
        })

    }

    sendMessages = () => {
        Keyboard.dismiss();
        let societyId = this.state.societyId;
        let labourId = this.state.labourId;
        let message = this.message;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        if (message == '') {
            Alert.alert('Please type a message');
            return;
        }

        let params = { "action": "LabourchatRequestsociety", "society_id": societyId, "labourid": labourId, "message": message };
        //let params = "&society_id=" + societyId + "&labourid=" + labourId + "&message=" + message + "&access_token=" + testToken;
        ApiRequest.httpsPostMaster("LabourchatRequestsociety", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ loading: false });
                this.clearInputValue();
                this.message = "";
            } else {
                this.setState({ loading: false });
                //Alert.alert(responseJson.message);
            }
        })

    }

    getTime = (time) => {
        return moment(time).format('D MMM YYYY, h:mm a');
    }

    renderItem = ({ item, index }) => {
        let date = this.getTime(item.date);
        return (

            <View style={{ flex: 1, padding: 10, marginLeft: 10, marginRight: 10, flexDirection: item.sender != 0 ? 'row' : 'row-reverse', alignItems: 'center' }}>
                {item.sender != 0 ?
                    <View style={{ flexDirection: 'row', alignItems: 'center', flex: 0.7 }}>
                        <View style={{ paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomRightRadius: 10, overflow: 'hidden', backgroundColor: Colors.appColor, borderColor: Colors.black, borderWidth: 0.5 }}>
                            <Text style={{ color: 'white', textAlign: 'left', fontSize: 15 }}>{item.message}</Text>
                            <Text style={{ color: 'white', fontSize: 12, textAlign: 'left' }}>{date}</Text>
                        </View>
                    </View>
                    :
                    <View style={{ flexDirection: 'row-reverse', alignItems: 'center', flex: 0.7 }}>
                        <View style={{ paddingTop: 5, paddingBottom: 5, paddingLeft: 10, paddingRight: 10, borderTopRightRadius: 10, borderBottomLeftRadius: 10, borderTopLeftRadius: 10, overflow: 'hidden', backgroundColor: Colors.appColor }}>
                            <Text style={{ textAlign: 'right', color: 'white', fontSize: 15 }}>{item.message}</Text>
                            <Text style={{ color: 'white', fontSize: 12, textAlign: 'right' }}>{date}</Text>
                        </View>
                    </View>
                }
            </View>

        );
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                <KeyboardShift>
                    {() => (
                        <SafeAreaView>
                            
                            <View style={{ height: '89%' }}>
                                {this.data.length ?
                                    <FlatList
                                        ref={ref => this.flatList = ref}
                                        onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                                        onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                                        data={this.data}
                                        renderItem={this.renderItem}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                    :
                                    <View style={{ height: '89%', alignItems: 'center', justifyContent: 'center' }}>
                                      <Text>No Chat Available</Text>
                                    </View>
                                }
                            </View>

                            <View style={[styles.input, { flexDirection: 'row', marginLeft: 10, marginRight: 10, alignSelf: 'center', backgroundColor: 'white' }]}>
                                <TextInput
                                    style={{ flex: 1, padding: 10, textAlign: 'left' }}
                                    placeholder={'Type a message'}
                                    ref={input => { this.textInput = input }}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor={Colors.darkGray}
                                    onChangeText={(message) => this.message = message}
                                />

                                <TouchableOpacity style={{ height: 40, width: 40, marginLeft: 10, borderRadius: 20, backgroundColor: Colors.white, borderColor: Colors.appColor, borderWidth: 2, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.sendMessages()}>
                                    <Image source={Icons.send} style={{ height: 20, width: 20, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                            </View>

                        </SafeAreaView>
                    )}
                </KeyboardShift>
            </View>

        )
    }

}

const styles = StyleSheet.create({
    input: {
        width: '94%',
        marginTop: 10,
        marginBottom: 0,
        height: 40,
        flexDirection: 'row',
        textAlign: 'left',
        alignItems: 'center',
        borderRadius: 20,
        borderColor: Colors.darkGray,
        borderWidth: 1,
        backgroundColor: 'white',
        ...Platform.select({
            ios: {
                marginBottom: isIphoneX() ? 40 : 20
            }
        })
    },

});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(AdminChat);