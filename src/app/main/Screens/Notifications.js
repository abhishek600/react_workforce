import React from 'react'
import { SafeAreaView, StyleSheet, Text, Image, View, FlatList, Platform } from 'react-native'
import { Icons, Colors, DataModel } from '@common'
import { Loader } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import moment from 'moment'
import { ApiRequest } from '@httpsRequest'

let dataModel;
class Notifications extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            isNotificationData: false,
            country: '',
            labourId: '',
            userData: [],
            notificationData: [],
            data: [{
                id: 1,
                message: 'Notification 1',
                date: '11/12/2020',
            },
            {
                id: 2,
                message: 'Notification 2',
                date: '01/05/2020',
            },
            {
                id: 3,
                message: 'Notification 3',
                date: '21/10/2020',
            },
            {
                id: 4,
                message: 'Notification 4',
                date: '02/02/2020',
            },
            {
                id: 5,
                message: 'Notification 5',
                date: '26/02/2020',
            }],
        };
        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let loginData = await AsyncStorage.getItem('loginData')

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({
                    country: this.state.userData[0].country_id,
                    labourId: this.state.userData[0].id
                }, () => { this.getNotifications(); })
            });
        }
    }

    getNotifications = () => {
        let country = this.state.country;
        let labourId = this.state.labourId;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        this.setState({ loading: true });

        let params = { "action": "get_labour_notification", "country": country, "labour_id": labourId };
        //let params = "&country=" + country + "&labour_id=" + labourId + "&access_token=" + testToken;
        ApiRequest.httpsPostMaster("get_labour_notification", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ loading: false, isNotificationData: false, notificationData: responseJson.result });
            } else {
                this.setState({ loading: false, isNotificationData: false });
                //Alert.alert(responseJson.message);
            }
        })

    }

    getTime = (time) => {
        return moment(time).format('D MMM YYYY, h:mm a');
    }

    renderItem = ({ item, index }) => {
        let date = this.getTime(item.created_at);
        return (
            <View style={styles.item}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={Icons.forgotIcon} style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                    <View style={{ width: '10%' }} />
                    <Text>{item.message}</Text>
                </View>

                <View style={{ position: 'absolute', right: 5, bottom: 5 }}>
                    <Text style={{ color: Colors.darkGray, fontSize: 12 }}>{date}</Text>
                </View>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.background, marginBottom: Platform.OS == 'android' ? 50 : 0 }}>
                <Loader
                    loading={this.state.loading} />

                <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
                    <View style={{ flex: 1, marginTop: 10, backgroundColor: 'white', borderTopRightRadius: 15, borderTopLeftRadius: 15 }}>
                        {Array.isArray(this.state.notificationData) && this.state.notificationData.length ?
                            <FlatList
                                data={this.state.notificationData}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => index.toString()}
                            />
                            :
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: Colors.appColor, fontSize: 16 }}>No Notification</Text>
                            </View>
                        }
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    item: {

        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 1,
        elevation: 1,
        backgroundColor: '#FFFFFF',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 1,
        marginTop: 10,
        marginBottom: 5,
        padding: 10,
    },

    separator: {
        height: 10
    },
});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(Notifications);