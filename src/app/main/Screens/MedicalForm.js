import React, { Component } from 'react'
import { SafeAreaView, StyleSheet, Text, Image, View, FlatList, Platform, TouchableOpacity, Alert, TextInput } from 'react-native'
import { Icons, Colors, DataModel } from '@common'
import { Loader, Header } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { ApiRequest } from '@httpsRequest'
import Icon from 'react-native-vector-icons/Ionicons'
import DateTimePickerModal from "react-native-modal-datetime-picker"

let dataModel;
class MedicalForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            order_id: this.props.navigation.getParam('order_id'),
            selectedDate: '',
            isDatePickerVisible: false,
            workForceId: '',
            society_id: '',
            data: [
                {
                    'id': 1,
                    'question': 'Name',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 2,
                    'question': 'Medical Record Number',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 3,
                    'question': 'Date',
                    'field': 'date',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 4,
                    'question': 'Type of Visit',
                    'field': 'radio',
                    'answer': '',
                    'options': [
                        {
                            'id': 1,
                            'value': 'In-Person',
                            'is_selected': false
                        },
                        {
                            'id': 2,
                            'value': 'Telemedicine',
                            'is_selected': false
                        }
                    ]
                },
                {
                    'id': 5,
                    'question': 'Age',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 6,
                    'question': 'Gender',
                    'field': 'radio',
                    'answer': '',
                    'options': [
                        {
                            'id': 1,
                            'value': 'Female',
                            'is_selected': false
                        },
                        {
                            'id': 2,
                            'value': 'Male',
                            'is_selected': false
                        },
                        {
                            'id': 3,
                            'value': 'Prefer not to say',
                            'is_selected': false
                        },
                        {
                            'id': 4,
                            'value': 'Other',
                            'is_selected': false
                        }
                    ]
                },
                {
                    'id': 7,
                    'question': 'Symptoms',
                    'field': 'checkbox',
                    'answer': '',
                    'options': [
                        {
                            'id': 1,
                            'value': 'Cough',
                            'is_selected': false
                        },
                        {
                            'id': 2,
                            'value': 'Fever(feeling)',
                            'is_selected': false
                        },
                        {
                            'id': 3,
                            'value': 'Shortness of Breath',
                            'is_selected': false
                        },
                        {
                            'id': 4,
                            'value': 'Chills',
                            'is_selected': false
                        },
                        {
                            'id': 5,
                            'value': 'Diarrhea',
                            'is_selected': false
                        },
                        {
                            'id': 6,
                            'value': 'Nausea',
                            'is_selected': false
                        },
                        {
                            'id': 7,
                            'value': 'Vomiting',
                            'is_selected': false

                        },
                        {
                            'id': 8,
                            'value': 'Loss of taste',
                            'is_selected': false
                        },
                        {
                            'id': 9,
                            'value': 'Loss of smell',
                            'is_selected': false
                        },
                        {
                            'id': 10,
                            'value': 'Muscle Aches',
                            'is_selected': false
                        },
                        {
                            'id': 11,
                            'value': 'Fatigue',
                            'is_selected': false
                        },
                        {
                            'id': 12,
                            'value': 'Contact with anyone who is COVID-19 positive over the past 14 days',
                            'is_selected': false
                        },
                        {
                            'id': 13,
                            'value': 'Other',
                            'is_selected': false
                        }
                    ]
                },
                {
                    'id': 8,
                    'question': 'Temperature',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 9,
                    'question': 'Heart Rate',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 10,
                    'question': 'Systolic Blood Pressure',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 11,
                    'question': 'Diastolic Blood Pressure',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 12,
                    'question': 'Respiratory Rate',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 13,
                    'question': 'Oxygen Saturation',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 14,
                    'question': 'White Blood Cell Count',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 15,
                    'question': 'Lymphocyte Count',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 16,
                    'question': 'Hemoglobin',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 17,
                    'question': 'Hematocrit',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 18,
                    'question': 'Platelets',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 19,
                    'question': 'D-dimer',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 20,
                    'question': 'Erythrocyte Sedimentation Rate',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 21,
                    'question': 'Troponin I',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 22,
                    'question': 'Sodium',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 23,
                    'question': 'Potassium',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 24,
                    'question': 'BUN',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 25,
                    'question': 'Creatinine',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 26,
                    'question': 'Glucose',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 27,
                    'question': 'CO2',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 28,
                    'question': 'Chloride',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 29,
                    'question': 'AST',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 30,
                    'question': 'ALT',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 31,
                    'question': 'Albumin',
                    'field': 'text',
                    'answer': '',
                    'options': []
                },
                {
                    'id': 32,
                    'question': 'Medical History - Co-morbidities',
                    'field': 'checkbox',
                    'answer': '',
                    'options': [
                        {
                            'id': 1,
                            'value': 'Hypertension',
                            'is_selected': false
                        },
                        {
                            'id': 2,
                            'value': 'Diabetes',
                            'is_selected': false
                        },
                        {
                            'id': 3,
                            'value': 'Chronic Lung Disease',
                            'is_selected': false
                        },
                        {
                            'id': 4,
                            'value': 'Chronic Kidney Disease',
                            'is_selected': false
                        },
                        {
                            'id': 5,
                            'value': 'Immunosuppression(HIV, Transplant, Recipient, Active Cancer Treatment)',
                            'is_selected': false
                        },
                        {
                            'id': 6,
                            'value': 'Liver Disease',
                            'is_selected': false
                        },
                        {
                            'id': 7,
                            'value': 'Obesity/Overweight',
                            'is_selected': false
                        },
                        {
                            'id': 8,
                            'value': 'Other',
                            'is_selected': false
                        }
                    ]
                },
                {
                    'id': 33,
                    'question': 'Assessment',
                    'field': 'radio',
                    'answer': '',
                    'options': [
                        {
                            'id': 1,
                            'value': 'Mild COVID-19 Disease',
                            'is_selected': false
                        },
                        {
                            'id': 2,
                            'value': 'Moderate COVID-19 Disease',
                            'is_selected': false
                        },
                        {
                            'id': 3,
                            'value': 'Severe COVID-19 Disease',
                            'is_selected': false
                        }
                    ]
                },
                {
                    'id': 34,
                    'question': 'Plan - Diagnostics',
                    'field': 'checkbox',
                    'answer': '',
                    'options': [
                        {
                            'id': 1,
                            'value': 'CBC with differential count',
                            'is_selected': false
                        },
                        {
                            'id': 2,
                            'value': 'CMP',
                            'is_selected': false
                        },
                        {
                            'id': 3,
                            'value': 'ESR',
                            'is_selected': false
                        },
                        {
                            'id': 4,
                            'value': 'CR-P',
                            'is_selected': false
                        },
                        {
                            'id': 5,
                            'value': 'D-Dimer',
                            'is_selected': false
                        },
                        {
                            'id': 6,
                            'value': 'Troponin I',
                            'is_selected': false
                        },
                        {
                            'id': 7,
                            'value': 'PT-INR',
                            'is_selected': false
                        },
                        {
                            'id': 8,
                            'value': 'APTT',
                            'is_selected': false
                        },
                        {
                            'id': 9,
                            'value': 'Other',
                            'is_selected': false
                        }
                    ]
                },
                {
                    'id': 35,
                    'question': 'Plan - Treatment',
                    'field': 'checkbox',
                    'answer': '',
                    'options': [
                        {
                            'id': 1,
                            'value': 'Prone Positioning',
                            'is_selected': false
                        },
                        {
                            'id': 2,
                            'value': 'Supplemental Oxygen',
                            'is_selected': false
                        },
                        {
                            'id': 3,
                            'value': 'Injection Enoxaparin - Prophylactic dose 40 mg',
                            'is_selected': false
                        },
                        {
                            'id': 4,
                            'value': 'Injection Enoxaparin - Therapeutic dose 1mg/kg',
                            'is_selected': false
                        },
                        {
                            'id': 5,
                            'value': 'Inhaler Budesonide 2 puffs twice dialy',
                            'is_selected': false
                        },
                        {
                            'id': 6,
                            'value': 'Inhaler Duo-Lin: Levosalbutamol 25mcg + Ipratroprium 50 mcg 2 puffs as needed every 4 hours',
                            'is_selected': false
                        },
                        {
                            'id': 7,
                            'value': 'Tablet Dexamethasone 6mg - 1 tablet daily for 5 days',
                            'is_selected': false
                        },
                        {
                            'id': 8,
                            'value': 'IV Remdesivir 200 mg loading dose Day 1, 100 mg daily dose Day 2 - 5',
                            'is_selected': false
                        },
                        {
                            'id': 9,
                            'value': 'IV Fluids',
                            'is_selected': false
                        },
                        {
                            'id': 10,
                            'value': 'Tablet Paracetamol 500 mg - take 1 tablet every 6 hours as needed for fever or muscle aches',
                            'is_selected': false
                        },
                        {
                            'id': 11,
                            'value': 'Tablet Paracetamol 500 mg - take 2 tablets (1000 mg dose)every 8 hours',
                            'is_selected': false
                        },
                        {
                            'id': 12,
                            'value': 'Tablet Naproxen 500 mg - take 1 tablet twice daily for fever and muscle aches',
                            'is_selected': false
                        }
                    ]
                },
                {
                    'id': 36,
                    'question': 'Monitoring Plan',
                    'field': 'radio',
                    'answer': '',
                    'options': [
                        {
                            'id': 1,
                            'value': 'Doctor Visit Daily',
                            'is_selected': false
                        },
                        {
                            'id': 2,
                            'value': 'Doctor Visit every 3 days',
                            'is_selected': false
                        },
                        {
                            'id': 3,
                            'value': 'Health Worker Visit Daily',
                            'is_selected': false
                        },
                        {
                            'id': 4,
                            'value': 'Health Worker Visit Twice Daily',
                            'is_selected': false
                        }
                    ]
                }
            ],
        };

        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let loginData = await AsyncStorage.getItem('loginData');

        if (loginData) {
            let l = JSON.parse(loginData);
            this.setState({ workForceId: l[0].id, society_id: l[0].societyId });
        }
    }

    toggleIndex = (index, f) => {
        let d = this.state.data.slice(0);

        for (let j = 0; j < d[index].options.length; j++) {
            d[index].options[j].is_selected = false;
        }

        d[index].answer = d[index].options[f].value;
        d[index].options[f].is_selected = !d[index].options[f].is_selected;

        console.log('data => ', JSON.stringify(d));
        this.setState({ data: d });
    }

    onMessageType = (index, message) => {
        //Keyboard.dismiss();
        let msg = message;
        let data = this.state.data.slice(0);
        data[index].answer = msg;
        this.setState({ data: data });
    }

    submitForm = () => {
        let opt = this.state.data;
    
        for (let i = 0; i < opt.length; i++) {
            let options = opt[i].options;

            if (options == []) {
                opt[i].options = [];
            } else {
                let values = '';
                let x = '';

                opt[i].options.map((v, j) => {

                    if (j == opt[i].options.length - 1) {
                        x = '';
                    } else {
                        x = ',';
                    }

                    values = values + v.value + x;
                });

                opt[i].options = Array.of(values);

            }
        }

        let workforceId = this.state.workForceId;
        let orderId = this.state.order_id;
        let society_id = this.state.society_id;
        let answerSubmission = opt;
        let dataLength = this.state.data.length;
        let answers = answerSubmission.filter((v, i) => v.answer);
        let answersLength = answers.length;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        /* if (dataLength != answersLength) {
            Alert.alert('Please select all Answers');
            return;
        } */

        this.setState({ loading: true });

        let params = { 'WorkForceId': workforceId, 'OrderId': orderId, 'societyId': society_id, 'answersubmission': JSON.stringify(answerSubmission), 'action': 'submit_medical_visit' };
        ApiRequest.httpsPost('submit_medical_visit', params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ loading: false }, () => {
                    this.props.navigation.navigate('HomeScreen');
                });
            } else {
                this.setState({ loading: false });
            }
        });
    }

    showDatePicker = () => {
        this.setState({ isDatePickerVisible: true });
    }

    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false });
    }

    handleConfirm = (date) => {
        let p = this.state.data.slice(0);
        let day = date.getDate() < 10 ? "0" + (date.getDate()) : date.getDate();
        let month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        let year = date.getFullYear();

        let d = day + '/' + month + '/' + year;

        p[this.state.index].answer = day + '/' + month + '/' + year;
        this.setState({ selectedDate: d, data: p, isDatePickerVisible: false });
        console.log('picked date => ', d);
    }

    renderItem = ({ item, index }) => {
        return (
            <View key={index} style={{ marginLeft: 10, marginRight: 10, marginTop: 10, padding: 20, borderRadius: 10, backgroundColor: 'white', shadowOffset: { width: 0, height: 0.5 }, shadowOpacity: 0.6, shadowRadius: 1, elevation: 1 }}>
                <Text>{item.question}</Text>
                <View style={{ marginTop: 10 }} />

                {item.field == 'text' ?
                    <TextInput
                        style={{ flex: 1, borderBottomColor: '#808080', borderBottomWidth: 1, paddingBottom: 10, marginTop: 10 }}
                        underlineColorAndroid={'transparent'}
                        placeholder={'Your answer'}
                        placeholderTextColor={Colors.darkGray}
                        multiline={true}
                        onChangeText={(msg) => this.onMessageType(index, msg)}
                    //onSubmitEditing={() => this.onMessageType(i)}
                    />
                    :
                    (item.field == 'date' ?
                        <TouchableOpacity key={index} style={{ flex: 1, borderBottomColor: '#808080', borderBottomWidth: 1, paddingBottom: 10, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }} onPress={() => { this.setState({ index: index }); this.showDatePicker() }}>
                            <Text>{this.state.selectedDate != '' ? this.state.selectedDate : 'dd/mm/yyyy'}</Text>
                            <Icon style={{ color: 'black' }} size={20} name={'calendar'} />
                        </TouchableOpacity>
                        :
                        <View key={index}>
                            {item.options.map((e, f) => (
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    {item.field == 'radio' ?
                                        <TouchableOpacity style={{ height: 20, width: 20, borderRadius: 10, borderColor: '#808080', borderWidth: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.toggleIndex(index, f)}>
                                            {this.state.data[index].options.length && this.state.data[index].options[f].is_selected ?
                                                <View style={{ height: 10, width: 10, borderRadius: 5, backgroundColor: Colors.appColor }} />
                                                :
                                                null
                                            }

                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity style={{ height: 20, width: 20, borderRadius: 5, borderColor: this.state.data[index].options[f].is_selected ? Colors.appColor : '#808080', backgroundColor: this.state.data[index].options[f].is_selected ? Colors.appColor : 'transparent', borderWidth: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.toggleIndex(index, f)}>
                                            {this.state.data[index].options.length && this.state.data[index].options[f].is_selected ?
                                                <Image source={Icons.tick} style={{ height: 14, width: 14, resizeMode: 'contain' }} />
                                                :
                                                null
                                            }

                                        </TouchableOpacity>
                                    }

                                    <Text style={{ marginLeft: 15 }}>{e.value}</Text>
                                </View>
                            ))
                            }
                        </View>
                    )
                }

            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader
                    loading={this.state.loading}
                />

                <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
                    <Header
                        sName={'Medical Visit Form'}
                        isQrCodeData={false}
                        backIcon={true}
                        goBack={() => this.props.navigation.goBack(null)}
                    />

                    <View style={{ flex: 1, backgroundColor: 'white', marginTop: 10, borderTopRightRadius: 15, borderTopLeftRadius: 15 }}>
                        <FlatList
                            data={this.state.data}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={this.renderItem}
                        />

                        <View style={{ height: 40 }} />
                    </View>

                    <TouchableOpacity style={{ position: 'absolute', width: '100%', height: 40, bottom: 0, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.submitForm()}>
                        <Text style={{ color: 'white', fontWeight: 'bold' }}>Submit</Text>
                    </TouchableOpacity>
                </View>

                <DateTimePickerModal
                    isVisible={this.state.isDatePickerVisible}
                    mode="date"
                    onConfirm={(date) => this.handleConfirm(date)}
                    onCancel={() => this.hideDatePicker()}
                />
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(MedicalForm);