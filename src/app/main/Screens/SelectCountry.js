import React, { Component } from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, FlatList, Image, Alert } from 'react-native';
import { connect } from 'react-redux';
import { Colors, Messages, Icons } from '@common';
import { Header } from '@components';

class SelectCountry extends Component {
    constructor(props) {
        super(props);

        this.state = {
            countries: [{
                id: 1,
                country: 'India',
                is_selected: false
            },
            {
                id: 2,
                country: 'Dubai',
                is_selected: false
            },
            /* {
                /* id: 3,
                country: 'Somaliland',
                is_selected: false */
           // } */,
            /* {
                /* id: 4,
                country: 'Nigeria',
                is_selected: false */
           // }, */
            {
                id: 5,
                country: 'Ghana',
                is_selected: false
            },
            /* {
                id: 6,
                country: 'South Africa',
                is_selected: false
            }, */
            /* {
                id: 7,
                country: 'Malaysia',
                is_selected: false
            }, */
            {
                id: 8,
                country: 'Kenya',
                is_selected: false
            },
            {
                id: 9,
                country: 'Rwanda',
                is_selected: false
            },
            /* {
                id: 10,
                country: 'Botswana',
                is_selected: false
            }, */
            /* {
                id: 11,
                country: 'Cameroon',
                is_selected: false
            }, */
            /* {
                id: 12,
                country: 'Zambia',
                is_selected: false
            }, */
            {
                id: 13,
                country: 'Mexico',
                is_selected: false
            },
            /* {
                id: 14,
                country: 'Uganda',
                is_selected: false
            } */],
        };
    }

    toggleIndex = (index) => {
        let data = this.state.countries.slice(0);

        for (i = 0; i < data.length; i++) {
            data[i].is_selected = false;
        }

        data[index].is_selected = !data[index].is_selected;
        this.setState({ countries: data });
    }

    onContinuePress = () => {
        let selectedCountry = this.state.countries.filter((v, i) => v.is_selected);
        let country_code = "";

        if (!selectedCountry.length) {
            Alert.alert('', "Please select country");
            return;
        }

        console.log('countryData', selectedCountry);

        if (selectedCountry[0].id == 1) {
            country_code = "IN"
        } else if (selectedCountry[0].id == 2) {
            country_code = "AE"
        } else if (selectedCountry[0].id == 3) {
            country_code = "SO"
        } else if (selectedCountry[0].id == 4) {
            country_code = "NG"
        } else if (selectedCountry[0].id == 5) {
            country_code = "GH"
        } else if (selectedCountry[0].id == 6) {
            country_code = "ZA"
        } else if (selectedCountry[0].id == 7) {
            country_code = "MY"
        } else if (selectedCountry[0].id == 8) {
            country_code = "KE"
        } else if (selectedCountry[0].id == 9) {
            country_code = "RW"
        } else if (selectedCountry[0].id == 10) {
            country_code = "BW"
        } else if (selectedCountry[0].id == 11) {
            country_code = "CM"
        } else if (selectedCountry[0].id == 12) {
            country_code = "ZM"
        } else if (selectedCountry[0].id == 13) {
            country_code = "MX"
        } else if (selectedCountry[0].id == 14) {
            country_code = "UG"
        } else {

        }

        //AsyncStorage.setItem('countryData', JSON.stringify(selectedCountry));
        this.props.navigation.navigate('SelectCity', {country_code: country_code});
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={{ padding: 10, flexDirection: 'row', borderBottomColor: '#F2F2F2', borderBottomWidth: 2, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => this.toggleIndex(index)}>
                <Text style={{ fontSize: 15 }}>{item.country}</Text>
                <TouchableOpacity style={{ height: 20, width: 20, backgroundColor: this.state.countries[index].is_selected ? Colors.appColor : 'transparent', alignItems: 'center', justifyContent: 'center', borderColor: 'black', borderWidth: 1 }} onPress={() => this.toggleIndex(index)}>
                    {this.state.countries[index].is_selected ?
                        <Image source={Icons.tick} style={{ height: 16, width: 16, resizeMode: 'contain' }} />
                        :
                        null
                    }

                </TouchableOpacity>
                {/* <View style={{ width: 1, backgroundColor: '#F2F2F2'}} /> */}
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View>
                    <Text style={{ marginTop: 20, textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'black' }}>{'Select Country'}</Text>
                    <View style={{ height: '80%', marginLeft: 10, marginRight: 10, marginTop: 20, marginBottom: 10, borderWidth: 1, borderColor: Colors.gray, backgroundColor: 'white' }}>
                        <FlatList
                            data={this.state.countries}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <TouchableOpacity style={{ height: 50, width: 140, marginTop: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 5, alignSelf: 'center', backgroundColor: Colors.appColor }} onPress={() => this.onContinuePress()}>
                        <Text style={{ fontSize: 15, color: 'white' }}>{'Continue'}</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(SelectCountry);
