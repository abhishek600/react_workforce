import React, { Component } from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, TouchableOpacity, Text, Image, View, FlatList, Platform } from 'react-native'
import { Icons, Colors, DataModel } from '@common'
import { Loader, Header } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { ScrollView } from 'react-native-gesture-handler'

class BillScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            taskPrice: this.props.navigation.getParam('taskPrice'),
            currency: '',
        };
    }

    componentDidMount() {
        this.getCurrency();
    }

    getCurrency = async () => {
        let currency = await AsyncStorage.getItem('currency');

        if (currency) {
            this.setState({ currency: currency });
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
                    <Header
                        sName={'Bill'}
                        isQrCodeData={false}
                        backIcon={false}
                    />

                    <View style={{ flex: 1, marginTop: 10, backgroundColor: 'white', borderTopRightRadius: 15, borderTopLeftRadius: 15 }}>
                        <ScrollView>
                            <View style={{ height: 400, alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={Icons.bill} style={{ height: 300, width: 300, }} />
                            </View>

                            <View style={{ marginTop: 10, marginLeft: 20, marginRight: 20, borderStyle: 'dashed', borderWidth: 1, borderColor: '#dddddd', borderRadius: 1 }} />
                            <View style={{ marginLeft: 24, marginTop: 5, marginRight: 24, flexDirection: 'row' }}>
                                <Text style={{ fontWeight: 'bold', width: '86%' }}>Total Bill</Text>
                                <Text style={{ fontWeight: 'bold' }}>{this.state.currency} {this.state.taskPrice}</Text>
                            </View>
                            <View style={{ marginTop: 10, marginLeft: 20, marginRight: 20, borderStyle: 'dashed', borderWidth: 1, borderColor: '#dddddd', borderRadius: 1 }} />

                        </ScrollView>
                        <TouchableOpacity style={{ width: '100%', height: 50, backgroundColor: Colors.appColor, position: 'absolute', bottom: 0, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onPayPress()}>
                            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 17 }}>CONTINUE</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(BillScreen);