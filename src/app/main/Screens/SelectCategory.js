import React, { Component } from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, Image, FlatList, Alert } from 'react-native';
import { Colors, DataModel, Icons, Messages } from '@common';
import { Header, Loader, Button } from '@components';
import { connect } from 'react-redux';
import { ApiRequest } from '@httpsRequest';

class SelectCategory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            cityId: this.props.navigation.getParam('cityId'),
            categoryList: [
                {
                    id: 0,
                    name: 'Electrician',
                    is_selected: false,

                },
                {
                    id: 1,
                    name: 'Plumber',
                    is_selected: false,
                },
                {
                    id: 2,
                    name: 'Painter',
                    is_selected: false,
                },
                {
                    id: 3,
                    name: 'Carpenter',
                    is_selected: false,
                }, 
                {
                    id: 4,
                    name: 'Beautician',
                    is_selected: false,
                }, 
                {
                    id: 5,
                    name: 'Other',
                    is_selected: false,
                }],
            errorMsg: 'No vendors are Available in your Selected City.',
            successMsg: '',
        };
        dataModel = DataModel.getInstance();
    }

    toggleIndex = (index) => {
        let data = this.state.categoryList.slice(0);

        for (let i = 0; i < data.length; i++) {
            data[i].is_selected = false;
        }

        data[index].is_selected = !data[index].is_selected;

        this.setState({ categoryList: data });
    }

    onContinuePress = () => {
        let data = this.state.categoryList.filter((v, i) => v.is_selected);

        if(!data.length) {
           Alert.alert('Please select Category');
           return;
        }

        if (data[0].name == "Other") {
            this.props.navigation.navigate('OtherCategory', { cityId: this.state.cityId });
            return;
        }

        this.props.navigation.navigate('Vendors', { cityId: this.state.cityId })
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={[styles.shadow, { alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: 10, backgroundColor: 'white', borderRadius: 15, overflow: 'hidden', padding: 10, flexDirection: 'row', justifyContent: 'space-between' }]} onPress={() => this.toggleIndex(index)}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={Icons.vendor} style={{ height: 50, width: 50 }} />
                    <View style={{ marginLeft: 15, width: '65%' }}>
                        <Text>{item.name}</Text>
                    </View>
                </View>

                <View style={{ height: 22, width: 22, borderRadius: 11, alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.categoryList[index].is_selected ? Colors.appColor : '#dddddd' }}>
                    <Image source={Icons.tick} style={{ height: 18, width: 18, resizeMode: 'contain' }} />
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    sName={''}
                    backIcon={true}
                    isQrCodeData={false}
                    goBack={() => this.props.navigation.goBack(null)}
                />

                <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 10 }}>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>{this.state.categoryList.length ? 'Select Your Category' : ''}</Text>
                </View>

                {this.state.categoryList.length ?
                    <FlatList
                        data={this.state.categoryList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this.renderItem}
                    />
                    :
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 15 }}>{this.state.errorMsg}</Text>
                    </View>
                }

                <Button
                    text={'Continue'}
                    onPress={() => this.onContinuePress()}
                />

            </SafeAreaView>
        )
    }

}

const styles = {
    shadow: {
        shadowColor: 'rgba(0, 0, 0, 0.45)',
        shadowOffset: { width: 0, height: 1.0 },
        shadowRadius: 3,
        shadowOpacity: 0.8,
        borderColor: '#dddddd',
        borderWidth: 3,
        elevation: 1
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(SelectCategory);