import React from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, TouchableOpacity, Text, Image, View, TextInput, Alert, FlatList, Platform } from 'react-native'
import { Icons, Colors, Messages, DataModel } from '@common'
import { Loader } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import Modal from 'react-native-modal'
import moment from 'moment'
import ToggleSwitch from 'toggle-switch-react-native'
import { ApiRequest } from '@httpsRequest'
import { Dropdown } from 'react-native-material-dropdown';
import Icon from 'react-native-vector-icons/Ionicons';
import SinchVoip, { CallManager, hasPermissions, SinchVoipEvents } from 'react-native-sinch-voip';


const APPLICATION_KEY = '7d8bcecd-b67d-457a-8674-4834b8e1e50f';
const APPLICATION_SECRET = 'eQn2lA9MU0eTpmOg+I6DcA==';
const HOST = 'clientapi.sinch.com'; // clientapi.sinch.com for production

let dataModel;
class OpenTask extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      token: '',
      society: '',
      country: '',
      labourId: '',
      page_start: 0,
      userData: [],
      currency: '',
      taskData: [],
      isAmountModalVisible: false,
      filterModalVisible: false,
      extraQuotation: '',
      communites: [],
      username: '',
      covid: '',
    };
    this.arrayholder = [];
    dataModel = DataModel.getInstance()
  }

  componentDidMount() {
    this.getData();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (this.props.state.updateTasksFlag != nextProps.state.updateTasksFlag) {
      this.setState({ updateTasksFlag: nextProps.state.updateTasksFlag }, () => { this.getOpenTask() });
    }
  }

  initialize = () => {
    let userId = this.state.labourId + "_" + this.state.username;
    console.log('sinch initialize with userId => ', userId);
    SinchVoip.initClient(
      APPLICATION_KEY,
      APPLICATION_SECRET,
      HOST,
      userId,
      'TownConnect'
    )
  }

  getData = async () => {
    let token = await AsyncStorage.getItem('fcmWorkToken')
    let society = await AsyncStorage.getItem('isSocietyUser')
    let loginData = await AsyncStorage.getItem('loginData')

    if (token) {
      this.setState({ token: token });
    }

    if (society) {
      this.setState({ society: society });
    }

    if (loginData) {
      this.setState({ userData: JSON.parse(loginData) }, () => {
        this.setState({
          country: this.state.userData[0].country_id,
          labourId: this.state.userData[0].id,
          currency: this.state.userData[0].currency,
          username: this.state.userData[0].username,
          covid: this.state.userData[0].covid
        }, () => { this.getOpenTask(), this.initialize() })
      });
    }
  }

  getOpenTask = () => {
    let society = this.state.society;
    let country = this.state.country;
    let labourId = this.state.labourId;
    let pageStart = this.state.page_start;
    let token = this.state.token;
    let authToken = dataModel.getAuthToken();
    let testToken = dataModel.getTestToken();

    this.setState({ loading: true });

    let params = { "action": "inprocessTask", "country": country, "labourId": labourId, "society": society, "page_start": pageStart, "deviceId": token };
    //let params = "&country=" + country + "&labourId=" + labourId + "&society=" + society + "&page_start=" + pageStart + "&deviceId=" + token + "&access_token=" + testToken;
    ApiRequest.httpsPost("inprocessTask", params, authToken).then((responseJson) => {
      console.log('response', responseJson);
      if (responseJson.status === 200) {
        let d = [];
        let f = [];

        if (responseJson.communites.length) {
          for (let p in responseJson.communites) {
            d.push({
              societyid: responseJson.communites[p].societyid,
              societyName: responseJson.communites[p].societyName,
              isSelected: false
            })
          }
        }

        responseJson.result.map((v, i) => {
          let x = {};

          x.assignId = v.assignId;
          x.user_id = v.user_id;
          x.username = v.username;
          x.schedule_datetime = v.schedule_datetime;
          x.execute_datetime = v.execute_datetime;
          x.address = v.address;
          x.mobile = v.mobile;
          x.city = v.city;
          x.services = v.services;
          x.discPromo = v.discPromo;
          x.order_id = v.order_id;
          x.taskPrice = v.taskPrice;
          x.StartStopStatus = v.StartStopStatus;
          x.updated_price = v.updated_price;
          x.payment_status = v.payment_status;
          x.startTime = v.startTime;
          x.ios = v.ios;
          x.latitude = v.latitude;
          x.longitude = v.longitude;
          x.android = v.android;
          x.is_fleet = v.is_fleet;
          x.pick_lat = v.pick_lat;
          x.pick_lng = v.pick_lng;
          x.drop_lat = v.drop_lat;
          x.drop_lng = v.drop_lng;
          x.pickup_name = v.pickup_name;
          x.dropoff_name = v.dropoff_name;
          x.isUrgent = v.isUrgent;
          x.approvestatus = v.approvestatus;
          x.inventoryName = v.inventoryName;
          x.inventoryquantity = v.inventoryquantity;
          x.pickup_address_location = v.pickup_address_location;
          x.dropoff_address_location = v.dropoff_address_location;
          x.inventorylist = v.inventorylist;
          x.community = v.community;
          x.clientName = v.clientName;
          x.clientBranch = v.clientBranch;
          x.clientDepartment = v.clientDepartment;
          x.serviceType = v.serviceType;
          x.showMoreInfo = false;

          f.push(x);

          if (i == responseJson.result.length - 1) {
            this.setState({
              loading: false,
              taskData: f,
              communites: d
            });

            this.arrayholder = f;

          }

        });



      } else {
        this.setState({ loading: false });
        //Alert.alert(responseJson.message);
      }
    })

  }

  onQuotationContinuePress = (order_id) => {
    let country = this.state.country;
    let labourId = this.state.labourId;
    let amount = this.state.extraQuotation;
    let authToken = dataModel.getAuthToken();
    let testToken = dataModel.getTestToken();

    this.setState({ loading: true });

    let params = { "action": "add_extra_amount_inorder", "country": country, "labourid": labourId, "orderid": order_id, "amount": amount };
    //let params = "&country=" + country + "&labourid=" + labourId + "&orderid=" + order_id + "&amount=" + amount + "&access_token=" + testToken;
    ApiRequest.httpsPostMaster("add_extra_amount_inorder", params, authToken).then((responseJson) => {
      console.log('response', responseJson);
      if (responseJson.status === 200) {
        this.setState({ loading: false });
        this.getOpenTask();
      } else {
        this.setState({ loading: false });
        Alert.alert(responseJson.message);
      }
    })

  }

  acceptOrRejectJob = (assignId, is_accept) => {
    let society = this.state.society;
    let authToken = dataModel.getAuthToken();
    let testToken = dataModel.getTestToken();

    this.setState({ loading: true });

    let params = { "action": "acceptOrdertask", "assignID": assignId, "is_accept": is_accept, "societyid": society };
    //let params = "&assignID=" + assignId + "&is_accept=" + is_accept + "&societyid=" + society + "&access_token=" + testToken;
    ApiRequest.httpsPost("acceptOrdertask", params, authToken).then((responseJson) => {
      console.log('response', responseJson);
      if (responseJson.status === 200) {
        this.setState({ loading: false });
        this.getOpenTask();
      } else {
        this.setState({ loading: false });
        Alert.alert(responseJson.message);
      }
    })

  }

  onListPressed = (item) => {
    let serviceType = '';
    let pick_lat = '';
    let pick_lng = '';
    let drop_lat = '';
    let drop_lng = '';

    if (item.serviceType == '3') {
      serviceType = item.serviceType;

      if (item.pick_lat != "" && item.pick_lng != "") {
        pick_lat = item.pick_lat;
        pick_lng = item.pick_lng;
      }

      if (item.drop_lat != "" && item.drop_lng != "") {
        drop_lat = item.drop_lat;
        drop_lng = item.drop_lng;
      }
    }

    if (item.StartStopStatus == "0") {
      dataModel.setOrderId(item.order_id);
      this.props.navigation.navigate('StartTask', { assignId: item.assignId, is_fleet: item.is_fleet, order_id: item.order_id, covid: this.state.covid });
    } else {
      this.props.navigation.navigate('EndTask', { assignId: item.assignId, is_fleet: item.is_fleet, serviceType: serviceType, pick_lat: pick_lat, pick_lng: pick_lng, drop_lat: drop_lat, drop_lng: drop_lng, order_id: item.order_id, covid: this.state.covid });
    }

  }

  filterData(societyName) {
    const newData = this.arrayholder.filter(function (item) {
      const itemData = item.community ? item.community : '';
      const textData = societyName;
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      taskData: newData,

    });
  }

  renderAmountPopUp = () => {
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.state.isAmountModalVisible}
        onBackdropPress={() => this.setState({ isAmountModalVisible: false })}
        onRequestClose={() => { console.log("Modal has been closed.") }}
      >
        <View style={styles.modal}>
          <View style={{ height: 40, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ color: 'white' }}>Add Extra Quotation</Text>
          </View>

          <View style={{ height: 160, alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontWeight: 'bold' }}>Rs</Text>
              <View style={{ width: 5 }} />
              <TextInput
                underlineColorAndroid="transparent"
                autoFocus={true}
                placeholder={'Enter Amount'}
                placeholderTextColor={Colors.darkGray}
                onChangeText={(extraQuotation) => this.setState({ extraQuotation })}
              />
            </View>
          </View>

          <TouchableOpacity style={{ height: 40, width: '100%', position: 'absolute', bottom: 0, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onQuotationContinuePress(this.state.order_id)}>
            <Text style={{ color: 'white' }}>CONTINUE</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }

  renderFilterPopUp = () => {
    let isFilterModalShown = this.state.filterModalVisible;
    return (
      <View style={{ height: isFilterModalShown ? '100%' : undefined, width: isFilterModalShown ? '100%' : undefined, backgroundColor: isFilterModalShown ? 'rgba(0,0,0,0.7)' : 'transparent' }}>
        <Modal
          animationType={"fade"}
          transparent={true}
          visible={this.state.filterModalVisible}
          onBackdropPress={() => this.setState({ filterModalVisible: false })}
          onRequestClose={() => { console.log("Modal has been closed.") }}>

          <TouchableOpacity style={styles.modalBackground} onPress={() => this.setState({ filterModalVisible: false })}>
            <View style={[styles.filterModal]}>
              <Text style={{ marginTop: 10, marginLeft: 10, color: '#808080', fontSize: 18 }}>Filter</Text>
              <FlatList
                data={this.state.communites}
                renderItem={this.renderFilterList}
                keyExtractor={(item, index) => index.toString()}
              />

              <View style={{ position: 'absolute', left: 0, right: 0, bottom: 5, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                <TouchableOpacity style={{ width: '30%', height: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.appColor, borderRadius: 5 }} onPress={() => this.onClearPress()}>
                  <Text style={{ fontWeight: 'bold', color: 'white' }}>CLEAR</Text>
                </TouchableOpacity>

                <View style={{ width: 20 }} />

                <TouchableOpacity style={{ width: '30%', height: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.appColor, borderRadius: 5 }} onPress={() => this.onApplyPress()}>
                  <Text style={{ fontWeight: 'bold', color: 'white' }}>APPLY</Text>
                </TouchableOpacity>
              </View>

            </View>
          </TouchableOpacity>

        </Modal>
      </View>

    )
  }

  renderFilterList = ({ item, index }) => {
    let clr = this.state.communites[index].isSelected ? Colors.appColor : 'transparent';
    return (
      <View style={{ flex: 1, marginLeft: 10, marginTop: 10, marginRight: 10, padding: 2 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ fontSize: 17 }}>{item.societyName}</Text>
          <TouchableOpacity style={{ height: 20, width: 20, borderColor: '#808080', borderWidth: 1, backgroundColor: clr, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.toggleFilterList(index)}>
            {this.state.communites[index].isSelected &&
              <Image source={Icons.tick} style={{ height: 16, width: 16, resizeMode: 'contain' }} />
            }
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  toggleFilterList = (index) => {
    let data = this.state.communites.slice(0);

    for (let i = 0; i < data.length; i++) {
      data[i].isSelected = false;
    }

    data[index].isSelected = !data[index].isSelected;

    this.setState({ communites: data });
  }

  onApplyPress = () => {

    let filData = this.state.communites.filter((v, i) => v.isSelected);

    if (filData.length) {
      this.setState({ filterModalVisible: false });
      this.filterData(filData[0].societyName);
    } else {
      Alert.alert('Please select community');
    }

  }

  onClearPress = () => {
    let data = this.state.communites.slice(0);

    for (let i = 0; i < data.length; i++) {
      data[i].isSelected = false;
    }
    this.setState({ filterModalVisible: false, taskData: this.arrayholder });
  }

  showMoreInfo = (index) => {
    let data = this.state.taskData.slice(0);

    data[index].showMoreInfo = !data[index].showMoreInfo;
    this.setState({ taskData: data });
  }

  render() {
    let dropdownOneData = this.state.communites;
    let dropdownOne = [];

    //dropdownOne.push({ value: "All" });

    if (dropdownOneData.length) {
      for (let p in dropdownOneData) {
        dropdownOne.push({
          value: dropdownOneData[p].societyName
        });
      }
    }

    return (
      <SafeAreaView style={{ flex: 1, marginBottom: Platform.OS == 'android' ? 50 : 0 }}>
        <Loader
          loading={this.state.loading} />
        <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
          <View style={{ flex: 1, backgroundColor: 'white', marginTop: 10, borderTopRightRadius: 15, borderTopLeftRadius: 15 }}>
            <View style={{ height: 50, marginTop: 10, width: '100%', alignSelf: 'center', marginLeft: 10, marginRight: 10, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>

              <ToggleSwitch
                isOn={this.props.isOn}
                onColor="green"
                offColor="red"
                label={this.props.duty}
                labelStyle={{ color: "black" }}
                size="medium"
                onToggle={this.props.toggleButton}
              />

              {/* this.state.communites.length ?
              <View style={{ height: 40, width: '30%', marginLeft: 5, marginRight: 5, borderRadius: 2, backgroundColor: "transparent", justifyContent: 'center', borderColor: Colors.appColor, borderWidth: 2 }}>
                <Dropdown
                  value={"Filter"}
                  containerStyle={{ marginBottom: 15 }}
                  inputContainerStyle={{ borderBottomColor: 'transparent', marginLeft: 5 }}
                  fontSize={12}
                  textColor={Colors.appColor}
                  fontWeight={'bold'}
                  data={dropdownOne}
                  onChangeText={(value, index) => this.filterData(value)}
                />
              </View>
              :
              null */
              }

              {/* <TouchableOpacity style={{ height: 40, padding: 5, borderRadius: 2, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.navigate('CheckSpeed')}>
              <Text style={{ color: 'white' }}>Check Speed</Text>
            </TouchableOpacity> */}

              <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                {/* <TouchableOpacity style={{ marginRight: 10, height: 30, width: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.navigate('CheckSpeed', { showScreen: true })}>
                 <Icon name={'location'} size={30} color={Colors.appColor} />
              </TouchableOpacity> */}
                {this.state.communites.length ?
                  <TouchableOpacity style={{ marginRight: 10, height: 30, width: 30, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.setState({ filterModalVisible: true })}>
                    <Icon name="funnel" size={30} color={Colors.appColor} />
                  </TouchableOpacity>
                  :
                  null
                }


                {/* <TouchableOpacity style={{ height: 40, padding: 5, borderRadius: 2, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center', marginRight: 10 }} onPress={() => this.props.navigation.navigate('ViewRoutes')}>
                  <Text style={{ color: 'white' }}>View Route</Text>
                </TouchableOpacity> */}
              </View>

            </View>

            {/* <View style={{ height: 50, marginLeft: 10, marginRight: 10, alignItems: 'center', justifyContent: 'center' }}>
            
          </View> */}

            <View style={{ marginTop: 10 }} />

            {this.state.taskData.length ?
              <FlatList
                data={this.state.taskData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) =>
                  <TaskList item={item}
                    onQuotationPress={() => { this.props.isOn ? this.setState({ isAmountModalVisible: true, order_id: item.order_id }) : Alert.alert('You are Off Duty.') }}
                    isAcceptPress={() => { this.props.isOn ? this.acceptOrRejectJob(item.assignId, "1") : Alert.alert('You are Off Duty.') }}
                    isDeclinePress={() => { this.props.isOn ? this.acceptOrRejectJob(item.assignId, "2") : Alert.alert('You are Off Duty.') }}
                    onListPressed={() => { this.props.isOn ? this.onListPressed(item) : Alert.alert('You are Off Duty.') }}
                    language={this.props.state.setLanguageFlag}
                    navigation={this.props.navigation}
                    isOn={this.props.isOn}
                    showMoreInfo={() => this.showMoreInfo(index)}
                    userId={item.user_id}
                    username={item.username}
                  />}
              />
              :
              <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <Text>No Data Found</Text>
              </View>
            }

            <View style={{ height: 20 }} />
          </View>
        </View>
        {this.renderAmountPopUp()}
        {this.renderFilterPopUp()}

        <TouchableOpacity style={{ position: 'absolute', bottom: 60, right: 30, height: 70, width: 70, borderColor: '#dddddd', borderWidth: 1, backgroundColor: 'white', borderRadius: 35, alignItems: 'center', justifyContent: 'center', shadowOpacity: 0.2, elevation: 2 }} onPress={() => this.props.navigation.navigate('ViewRoutes')}>
          <Image source={Icons.map} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
        </TouchableOpacity>
      </SafeAreaView>
    )
  }
}


class TaskList extends React.PureComponent {

  state = {
    isSosVisible: false,
  };

  getDate = (time) => {
    let date = time.split(' ');
    let t = time.split("-");
    let tt = "";

    if (t[3] != undefined)
      tt = "/" + t[3].trim();
    else
      tt = "";

    return moment(date[0]).format('DD MMM YYYY') + tt;
  }

  getTime = (time) => {
    let t = time.split("-")

    if (t[3] != undefined)
      return t[3].trim();
    else
      return "";
  }

  getInventoryName = (inventoryList) => {
    let inventoryName = "";
    let x = "";

    inventoryList.map((v, i) => {
      if (i == inventoryList.length - 1) {
        x = "";
      } else {
        x = ",";
      }

      if (v.inventoryName != null && v.inventoryName != "") {
        inventoryName = inventoryName + v.inventoryName + x;
      }

    });
    return inventoryName;
  }

  getInventoryQuantity = (inventoryList) => {
    let inventoryQuantity = "";
    let x = "";

    inventoryList.map((v, i) => {
      if (i == inventoryList.length - 1) {
        x = "";
      } else {
        x = ",";
      }

      inventoryQuantity = inventoryQuantity + v.inventoryquantity + x;

    });
    return inventoryQuantity;
  }

  audioCall = () => {
    this.setState({ isSosVisible: false });
    let userId = this.props.userId + "_" + this.props.username;
    console.log('callId => ', userId);
    if (hasPermissions()) {
      SinchVoip.callUserWithId("234_deep");
      this.props.navigation.navigate('CallScreen', { incoming: false });
    }
  }

  videoCall = () => {
    this.setState({ isSosVisible: false });
    let userId = this.props.userId + "_" + this.props.username;
    console.log('callId => ', userId);
    if (hasPermissions()) {
      SinchVoip.callUserWithIdUsingVideo("234_deep");
      this.props.navigation.navigate('CallScreen', { incoming: false, camera: true, });
    }
  }

  renderSoSModal = () => {
    return (
      <Modal
        style={{ width: '100%', height: undefined, alignSelf: 'center' }}
        animationType={"fade"}
        transparent={true}
        visible={this.state.isSosVisible}
      >
        <View style={{ height: this.state.isSosVisible ? '100%' : undefined, width: this.state.isSosVisible ? '100%' : undefined, backgroundColor: this.state.isSosVisible ? 'rgba(0,0,0,0.7)' : 'transparent' }}>
          <View style={[styles.modal, { top: '40%' }]}>

            <View style={{ padding: 10, height: 50, width: '100%', backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }}>
              <Image source={Icons.ic_sos_red} style={{ height: 25, width: 25, resizeMode: 'contain' }} />
            </View>

            {Platform.OS == 'ios' ?
              <TouchableOpacity style={{ padding: 10, height: 50, width: '100%', backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', borderBottomColor: 'black', borderBottomWidth: 1 }} onPress={() => this.audioCall()}>
                <Text style={{ fontSize: 15, color: 'black' }}>Audio Call</Text>
              </TouchableOpacity>
              :
              null
            }

            {Platform.OS == 'ios' ?
              <TouchableOpacity style={{ padding: 10, height: 50, width: '100%', backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', borderBottomColor: 'black', borderBottomWidth: 1 }} onPress={() => this.videoCall()}>
                <Text style={{ fontSize: 15, color: 'black' }}>Video Call</Text>
              </TouchableOpacity>
              :
              null
            }

            <TouchableOpacity style={{ padding: 10, height: 50, width: '100%', backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', borderBottomColor: 'black', borderBottomWidth: 1 }} onPress={() => this.setState({ isSosVisible: false })}>
              <Text style={{ fontSize: 15, color: '#ff0000' }}>Cancel</Text>
            </TouchableOpacity>

          </View>
        </View>
      </Modal>
    )
  }

  render() {
    const { item, onQuotationPress, isAcceptPress, isDeclinePress, onListPressed, language, showMoreInfo } = this.props;
    return (
      <TouchableOpacity style={{ paddingLeft: 5, marginTop: 5, marginBottom: 5, marginLeft: 10, marginRight: 10, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 2, shadowRadius: 1, shadowOffset: { height: 1, width: 0 }, shadowColor: '#808080', elevation: 2 }} onPress={onListPressed}>
        <View style={{ flexDirection: 'row', }}>
          <View style={{ paddingTop: 5, paddingBottom: 6, flex: 1 }}>
            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 10 }}>
              <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.community : Messages.En.community}</Text>
              <Text style={{ color: '#808080', fontWeight: '500' }}>{item.community}</Text>
            </View>

            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3, alignItems: 'center' }}>
              <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.name : Messages.En.name}</Text>
              <Text style={{ color: '#808080', fontWeight: '500' }}>{item.username}</Text>
              <TouchableOpacity onPress={() => this.setState({ isSosVisible: true })}>
                <Image source={Icons.phone} style={{ height: 30, width: 30, resizeMode: 'contain', marginLeft: 10 }} />
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
              <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.task : Messages.En.task}</Text>
              <View style={{ flexDirection: 'row' }}>
                <Text></Text>
                <Text style={{ width: 180, color: '#808080', fontWeight: '500' }}>{item.services}({'#' + item.order_id})</Text>
              </View>
            </View>

            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
              <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.date : Messages.En.date}</Text>
              <Text style={{ color: '#808080', fontWeight: '500' }}>{this.getDate(item.execute_datetime)}</Text>
            </View>

            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
              <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.phone : Messages.En.phone}</Text>
              <Text style={{ color: '#808080', fontWeight: '500' }}>{item.mobile}</Text>
            </View>

            {/* <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
          <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.orderId : Messages.En.orderId}</Text>
          <Text style={{ color: '#808080', fontWeight: '500' }}>{item.order_id}</Text>
        </View> */}

            {item.showMoreInfo ?
              <View>
                {item.inventoryList != null && item.inventorylist.length ?
                  <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.productName : Messages.En.productName}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{this.getInventoryName(item.inventorylist)}</Text>
                  </View>
                  :
                  null
                }

                {item.inventoryList != null && item.inventorylist.length ?
                  <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.quantity : Messages.En.quantity}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{this.getInventoryQuantity(item.inventorylist)}</Text>
                  </View>
                  :
                  null
                }

                {item.pickup_name != "" && item.pickup_name != "0" && item.pickup_name != null &&
                  <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.pickupName : Messages.En.pickupName}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{item.pickup_name}</Text>
                  </View>

                }

                {item.dropoff_name != "" && item.dropoff_name != "0" && item.dropoff_name != null &&
                  <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.dropoffName : Messages.En.dropoffName}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{item.dropoff_name}</Text>
                  </View>

                }


                {item.address != "" ?
                  <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.address : Messages.En.address}</Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text></Text>
                      <Text style={{ width: 180, color: '#808080', fontWeight: '500' }}>{item.address}</Text>
                    </View>
                  </View>
                  :
                  null
                }

                {item.pickup_address_location != "0" && item.pickup_address_location != "" ?
                  <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.pickupAddress : Messages.En.pickupAddress}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text></Text>
                      <Text style={{ width: 180, color: '#808080', fontWeight: '500' }}>{item.pickup_address_location}</Text>

                      <TouchableOpacity style={{ marginLeft: 5 }} onPress={() => { this.props.isOn ? this.props.navigation.navigate('MapActivity', { latitude: item.pick_lat, longitude: item.pick_lng, name: 'Directions for Pickup' }) : Alert.alert('You are Off Duty.') }}>
                        <Image source={Icons.direction} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
                      </TouchableOpacity>
                    </View>
                  </View>
                  :
                  null
                }

                {item.dropoff_address_location != "0" && item.dropoff_address_location != "" ?
                  <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.dropAddress : Messages.En.dropAddress}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text></Text>
                      <Text style={{ width: 180, color: '#808080', fontWeight: '500' }}>{item.dropoff_address_location}</Text>

                      <TouchableOpacity style={{ marginLeft: 5 }} onPress={() => { this.props.isOn ? this.props.navigation.navigate('MapActivity', { latitude: item.drop_lat, longitude: item.drop_lng, name: 'Directions for Drop' }) : Alert.alert('You are Off Duty.') }}>
                        <Image source={Icons.direction} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
                      </TouchableOpacity>
                    </View>
                  </View>
                  :
                  null
                }

                {item.taskPrice != "0" &&
                  <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.price : Messages.En.price}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>Rs {item.taskPrice}{item.payment_status != "" ? " (" + item.payment_status + ")" : ""}</Text>
                  </View>
                }


                {item.updated_price !== 0 ?
                  <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.extraQuotation : Messages.En.extraQuotation}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>Rs {item.updated_price}</Text>
                    <Image source={Icons.circularTick} style={{ marginLeft: 10, width: 15, height: 15, resizeMode: 'contain' }} />
                  </View>
                  :
                  null
                }
              </View>
              :
              null
            }
            {/**View End */}

            {/* <View style={{ flexDirection: 'row' }}>
          <Text style={{ width: '35%' }}>{language ? language.payment : Messages.En.payment}</Text>
          <Text>- {item.payment_status}</Text>
        </View> */}

            {item.latitude != "" && item.longitude != "" ?
              (item.updated_price !== 0 ?
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                  <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', height: 35, padding: 10, borderRadius: 5, backgroundColor: Colors.appColor }} onPress={() => { this.props.isOn ? this.props.navigation.navigate('MapActivity', { latitude: item.latitude, longitude: item.longitude, name: 'Directions for Destination' }) : Alert.alert('You are Off Duty.') }}>
                    <Text style={{ color: 'white' }}>{language ? language.navigate : Messages.En.navigate}</Text>
                  </TouchableOpacity>
                </View>
                :
                <View style={{ flexDirection: 'row-reverse', marginTop: 10, }}>
                  <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', height: 35, padding: 10, borderRadius: 5, backgroundColor: Colors.appColor }} onPress={onQuotationPress}>
                    <Text style={{ color: 'white' }}>{language ? language.addExtraQuotation : Messages.En.addExtraQuotation}</Text>
                  </TouchableOpacity>
                  <View style={{ width: 10 }} />
                  <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', height: 35, padding: 10, borderRadius: 5, backgroundColor: Colors.appColor }} onPress={() => { this.props.isOn ? this.props.navigation.navigate('MapActivity', { latitude: item.latitude, longitude: item.longitude, name: 'Directions for Destination' }) : Alert.alert('You are Off Duty.') }}>
                    <Text style={{ color: 'white' }}>{language ? language.navigate : Messages.En.navigate}</Text>
                  </TouchableOpacity>
                </View>
              )
              :
              null
            }

            {item.approvestatus != "1" ?
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10, marginLeft: 10 }}>
                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', height: 35, flex: 0.45, padding: 6, borderRadius: 5, borderColor: Colors.appColor, borderWidth: 1 }} onPress={isAcceptPress}>
                  <Text style={{ color: Colors.appColor }}>{language ? language.accept : Messages.En.accept}</Text>
                </TouchableOpacity>
                  <View style={{ width: 10 }} />
                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', height: 35, flex: 0.45, padding: 6, borderRadius: 5, borderColor: Colors.appColor, borderWidth: 1 }} onPress={isDeclinePress}>
                  <Text style={{ color: Colors.appColor }}>{language ? language.decline : Messages.En.decline}</Text>
                </TouchableOpacity>
              </View>
              :
              null
            }


            {/* item.pick_lat != "" && item.pick_lng != "" && item.pickup_address_location != "" &&
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', height: 35, flex: 1, padding: 6, borderRadius: 5, borderColor: Colors.appColor, borderWidth: 1 }}>
              <Text style={{ color: Colors.appColor }}>Click to Start Navigation to Pickup Location</Text>
            </TouchableOpacity>
          </View> */
            }

            {/* item.drop_lat != "" && item.drop_lng != "" && item.dropoff_address_location != "" &&
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', height: 35, flex: 1, padding: 6, borderRadius: 5, borderColor: Colors.appColor, borderWidth: 1 }}>
              <Text style={{ color: Colors.appColor }}>Click to Start Navigation to Drop Location</Text>
            </TouchableOpacity>
          </View> */
            }

            {/* item.approvestatus === "1" ?
              <View style={{ position: 'absolute', right: 10, top: "20%" }}>
                {item.is_fleet != "1" ?
                  <Image source={item.StartStopStatus == "1" ? Icons.stop : Icons.start} style={{ width: 60, height: 60, resizeMode: 'contain' }} />
                  :
                  <Image source={item.StartStopStatus == "1" ? Icons.dropoff : Icons.pickup} style={{ width: 60, height: 60, resizeMode: 'contain' }} />
                }
              </View>
              :
              null */
            }

          </View>

          {item.approvestatus === "1" ?
            (item.is_fleet != "1" ?
              <View style={{ backgroundColor: item.StartStopStatus == "1" ? '#f50101' : '#1bd740', height: undefined, width: 20, borderTopRightRadius: 10, overflow: 'hidden', justifyContent: 'center', alignItems: 'center' }}>
                <Image source={Icons.arrowRight} style={{ height: 14, width: 14, resizeMode: 'contain', tintColor: 'white' }} />
              </View>
              :
              <View style={{ backgroundColor: item.StartStopStatus == "1" ? '#f50101' : '#1bd740', height: undefined, width: 20, borderTopRightRadius: 10, overflow: 'hidden', justifyContent: 'center', alignItems: 'center' }}>
                <Image source={Icons.arrowRight} style={{ height: 14, width: 14, resizeMode: 'contain', tintColor: 'white' }} />
              </View>)
            :
            null
          }
        </View>

        <View style={{ height: 1, marginTop: 3, width: '100%', borderColor: '#dddddd', borderWidth: 1, borderStyle: 'dashed' }} />
        <TouchableOpacity style={{ height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={showMoreInfo}>
          <Text style={{ color: '#808080', fontWeight: '400' }}>More Info</Text>
        </TouchableOpacity>
        {this.renderSoSModal()}
      </TouchableOpacity>
    )
  }
}


const styles = StyleSheet.create({
  modal: {
    backgroundColor: 'white',
    width: '95%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.darkGray,
    alignSelf: 'center',
    justifyContent: 'space-around',
    position: 'absolute',
    top: '35%',
  },

  modalBackground: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: 'transparent'
  },

  filterModal: {
    backgroundColor: 'white',
    height: '80%',
    width: '95%',
    borderRadius: 5,
    borderWidth: 1,
    elevation: 1,
    borderColor: '#808080',
    borderWidth: 1
  },
});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(OpenTask);