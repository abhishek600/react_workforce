import React, { Component } from 'react';
import { SafeAreaView, View, FlatList, Text, TouchableOpacity, Image, Platform, Alert, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { Icons, Colors, Messages, Config, DataModel, NetworkUtils } from '@common';
import { connect } from 'react-redux';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { Header } from "@components";
import { ApiRequest } from '@httpsRequest';

let dataModel;
class ScanQRCode extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            userId: '',
            torchOff: RNCamera.Constants.FlashMode.off,
            torchOn: RNCamera.Constants.FlashMode.on,
            data: {},
            isFlashVisible: false,
            scannerShown: false,
            showData: false,
        };
        dataModel = DataModel.getInstance()
    }

    onSuccess = e => {
        console.log('message', e.data);
        //Alert.alert('message', JSON.stringify(e.data));
        this.setState({ scannerShown: false });
        this.assetScanInfo(e.data);
    };

    changeTorchMode = () => {
        this.setState({ isFlashVisible: !this.state.isFlashVisible });
    }

    onQrCodeScan = () => {
        this.setState({ scannerShown: true });
    }

    assetScanInfo = (info) => {
        let authToken = dataModel.getAuthToken();

        let params = { "action": "asset_scan_info", "qrinfo": info };
        ApiRequest.httpsPostMaster("asset_scan_info", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ showData: true, data: responseJson.result });
            } else {
                //Alert.alert(responseJson.message);
            }
        })

    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.background }}>
                {!this.state.scannerShown ?
                    <Header
                        backIcon={true}
                        isQrCodeData={false}
                        goBack={() => this.props.navigation.goBack(null)}
                        sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.scan : Messages.En.scan}
                    />
                    :
                    null
                }

                {!this.state.scannerShown ?
                    <TouchableOpacity style={{ height: 50, width: "60%", alignSelf: 'center', marginTop: 10, marginLeft: 20, marginRight: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.appColor }} onPress={() => this.onQrCodeScan()}>
                        <Text style={{ color: 'white', fontWeight: 'bold' }}>Scan QR OR BARCODE</Text>
                    </TouchableOpacity>
                    :
                    null
                }

                {this.state.showData ?
                    <View style={{ marginLeft: 20, marginRight: 20, marginTop: 20, flexDirection: 'row' }}>
                        <Text>Asset Name : </Text>
                        <Text>{this.state.data.assetName}</Text>
                    </View>
                    :
                    null
                }

                {this.state.showData ?
                    <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10, flexDirection: 'row' }}>
                        <Text>Inspection Type : </Text>
                        <Text>{this.state.data.inpsectionlist}</Text>
                    </View>
                    :
                    null
                }

                {this.state.showData ?
                    <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10, flexDirection: 'row' }}>
                        <Text>Next Inspection Date : </Text>
                        <Text>{this.state.data.nextinspectiondate}</Text>
                    </View>
                    :
                    null
                }

                {this.state.showData ?
                    <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10, flexDirection: 'row' }}>
                        <Text>Last Inspection Date : </Text>
                        <Text>{this.state.data.lastinspectiondate}</Text>
                    </View>
                    :
                    null
                }


                {this.state.scannerShown ?
                    <QRCodeScanner
                        onRead={this.onSuccess}
                        flashMode={this.state.isFlashVisible ? this.state.torchOn : this.state.torchOff}
                        bottomContent={
                            <TouchableOpacity style={{ padding: 5 }} onPress={() => this.changeTorchMode()}>
                                <Image source={this.state.isFlashVisible ? Icons.orangeTourch : Icons.normalTourch} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
                            </TouchableOpacity>
                        }
                    />
                    :
                    null
                }

            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: 32,
        color: '#777'
    },
    textBold: {
        fontWeight: '500',
        color: '#000'
    },
    buttonText: {
        fontSize: 21,
        color: 'rgb(0,122,255)'
    },
    buttonTouchable: {
        padding: 16
    }
});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(ScanQRCode);