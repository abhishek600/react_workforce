import React from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, TouchableOpacity, Text, Image, View, TextInput, Alert, ScrollView, FlatList, Platform, Keyboard } from 'react-native'
import { Icons, Colors, Messages, DataModel } from '@common'
import { Button, Loader, Header, QrCodeModal } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker';
import Modal from 'react-native-modal';
import { ApiRequest } from '@httpsRequest';

let dataModel;
class StopAssetInspection extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            photo1: null,
            photo2: null,
            isNoteModalVisible: false,
            qrModalVisible: false,
            note: '',
            assetId: this.props.navigation.getParam('assetId'),
            assetGroup: this.props.navigation.getParam('assetGroup'),
            startStopStatus: this.props.navigation.getParam('startStopStatus'),
            isMaintenance: this.props.navigation.getParam('isMaintenance'),
            userData: [],
            country: '',
            labourId: '',
            inspectData: [],
        };

        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let loginData = await AsyncStorage.getItem('loginData')

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({
                    country: this.state.userData[0].country_id,
                    labourId: this.state.userData[0].id
                }, () => { this.getAssetInspectionList(); })
            });
        }
    }

    getAssetInspectionList = () => {
        let country = this.state.country;
        let assetGroup = this.state.assetGroup;
        let isMaintenance = this.state.isMaintenance;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        let params = { "action": "get_asset_inspection_list", "country": country, "asset_group": assetGroup, "is_maintenance": isMaintenance };
        //let params = "&country=" + country + "&asset_group=" + assetGroup + "&is_maintenance=" + isMaintenance + "&access_token=" + testToken;
        ApiRequest.httpsPostMaster("get_asset_inspection_list", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                let data = [];

                responseJson.result.map((v, i) => {
                    let x = {};

                    x.name = v.name;
                    x.is_selected = false;

                    data.push(x);

                    if (i == responseJson.result.length - 1) {
                        this.setState({
                            loading: false,
                            inspectData: data
                        })
                    }
                });
            } else {
                this.setState({ loading: false });
                Alert.alert(responseJson.message);
            }
        })

    }

    launchCamera = (num) => {
        let options = {
            quality: 1,
            maxWidth: 150,
            maxHeight: 150,
            allowsEditing: false,
            noData: true,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = response;
                console.log('response', JSON.stringify(response));
                if (num === "1") {
                    this.setState({ photo1: source });
                } else {
                    this.setState({ photo2: source });
                }
            }
        });

    }

    onBtnPressed = () => {
        let photo1 = this.state.photo1;
        let photo2 = this.state.photo2;
        if (photo1 == null && photo2 == null) {
            Alert.alert('Please select atleast 1 image');
            return;
        }

        let assets = this.state.inspectData.filter((v, i) => v.is_selected);

        if (!assets.length) {
            Alert.alert('Please select Asset');
            return;
        }

        this.setState({ isNoteModalVisible: true })
    }

    submitAssetInspection = () => {
        this.setState({ isNoteModalVisible: false });

        let photo1 = this.state.photo1;
        let photo2 = this.state.photo2;
        let country = this.state.country;
        let labourId = this.state.labourId;
        let assetId = this.state.assetId;
        let note = this.state.note;
        let isMaintenance = this.state.isMaintenance;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        let assets = this.state.inspectData.filter((v, i) => v.is_selected);
        let assetNames = '';
        let x = '';

        assets.map((v, i) => {

            if (i == assets.length - 1) {
                x = '';
            } else {
                x = ',';
            }

            assetNames = assetNames + v.name + x;
        });

        this.setState({ loading: true });

        let url = "https://townconnect.in/townlabour/webservice/master2.php?";
        let authUrl = "https://townconnect.in/townlabour/webservice/v1.2/workforce.php?access_token=" + authToken;

        let formdata = new FormData();

        formdata.append('action', 'submit_asset_inspection')
        if (photo1 != null) {
            formdata.append('image1', { uri: photo1.uri, name: photo1.fileName != null ? photo1.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo1.type })
        }
        if (photo2 != null) {
            formdata.append('image2', { uri: photo2.uri, name: photo2.fileName != null ? photo2.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo2.type })
        }

        if (isMaintenance == "0") {
            formdata.append('asset_group', assetNames)
            formdata.append('assetId_maintainance', '')
        } else {
            formdata.append('asset_group', '')
            formdata.append('assetId_maintainance', assetNames)
        }

        formdata.append('submissionId', '')
        formdata.append('country', country)
        formdata.append('assetId', assetId)
        formdata.append('note', note)
        formdata.append('labourId', labourId)
        formdata.append('StartStopStatus', "2")

        fetch(authUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data;',
                Accept: "application/json"
            },
            timeout: 5000,
            body: formdata
        })
            .then(response => response.json())
            .then((responseJson) => {
                console.log('stopAssetInspection', responseJson);
                if (responseJson.status === 200) {
                    this.setState({ loading: false });
                    this.props.navigation.goBack(null);
                } else {
                    this.setState({ loading: false });
                    Alert.alert(responseJson.message);
                }

            })
            .catch(error => console.log(error))
    }

    GenerateRandomNumber = () => {
        var RandomNumber = Math.floor(Math.random() * 100) + 1;
        return RandomNumber + ".jpg";
    }

    toogleIndex = (index) => {

        let data = this.state.inspectData.slice(0);
        data[index].is_selected = true;

        this.setState({ inspectData: data });
    }

    renderItem = ({ item, index }) => {
        return (
            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ fontSize: 15, color: Colors.darkGray }}>{item.name}</Text>
                <TouchableOpacity style={{ height: 20, width: 20, borderColor: item.is_selected ? 'transparent' : Colors.darkGray, backgroundColor: item.is_selected ? Colors.appColor : 'transparent', borderWidth: 2, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.toogleIndex(index)}>
                    {item.is_selected ?
                        <Image source={Icons.tick} style={{ height: 18, width: 18, borderRadius: 9 }} />
                        :
                        null
                    }
                </TouchableOpacity>
            </View>
        )
    }


    render() {
        let qrCodeData = dataModel.getQrCodeData();
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader
                    loading={this.state.loading} />
                <ImageBackground source={Icons.background} style={{ flex: 1 }}>
                    <ScrollView keyboardShouldPersistTaps="always">
                        <Header
                            sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.stop : Messages.En.stop}
                            qrCodeData={qrCodeData}
                            qrCodePress={() => this.setState({ qrModalVisible: true })}
                            onChatPress={() => this.props.navigation.navigate('Chat')}
                            backIcon={true}
                            goBack={() => this.props.navigation.goBack(null)}
                        />

                        <View style={{ alignSelf: 'center', marginTop: 20, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => this.launchCamera("1")}>
                                {this.state.photo1 == null ?
                                    <Image source={Icons.imageUpload} style={{ height: 150, width: 150, resizeMode: 'contain' }} />
                                    :
                                    <Image source={{ uri: this.state.photo1.uri }} style={{ height: 150, width: 150 }} />
                                }
                            </TouchableOpacity>
                            <View style={{ width: 10 }} />
                            <TouchableOpacity onPress={() => this.launchCamera("2")}>
                                {this.state.photo2 == null ?
                                    <Image source={Icons.imageUpload} style={{ height: 150, width: 150, resizeMode: 'contain' }} />
                                    :
                                    <Image source={{ uri: this.state.photo2.uri }} style={{ height: 150, width: 150 }} />
                                }
                            </TouchableOpacity>
                        </View>

                        <FlatList
                            data={this.state.inspectData}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />

                    </ScrollView>
                    <View style={{ position: 'absolute', bottom: 10, width: '100%' }}>
                        <Button
                            text={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.stop : Messages.En.stop}
                            onPress={() => this.onBtnPressed()}
                        />
                    </View>
                </ImageBackground>

                <Modal
                    animationType={"fade"}
                    transparent={true}
                    visible={this.state.isNoteModalVisible}
                    onBackdropPress={() => this.setState({ isNoteModalVisible: false })}
                    onRequestClose={() => { console.log("Modal has been closed.") }}
                >
                    <View style={styles.modal}>

                        <View style={{ marginTop: 5 }}>
                            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 1 }}>
                                <TextInput
                                    style={[styles.input1, { height: 85, borderColor: Colors.darkGray }]}
                                    autoFocus={true}
                                    placeholder={'Note(Optional)'}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor={Colors.black}
                                    multiline={true}
                                    blurOnSubmit={true}
                                    onSubmitEditing={() => { Keyboard.dismiss() }}
                                    onChangeText={(note) => this.setState({ note })}
                                />
                            </View>
                        </View>

                        <TouchableOpacity style={{ marginTop: 10, marginBottom: 10, alignSelf: 'center', backgroundColor: Colors.appColor, height: 50, width: '60%', alignItems: 'center', justifyContent: 'center', borderRadius: 5 }} onPress={() => this.submitAssetInspection()}>
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>SUBMIT</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>

                <QrCodeModal
                    qrCodeData={qrCodeData}
                    qrModalVisible={this.state.qrModalVisible}
                    backPress={() => this.setState({ qrModalVisible: false })}
                />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    modal: {
        backgroundColor: 'white',
        width: '95%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Colors.darkGray,
        alignSelf: 'center',
        justifyContent: 'space-around',
        position: 'absolute',
        top: '35%',
    },
    input1: {
        marginTop: 10,
        textAlignVertical: "top",
        padding: 10,
        textAlign: 'left',
        backgroundColor: Colors.white,
        borderColor: Colors.gray,
        borderWidth: 0.6,
        ...Platform.select({
            ios: {
                marginBottom: 10
            }
        })
    },

});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(StopAssetInspection);