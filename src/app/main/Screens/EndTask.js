import React from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, TouchableOpacity, Text, Image, View, TextInput, Alert, ScrollView, Platform, Keyboard } from 'react-native'
import { Icons, Colors, Messages, DataModel } from '@common'
import { Button, Loader, Header, QrCodeModal } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { Dropdown } from 'react-native-material-dropdown';
import StarRating from 'react-native-star-rating';
import ImagePicker from 'react-native-image-picker';
import { updateTasks } from '../../actions/actions';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { getDistance, getPreciseDistance } from 'geolib';

let dataModel;
class EndTask extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      photo1: null,
      photo2: null,
      photo3: null,
      photo4: null,
      photo5: null,
      photo6: null,
      assignId: this.props.navigation.getParam('assignId'),
      is_fleet: this.props.navigation.getParam('is_fleet'),
      serviceType: this.props.navigation.getParam('serviceType'),
      pick_lat: this.props.navigation.getParam('pick_lat'),
      pick_lng: this.props.navigation.getParam('pick_lng'),
      drop_lat: this.props.navigation.getParam('drop_lat'),
      drop_lng: this.props.navigation.getParam('drop_lng'),
      order_id: this.props.navigation.getParam('order_id'),
      covid: this.props.navigation.getParam('covid'),
      description: '',
      dayValue: '1 Day',
      orderStatusValue: 'Completed',
      orderStatusIndex: '6',
      isFollowUp: false,
      starCount: 0,
      country: '',
      society: '',
      quantity: '',
      workforceFlow: '',
      client: '',
      branch: '',
      department: '',
      remarks: '',
      qrModalVisible: false,
      userData: [],
      metersData: 'KM',
      meterReading: '',
      meters: [{
        value: 'KM',
      },
      {
        value: 'ML',
      }],
      day: [{
        value: '1 Day',
      },
      {
        value: '2 Day',
      },
      {
        value: '3 Day',
      },
      {
        value: '4 Day',
      },
      {
        value: '5 Day',
      },
      {
        value: '6 Day',
      },
      {
        value: '7 Day',
      }],
      orderStatus: [{
        value: '',
      },
      {
        value: 'Order incomplete as issue not under FM Preview',
      },
      {
        value: 'Order incomplete, no materials',
      },
      {
        value: 'Order site viewed, external contractor needed',
      },
      {
        value: 'Order site viewed, issue found in common area',
      },
      {
        value: 'Order incomplete, resident not available',
      },
      {
        value: 'Completed',
      }],
    };

    dataModel = DataModel.getInstance();
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    let society = await AsyncStorage.getItem('isSocietyUser')
    let loginData = await AsyncStorage.getItem('loginData')

    if (society) {
      this.setState({ society: society });
    }

    if (loginData) {
      this.setState({ userData: JSON.parse(loginData) }, () => {
        this.setState({
          country: this.state.userData[0].country_id,
          workforceFlow: this.state.userData[0].workforceflow1
        })
      });
    }
  }

  onFollowUpPress = () => {
    this.setState({ isFollowUp: !this.state.isFollowUp });
  }

  onStarRatingPress = (rating) => {
    this.setState({
      starCount: rating
    });
  }

  launchCamera = (num) => {
    let photo3 = this.state.photo3;
    let photo4 = this.state.photo4;
    let photo5 = this.state.photo5;
    let photo6 = this.state.photo6;

    if (num == "999") {
      if (photo3 != null && photo4 != null && photo5 != null && photo6 != null) {
        Alert.alert("You can't select more than 6 images");
        return;
      }
    }

    let options = {
      quality: 1,
      maxWidth: 150,
      maxHeight: 150,
      allowsEditing: false,
      noData: true,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        console.log('response', JSON.stringify(response));

        if (num == "999") {
          if (photo3 == null && photo4 == null && photo5 == null && photo6 == null) {
            this.setState({ photo3: source });
          } else if (photo3 != null && photo4 == null && photo5 == null && photo6 == null) {
            this.setState({ photo4: source });
          } else if (photo3 != null && photo4 != null && photo5 == null && photo6 == null) {
            this.setState({ photo5: source });
          } else if (photo3 != null && photo4 != null && photo5 != null && photo6 == null) {
            this.setState({ photo6: source });
          } else {

          }
        } else {
          if (num == "1") {
            this.setState({ photo1: source });
          } else if (num == "2") {
            this.setState({ photo2: source });
          } else if (num == "3") {
            this.setState({ photo3: source });
          } else if (num == "4") {
            this.setState({ photo4: source });
          } else if (num == "5") {
            this.setState({ photo5: source });
          } else {
            this.setState({ photo6: source });
          }
        }

      }
    });

  }

  GenerateRandomNumber = () => {
    var RandomNumber = Math.floor(Math.random() * 100) + 1;
    return RandomNumber + ".jpg";
  }

  onEndTaskPress = () => {
    let photo1 = this.state.photo1;
    let photo2 = this.state.photo2;
    let photo3 = this.state.photo3;
    let photo4 = this.state.photo4;
    let photo5 = this.state.photo5;
    let photo6 = this.state.photo6;
    let rating = this.state.starCount.toString();
    let des = this.state.description;
    let society = this.state.society;
    let country = this.state.country;
    let assignId = this.state.assignId;
    let quantity = this.state.quantity;
    let client = this.state.client;
    let branch = this.state.branch;
    let department = this.state.department;
    let remarks = this.state.remarks;
    let serviceType = this.state.serviceType;
    let distance = '';
    let authToken = dataModel.getAuthToken();
    let testToken = dataModel.getTestToken();

    if (photo1 == null && photo2 == null) {
      Alert.alert('Please select atleast 1 image');
      return;
    }

    if (serviceType == '3') {
      distance = this.calculateDistance();
    }

    let is_schdule = "0";
    let reschdule_time = "";

    if (this.state.isFollowUp) {
      is_schdule = "1";
      reschdule_time = this.state.dayValue;
    }

    if (this.state.orderStatusIndex == 0) {
      Alert.alert('Please Select Order Status');
      return;
    }

    this.setState({ loading: true });
    let url = "https://townconnect.in/townlabour/webservice/Api.php?";
    let authUrl = "https://townconnect.in/townlabour/webservice/v1.2/workforce.php?access_token=" + authToken;

    let formdata = new FormData();

    formdata.append('action', 'startStop')
    formdata.append('sign_start', null)
    formdata.append('sign_stop', null)

    if (photo1 != null) {
      formdata.append('image1', { uri: photo1.uri, name: photo1.fileName != null ? photo1.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo1.type })
    }

    if (photo2 != null) {
      formdata.append('image2', { uri: photo2.uri, name: photo2.fileName != null ? photo2.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo2.type })
    }

    if (photo3 != null) {
      formdata.append('imageoptional1', { uri: photo3.uri, name: photo3.fileName != null ? photo3.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo3.type })
    }

    if (photo4 != null) {
      formdata.append('imageoptional2', { uri: photo4.uri, name: photo4.fileName != null ? photo4.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo4.type })
    }

    if (photo5 != null) {
      formdata.append('imageoptional3', { uri: photo5.uri, name: photo5.fileName != null ? photo5.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo5.type })
    }

    if (photo6 != null) {
      formdata.append('imageoptional4', { uri: photo6.uri, name: photo6.fileName != null ? photo6.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo6.type })
    }

    formdata.append('no_items', quantity)
    formdata.append('unit_type', this.state.metersData)
    formdata.append('isReschdule', is_schdule)
    formdata.append('reschdule_time', reschdule_time)
    formdata.append('mstop_reading', this.state.meterReading)
    formdata.append('m_reading', '')
    formdata.append('assetId', '')
    formdata.append('rating', rating)
    formdata.append('description', des)
    formdata.append('reason', this.state.orderStatusIndex.toString())
    formdata.append('country', country)
    formdata.append('verifyOTP', '')
    formdata.append('assignId', assignId)
    formdata.append('StartStopStatus', "1")
    formdata.append('society', society)
    formdata.append('time', '')
    formdata.append('km', distance)
    formdata.append('clientName', client)
    formdata.append('clientBranch', branch)
    formdata.append('clientDepartment', department)
    formdata.append('remarkonclient', remarks)

    console.log('data', formdata);

    fetch(authUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data;',
        Accept: "application/json"
      },
      body: formdata
    })
      .then(response => response.json())
      .then((responseJson) => {
        console.log('endTaskResponse', responseJson);
        if (responseJson.status === 200) {
          this.setState({ loading: false });
          let x = this.props.state.updateTasksFlag ? this.props.state.updateTasksFlag + 1 : 1;
          this.props.updateTasks(x);
          if (serviceType == '3') {
            this.props.navigation.navigate('BillScreen', { taskPrice: responseJson.taskPrice });
          } else {
            this.props.navigation.goBack(null);
          }
        } else {
          this.setState({ loading: false });
          //Alert.alert(responseJson.message);
        }

      })
      .catch(error => console.log(error))
  }

  calculateDistance = () => {
    let pick_lat = this.state.pick_lat;
    let pick_lng = this.state.pick_lng;
    let drop_lat = this.state.drop_lat;
    let drop_lng = this.state.drop_lng;

    var dis = getDistance(
      { latitude: pick_lat, longitude: pick_lng },
      { latitude: drop_lat, longitude: drop_lng },
    );

    console.log('distance', dis / 1000);
    return dis / 1000;
  }

  render() {
    let borderColor = this.state.isFollowUp ? 'transparent' : Colors.darkGray;
    let backgroundColor = this.state.isFollowUp ? Colors.appColor : 'transparent';
    let qrCodeData = dataModel.getQrCodeData();
    let is_fleet = this.state.is_fleet;
    let workforceFlow = this.state.workforceFlow;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Loader
          loading={this.state.loading} />

        <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
          <Header
            sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.endTask : Messages.En.endTask}
            isQrCodeData={false}
            backIcon={true}
            goBack={() => this.props.navigation.goBack(null)}
          />
          <View style={{ flex: 1, backgroundColor: 'white', marginTop: 10, borderTopLeftRadius: 15, borderTopRightRadius: 15 }}>
            <ScrollView keyboardShouldPersistTaps="always">
              <ScrollView horizontal={true} style={{ marginLeft: 20, marginRight: 20 }}>
                <View style={{ alignItems: 'center', marginTop: 20, flexDirection: 'row' }}>
                  <TouchableOpacity style={{ height: 120, width: 120, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("1")}>
                    {this.state.photo1 == null ?
                      <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                      :
                      <Image source={{ uri: this.state.photo1.uri }} style={{ height: 100, width: 100 }} />
                    }
                  </TouchableOpacity>
                  <View style={{ width: 10 }} />
                  <TouchableOpacity style={{ height: 120, width: 120, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("2")}>
                    {this.state.photo2 == null ?
                      <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                      :
                      <Image source={{ uri: this.state.photo2.uri }} style={{ height: 100, width: 100 }} />
                    }
                  </TouchableOpacity>

                  {this.state.photo1 != null && this.state.photo2 != null ?
                    <TouchableOpacity style={{ height: 120, width: 120, marginLeft: 10, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("3")}>
                      {this.state.photo3 == null ?
                        <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                        :
                        <Image source={{ uri: this.state.photo3.uri }} style={{ height: 100, width: 100 }} />
                      }
                    </TouchableOpacity>
                    :
                    null
                  }

                  {this.state.photo1 != null && this.state.photo2 != null && this.state.photo3 != null ?
                    <TouchableOpacity style={{ height: 120, width: 120, marginLeft: 10, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("4")}>
                      {this.state.photo4 == null ?
                        <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                        :
                        <Image source={{ uri: this.state.photo4.uri }} style={{ height: 100, width: 100 }} />
                      }
                    </TouchableOpacity>
                    :
                    null
                  }

                  {this.state.photo1 != null && this.state.photo2 != null && this.state.photo3 != null && this.state.photo4 != null ?
                    <TouchableOpacity style={{ height: 120, width: 120, marginLeft: 10, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("5")}>
                      {this.state.photo5 == null ?
                        <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                        :
                        <Image source={{ uri: this.state.photo5.uri }} style={{ height: 100, width: 100 }} />
                      }
                    </TouchableOpacity>
                    :
                    null
                  }

                  {this.state.photo1 != null && this.state.photo2 != null && this.state.photo3 != null && this.state.photo4 != null && this.state.photo5 != null ?
                    <TouchableOpacity style={{ height: 120, width: 120, marginLeft: 10, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("6")}>
                      {this.state.photo6 == null ?
                        <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                        :
                        <Image source={{ uri: this.state.photo6.uri }} style={{ height: 100, width: 100 }} />
                      }
                    </TouchableOpacity>
                    :
                    null
                  }

                  {/* <Icon style={{ color: 'limegreen', marginLeft: 10 }} size={50} name={'add-circle-sharp'} onPress={() => this.launchCamera("999")} /> */}
                </View>
              </ScrollView>

              {is_fleet == "0" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                  <Text style={{ fontSize: 15, color: Colors.darkGray, fontWeight: '500' }}>Follow up</Text>
                  <TouchableOpacity style={{ height: 20, width: 20, borderColor: borderColor, backgroundColor: backgroundColor, borderWidth: 2, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onFollowUpPress()}>
                    {this.state.isFollowUp ?
                      <Image source={Icons.tick} style={{ height: 18, width: 18, borderRadius: 9 }} />
                      :
                      null
                    }
                  </TouchableOpacity>
                </View>
                :
                null
              }

              {this.state.isFollowUp && is_fleet == "0" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 1 }}>
                  <View style={[styles.input1, { height: 40, borderRadius: 10, justifyContent: 'center', backgroundColor: 'white' }]}>
                    <Dropdown
                      value={this.state.dayValue}
                      containerStyle={{ marginBottom: 12 }}
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      fontSize={12}
                      data={this.state.day}
                      onChangeText={(value, index) => this.setState({ dayValue: value })}
                    />
                  </View>
                </View>
                :
                null
              }

              {workforceFlow == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 30 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{'Client'}</Text>
                  <TextInput
                    style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    editable={false}
                    onChangeText={(client) => this.setState({ client })}
                  />
                </View>
                :
                null
              }

              {workforceFlow == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{'Branch'}</Text>
                  <TextInput
                    style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    editable={false}
                    onChangeText={(branch) => this.setState({ branch })}
                  />
                </View>
                :
                null
              }

              {workforceFlow == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{'Department'}</Text>
                  <TextInput 
                    style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    editable={false}
                    onChangeText={(department) => this.setState({ department })}
                  />
                </View>
                :
                null
              }

              {workforceFlow == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                   <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{'Remarks'}</Text>
                   <TextInput 
                     style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                     underlineColorAndroid="transparent"
                     onChangeText={(remarks) => this.setState({ remarks })}
                   />
                </View>
                :
                null
              }

              {is_fleet == "0" || is_fleet == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.observations : Messages.En.observations}</Text>
                  <TextInput
                    style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    multiline={true}
                    blurOnSubmit={false}
                    onSubmitEditing={() => Keyboard.dismiss()}
                    onChangeText={(description) => this.setState({ description })}
                  />
                </View>
                :
                null
              }

              {is_fleet == "1" && workforceFlow != "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, marginTop: 10 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.unitType : Messages.En.unitType} :</Text>
                  <View style={[styles.input1, { height: 40, marginTop: 1, borderRadius: 10, justifyContent: 'center', backgroundColor: 'white' }]}>
                    <Dropdown
                      value={this.state.metersData}
                      containerStyle={{ marginBottom: 12 }}
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      fontSize={12}
                      data={this.state.meters}
                      onChangeText={(value, index) => this.setState({ metersData: value })}
                    />
                  </View>
                </View>
                :
                null
              }

              {is_fleet == "1" && workforceFlow != "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>Unit(Meter Reading)</Text>
                  <TextInput
                    style={[styles.input, { height: 40 }]}
                    underlineColorAndroid="transparent"
                    keyboardType={'numeric'}
                    returnKeyType={'done'}
                    onChangeText={(meterReading) => this.setState({ meterReading })}
                  />
                </View>
                :
                null
              }


              <View style={{ marginLeft: 20, marginRight: 20, marginTop: 20 }}>
                <Text style={{ color: Colors.darkGray, marginTop: 10 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.orderStatus : Messages.En.orderStatus}</Text>
                <View style={[styles.input1, { height: 40, marginTop: 1, borderRadius: 10, justifyContent: 'center', backgroundColor: 'white' }]}>
                  <Dropdown
                    value={this.state.orderStatusValue}
                    containerStyle={{ marginBottom: 12 }}
                    inputContainerStyle={{ borderBottomColor: 'transparent' }}
                    fontSize={12}
                    data={this.state.orderStatus}
                    onChangeText={(value, index) => this.setState({ orderStatusValue: value, orderStatusIndex: index })}
                  />
                </View>
              </View>

              {is_fleet == "1" && workforceFlow != "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500', marginTop: 10 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.quantity : Messages.En.quantity}</Text>
                  <TextInput
                    style={[styles.input1, { height: 40, marginTop: 1, borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    keyboardType={'numeric'}
                    returnKeyType={'done'}
                    onChangeText={(quantity) => this.setState({ quantity })}
                  />
                </View>
                :
                null
              }

              {is_fleet == "0" ?
                <View style={{ backgroundColor: 'white', height: 80, alignItems: 'center', justifyContent: 'center' }}>
                  <StarRating
                    //disabled={false}
                    starSize={40}
                    starStyle={{ marginLeft: 10 }}
                    maxStars={5}
                    rating={this.state.starCount}
                    emptyStar={Icons.emptyStar}
                    fullStar={Icons.fullStar}
                    fullStarColor={Colors.appColor}
                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                  />
                </View>
                :
                null
              }

              {is_fleet == "0" ?
                <View style={{ flexDirection: 'row', backgroundColor: 'white', alignItems: 'center', marginLeft: 10, marginRight: 10 }}>
                  <View style={{ flex: 1, height: 1, backgroundColor: Colors.darkGray }} />
                  <View>
                    <Text style={{ width: 130, textAlign: 'center', color: Colors.darkGray, fontWeight: '500' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.customerRating : Messages.En.customerRating}</Text>
                  </View>
                  <View style={{ flex: 1, height: 1, backgroundColor: Colors.darkGray }} />
                </View>
                :
                null
              }

              <View style={{ height: 20 }} />
            </ScrollView>
            <TouchableOpacity style={{ height: 50, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onEndTaskPress()}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.endTaskCaps : Messages.En.endTaskCaps}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <QrCodeModal
          qrCodeData={qrCodeData}
          qrModalVisible={this.state.qrModalVisible}
          backPress={() => this.setState({ qrModalVisible: false })}
        />
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  input1: {
    marginTop: 10,
    textAlignVertical: "top",
    padding: 10,
    textAlign: 'left',
    borderBottomColor: Colors.darkGray,
    borderBottomWidth: 1,
    borderRadius: 2,
    ...Platform.select({
      ios: {
        marginBottom: 10
      }
    })
  },

  input: {
    marginTop: 10,
    textAlignVertical: "top",
    padding: 10,
    textAlign: 'left',
    borderBottomColor: Colors.darkGray,
    borderBottomWidth: 1,
    borderRadius: 2,
    ...Platform.select({
      ios: {
        marginBottom: 10
      }
    })
  },

});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
  updateTasks: (data) => {
    dispatch(updateTasks(data));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(EndTask);