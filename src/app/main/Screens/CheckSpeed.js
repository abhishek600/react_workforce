import React, { Component } from 'react';
import { SafeAreaView, Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import { Messages, Colors, Icons, DataModel } from '@common';
import { Header } from '@components';
import { connect } from 'react-redux';
import MapView from 'react-native-maps';
import { ApiRequest } from '@httpsRequest';

navigator.geolocation = require('@react-native-community/geolocation');

const styles = StyleSheet.create({
    map: {
        height: '100%',
        width: '100%',
    },

});

let id = 0;
let dataModel;
class CheckSpeed extends Component {

    static defaultProps = {
        acceleration: 0,
        breakdiff: 0,
        overspeed: 0,
    }

    constructor(props) {
        super(props);
        dataModel = DataModel.getInstance();

        let watchID = navigator.geolocation.watchPosition((position) => {
            let lastSpeed = 0 || this.state.speed;

            let speed = position.coords.speed;
            let lat = position.coords.latitude;
            let lng = position.coords.longitude;

            let speedDifference = lastSpeed - speed;

            console.log('lastSpeed', lastSpeed);
            console.log('nextSpeed', speed);
            console.log('speedDifference', speedDifference);

            if (this.props.acceleration == 0) {
                if (speed > lastSpeed && speedDifference > 34) {
                    if (dataModel.getAssignId() != "") {
                        console.log('Harsh Acceleration');
                        this.submitSpeed(speed, lat, lng, 0, 0, 1);
                    }
                }

            } else {
                if (speed > lastSpeed && speedDifference > this.props.acceleration) {
                    if (dataModel.getAssignId() != "") {
                        console.log('Harsh Acceleration');
                        this.submitSpeed(speed, lat, lng, 0, 0, 1);
                    }
                }

            }

            if (this.props.breakdiff == 0) {
                if (speedDifference > 36) {
                    if (dataModel.getAssignId() != "") {
                        console.log('Harsh Break');
                        this.submitSpeed(speed, lat, lng, 0, 1, 0);
                    }
                }
            } else {
                if (speedDifference > this.props.breakdiff) {
                    if (dataModel.getAssignId() != "") {
                        console.log('Harsh Break');
                        this.submitSpeed(speed, lat, lng, 0, 1, 0);
                    }
                }
            }

            if (this.props.overspeed == 0) {
                if (speed > 80) {
                    if (dataModel.getAssignId() != "") {
                        console.log('Over-Speeding');
                        this.submitSpeed(speed, lat, lng, 1, 0, 0);
                    }
                }
            } else {
                if (speed > this.props.overspeed) {
                    if (dataModel.getAssignId() != "") {
                        console.log('Over-Speeding');
                        this.submitSpeed(speed, lat, lng, 1, 0, 0);
                    }
                }
            }

            this.setState({
                markers: [
                    ...this.state.markers, {
                        coordinate: position.coords,
                        key: id++
                    }
                ],
                speed

            }, null, { distanceFilter: 100 })
        });

        this.state = { markers: [], watchID, speed: 0, userData: [], country: '' };

    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let loginData = await AsyncStorage.getItem('loginData');

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({ country: this.state.userData[0].country_id });
            });
        }
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.state.watchID);
    }

    submitSpeed = (speed, lat, lng, overspeed, brake, acceleration) => {
        let country = this.state.country;
        let assignId = dataModel.getAssignId();
        let authToken = dataModel.getAuthToken();

        let params = { "action": "submit_speed", "country": country, "assignid": assignId, "speed": speed, "overspeed": overspeed, "break": brake, "acceleration": acceleration, "lat": lat, "lng": lng };
        ApiRequest.httpsPost("submit_speed", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {

            } else {

            }
        })
    }

    addMarker(region) {
        let now = (new Date).getTime();
        if (this.state.ladAddedMarker > now - 5000) {
            return;
        }

        this.setState({
            markers: [
                ...this.state.markers, {
                    coordinate: region,
                    key: id++
                }
            ],
            ladAddedMarker: now
        });
    }

    render() {
        if (!this.props.navigation.getParam('showScreen')) {
            return (
                <SafeAreaView style={{ height: undefined, width: undefined }}>
                </SafeAreaView>
            )
        } else {
            return (
                <SafeAreaView style={{ height: "100%", width: "100%" }}>
                    <Header
                        sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.speed : Messages.En.speed}
                        backIcon={true}
                        goBack={() => this.props.navigation.goBack(null)}
                        isQrCodeData={false}
                    />

                    <MapView style={styles.map}
                        showsUserLocation
                        followsUserLocation
                        initialRegion={{
                            latitude: 37.33307,
                            longitude: -122.0324,
                            latitudeDelta: 0.02,
                            longitudeDelta: 0.02,
                        }}
                    //onRegionChange={(region) => this.addMarker(region)}
                    >
                        {/* <MapView.Polyline
                        coordinates={this.state.markers.map((marker) => marker.coordinate)}
                        strokeWidth={5}
                        strokeColor={Colors.appColor}
                    /> */}

                        {/* {this.state.markers.map((marker) => (
                            <MapView.Marker coordinate={marker.coordinate} key={marker.key} />
                        ))} */}

                    </MapView>

                    {/* <View style={{ position: 'absolute', left: 0, right: 0, bottom: 1, backgroundColor: 'rgba(255, 255, 255, 0.75)', height: 50, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 15 }}>Speed: {this.state.speed} km/h</Text>
                </View> */}

                </SafeAreaView>
            )
        }

    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(CheckSpeed);

