import React from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, Image, Platform, Alert } from 'react-native'
import { Icons, Messages, DataModel } from '@common'
import { connect } from 'react-redux'
import { setLanguage, getUserToken } from '../../actions/actions'
import AsyncStorage from '@react-native-community/async-storage'
import firebase from 'react-native-firebase'
import Bugsnag from '@bugsnag/react-native'
import { ApiRequest } from '@httpsRequest';

let dataModel;
class Splash extends React.Component {

  constructor(props) {
    super(props);

    dataModel = DataModel.getInstance();
  }

  componentDidMount() {
    Bugsnag.start();
    this.getAuthorizeToken();
    this.getTestToken();
    this.getLanguage();
    this.checkPermission();
    this.createNotificationListeners();
    this.moveToLogin();
  }

  moveToLogin = () => {
    setTimeout(() => {
      this._bootstrapAsync();
    }, 3000)
  }

  _bootstrapAsync = () => {

    this.props.getUserToken().then(() => {
      this.props.navigation.navigate(this.props.token != null ? 'HomeScreen' : 'Login');

    }).catch(error => {
      this.setState({ error })
    })
  }

  getAuthorizeToken = () => {
    ApiRequest.getAuthorizeToken().then((responseJson) => {
       console.log('response', responseJson);
       dataModel.setAuthToken(responseJson.access_token);
    })

  }

  getTestToken = () => {
    ApiRequest.getTestToken().then((responseJson) => {
       console.log('response', responseJson);
       dataModel.setTestToken(responseJson.access_token);
    })
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await firebase.messaging().getToken();
    console.log('token ', fcmToken);
    if (fcmToken) {
      await AsyncStorage.setItem('fcmWorkToken', fcmToken);

    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */

    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { _data, _notificationId } = notification;

      if (_data) {
        //AsyncStorage.setItem('notificationCount', "1");
        //let x = this.props.state.updateNotificationCountFlag ? this.props.state.updateNotificationCountFlag + 1 : 1;
        //this.props.updateNotificationCount(x);
      }

      alert('Notification received');
      console.log('Notification', notification);
      const action = notification.action
      // Get information about the notification that was opened

      if (Platform.OS == 'android') {
        const newNotification = new firebase.notifications.Notification()
          .android.setChannelId(_data.android_channel_id)
          .setNotificationId(_notificationId)
          .setTitle(_data.title)
          .setBody(_data.body)
          .setSound("default")
          .setData(_data)
          .android.setAutoCancel(true)
          .android.setSmallIcon('ic_launcher')
          .android.setCategory(firebase.notifications.Android.Category.Alarm)


        // Build a channel
        const channelId = new firebase.notifications.Android.Channel(_data.android_channel_id, "TC Workforce", firebase.notifications.Android.Importance.Max);

        // Create the channel
        firebase.notifications().android.createChannel(channelId);
        firebase.notifications().displayNotification(newNotification)
      } else {
        const newNotification = new firebase.notifications.Notification()
          .setNotificationId(_notificationId)
          .setTitle(_data.title)
          .setBody(_data.body)
          .setSound("customnotification.wav")
          .setData(_data);

        firebase.notifications().displayNotification(newNotification)
        .catch((error) => Bugsnag.notify(new Error(error)))
      }

    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { _data } = notificationOpen.notification;

      console.log('NotificationOpen', notificationOpen);
      let action = notificationOpen.action;

      //let title = _data.title;
      let type = _data.type;
      let params = JSON.parse(_data.params);
      //let body = _data.body;

      //Alert.alert('params', JSON.stringify(type) + " " + JSON.stringify(params));
      this.checkPushNotificationType(type, params);

    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { _data } = notificationOpen.notification;

      let action = notificationOpen.action;

      //let title = _data.title;
      let type = _data.type;
      let params = JSON.parse(_data.params);
      //let body = _data.body;

      this.checkPushNotificationType(type, params);

    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      //Alert.alert('message1', JSON.stringify(message));
      console.log('NotificationMessage ', message);
      const { _data, _notificationId } = message;

    });


    this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
      // Process your token as required
      console.log('RefreshToken', fcmToken);
      //AsyncStorage.setItem('refreshToken', fcmToken);
      this.getToken();
    });
  }

  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  checkPushNotificationType = (type, params) => {
    if (type == '7') {
      this.props.navigation.navigate('AcceptRejectTask', { params: params });
    } else {
      this.props.navigation.navigate('HomeScreen');
    }
    /* if (this.props.token == null || this.props.token.length() == 0 || this.props.token == "" || this.props.token == " ") {
      this.props.navigation.navigate('Login');
    } else {
      
    } */

  }

  getLanguage = async () => {
    let language = await AsyncStorage.getItem('language');

    if (language == 'En') {
      let x = this.props.state.setLanguageFlag ? Messages.En : Messages.En;
      this.props.setLanguage(x);
    } else if (language == 'Ru') {
      let x = this.props.state.setLanguageFlag ? Messages.Ru : Messages.Ru;
      this.props.setLanguage(x);
    } else if (language == 'Gr') {
      let x = this.props.state.setLanguageFlag ? Messages.Gr : Messages.Gr;
      this.props.setLanguage(x);
    } else if (language == 'Fr') {
      let x = this.props.state.setLanguageFlag ? Messages.Fr : Messages.Fr;
      this.props.setLanguage(x);
    }
    else {

    }
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ImageBackground source={Icons.background} style={styles.container}>
          <Image source={Icons.logo} style={styles.image} />
        </ImageBackground>
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  image: {
    height: 250,
    width: 250,
    resizeMode: 'contain'
  }
});

const mapStateToProps = (state) => ({
  state: state,
  token: state.token
});

const mapDispatchToProps = (dispatch) => ({
  setLanguage: (data) => {
    dispatch(setLanguage(data));
  },

  getUserToken: () => dispatch(getUserToken()),
});
export default connect(mapStateToProps, mapDispatchToProps)(Splash);