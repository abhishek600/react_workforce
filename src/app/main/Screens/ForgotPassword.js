import React from 'react'
import { SafeAreaView, ImageBackground, Image, StyleSheet, View, Text, TextInput, ScrollView, Alert } from 'react-native'
import { Icons, Colors, Messages, DataModel } from '@common'
import AsyncStorage from '@react-native-community/async-storage'
import { Button, Loader, Header } from '@components'
import { connect } from 'react-redux'
import { ApiRequest } from '@httpsRequest'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Ionicons'
import CountryPicker from 'react-native-country-picker-modal'

let dataModel;
class ForgotPassword extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      phone: '',
      society: '',
      countryCode: 'IN',
    };
    dataModel = DataModel.getInstance();
  }

  componentDidMount() {
    this.getSociety();
  }

  getSociety = async () => {
    let society = await AsyncStorage.getItem('isSocietyUser');
    let country = await AsyncStorage.getItem('country_code');

    if (society) {
      this.setState({ society: society });
    }

    if (country) {
      this.setState({ countryCode: country });
    }
  }

  onValidate = () => {
    let phone = this.state.phone;

    if (phone == '') {
      Alert.alert(Messages.alertMessages.phone)
    } else if (phone.length <= 9) {
      Alert.alert(Messages.alertMessages.validPhone)
    } else {
      this.ForgotPassword();
    }
  }

  // Function which is used for hitting the Forgot Password api
  ForgotPassword = () => {
    let phone = this.state.phone;
    let isSocietyUser = this.state.society;
    let authToken = dataModel.getAuthToken();
    let testToken = dataModel.getTestToken();

    this.setState({ loading: true });

    let params = { "action": "forgotPassword", "phone": phone, "society": isSocietyUser };
    //let params = "&phone=" + phone + "&society=" + isSocietyUser + "&access_token=" + testToken;
    ApiRequest.httpsPost("forgotPassword", params, authToken).then((responseJson) => {
      console.log('response', responseJson);
      if (responseJson.status === 200) {
        this.setState({ loading: false });
        Alert.alert(responseJson.message);
        this.props.navigation.goBack(null);
      } else {
        this.setState({ loading: false });
        Alert.alert(responseJson.message);
      }
    })

  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ height: 150, width: '40%', backgroundColor: Colors.appColor, paddingLeft: 30, paddingTop: 50, borderBottomRightRadius: 150, overflow: 'hidden' }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
              <Icon style={{ color: 'white' }} size={30} name={'arrow-back'} />
            </TouchableOpacity>
          </View>
        </View>

        <SafeAreaView style={styles.container}>
          <Loader
            loading={this.state.loading} />


          <View style={styles.container}>

            <ScrollView keyboardShouldPersistTaps="always">
              {/* <Header
                sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.forgotPassword : Messages.En.forgotPassword}
                isQrCodeData={false}
                backIcon={true}
                goBack={() => this.props.navigation.goBack(null)}
              /> */}


              <View style={{ height: 200, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Icons.forgotIcon} style={styles.image} />
              </View>
              <Text style={{ marginLeft: 10, color: Colors.appColor, fontSize: 20, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.forgotPassword : Messages.En.forgotPassword}</Text>

              <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10 }}>
                <CountryPicker
                  countryCode={this.state.countryCode}
                  withFlag={true}
                  withCallingCode={true}
                  withCallingCodeButton={true}
                  withCountryNameButton={true}
                  countryCodes={this.state.countryModalList}
                  preferredCountries={this.state.countryModalList}
                  onSelect={(country) => this.onCountryPress(country)}
                />

                <TextInput
                  style={[styles.input, { flex: 1 }]}
                  underlineColorAndroid="transparent"
                  onChangeText={(phone) => this.setState({ phone })}
                  maxLength={13}
                  keyboardType={'numeric'}
                  returnKeyType={'done'}
                />
              </View>

              <View style={{ marginTop: 30 }} />
              <Button
                text={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.submit : Messages.En.submit}
                onPress={() => this.onValidate()}
              />
            </ScrollView>

          </View>
        </SafeAreaView>
        <View style={{ flexDirection: 'row-reverse' }}>
          <View style={{ height: 150, width: '40%', backgroundColor: Colors.appColor, borderTopLeftRadius: 150, overflow: 'hidden' }} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  image: {
    height: 100,
    width: 100,
    resizeMode: 'contain'
  },

  input: {
    marginLeft: 10,
    marginRight: 10,
    height: 40,
    paddingLeft: 10,
    paddingTop: 2,
    borderRadius: 10,
    textAlign: 'left',
    borderBottomWidth: 2,
    borderBottomColor: Colors.appColor,
    ...Platform.select({
      ios: {
        marginBottom: 10
      }
    })
  },
});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);