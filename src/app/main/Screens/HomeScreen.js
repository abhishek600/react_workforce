import React from 'react'
import { SafeAreaView, TouchableOpacity, AppState, Text, Image, View, Alert, Platform, NativeModules } from 'react-native'
import { Icons, Colors, Messages, Config, DataModel, NetworkUtils } from '@common'
import { Header, QrCodeModal } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import History from './History'
import OpenTask from './OpenTask'
import AssetInspection from './AssetInspection'
import Notifications from './Notifications'
import { removeUserToken } from '../../actions/actions'
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import io from 'socket.io-client';
import BackgroundJob from 'react-native-background-actions';
import KeepAwake from 'react-native-keep-awake';
import moment from 'moment';
import { ApiRequest } from '@httpsRequest';
import CheckSpeed from './CheckSpeed';
import QRCode from 'react-native-qrcode-generator'
import Icon from 'react-native-vector-icons/Ionicons';

const sleep = time => new Promise(resolve => setTimeout(() => resolve(), time));
const BackgroundTask = NativeModules.BackgroundTask;
let dataModel;
Geocoder.init(Config.google_api_key);

var socketConfig = {
    secure: true,
    rejectUnauthorized: false
}

const socketIOConnOpt = {
    'force new connection': true,
    reconnection: true,
    reconnectionDelay: 10000,
    reconnectionDelayMax: 60000,
    reconnectionAttempts: 'Infinity',
    timeout: 10000,
    transports: ['websocket']
};

class HomeScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isHistory: false,
            isOpenTask: true,
            isNotifications: false,
            isLogout: false,
            isSocietyUser: false,
            society: '',
            societyId: '',
            checkSpeed: '',
            speed: '',
            acceleration: '',
            breakdiff: '',
            userData: [],
            user_id: '',
            qrCodeData: '',
            qrModalVisible: false,
            isOn: false,
            duty: 'Off Duty',
            offlineLocs: [],
            appState: AppState.currentState,
            username: '',
            profilePic: '',
        };
        this.latitude = '',
            this.longitude = '',
            this.backgroundState = false;
        dataModel = DataModel.getInstance()
    }

    componentDidMount() {
        this.socket = io("https://townconnect.in:1431");

        //this.socket = io("http://13.234.121.215:3000");
        this.checkPermission();
        this.getData();
        AppState.addEventListener('change', this._handleAppStateChange);
    }

    taskRandom = async taskData => {
        if (Platform.OS === 'ios') {
            console.warn(
                'This task will not keep your app alive in the background by itself, use other library like react-native-track-player that use audio,',
                'geolocalization, etc. to keep your app alive in the background while you excute the JS from this library.',
            );
        }
        await new Promise(async resolve => {
            // For loop with a delay
            let { delay } = taskData;
            for (let i = 0; BackgroundJob.isRunning(); i++) {
                console.log('Runned -> ', i);
                this.checkPermission();
                await BackgroundJob.updateNotification({ taskDesc: 'Runned -> ' + i });
                await sleep(this.isNetworkConnected ? 5000 : 100000);
            }
        });
    };

    componentWillUnmount() {
        clearTimeout(this.intervalID);
        AppState.removeEventListener('change', this._handleAppStateChange);

    }

    startServiceAndroid = async (timer) => {
        const options = {
            taskName: 'Example',
            taskTitle: 'ExampleTask title',
            taskDesc: 'ExampleTask desc',
            taskIcon: {
                name: 'ic_launcher',
                type: 'mipmap',
            },
            color: '#ff00ff',
            //linkingURI: 'exampleScheme://chat/jane',
            parameters: {
                delay: timer,
            },
        };

        try {
            await BackgroundJob.start(this.taskRandom, options);
            console.log('Successful start!');
        } catch (e) {
            console.log('Error', e);
        }

    }

    stopServiceAndroid = async () => {
        console.log('Stop background service');
        await BackgroundJob.stop();
    }

    _handleAppStateChange = (nextAppState) => {
        console.log('appState', nextAppState);
        let timer = 5000;

        if (this.isNetworkConnected == false)
            timer = 100000;

        if (nextAppState.match(/inactive|background/)) {
            console.log('App is in the background');

            if (Platform.OS == 'android') {
                this.backgroundState = true;
                if (this.state.isOn) {
                    this.startServiceAndroid(timer);
                }

            } else {

                if (this.state.isOn) {
                    console.log('ios task started');
                    BackgroundTask.beginBackgroundTask("gpsTracking", () => {
                        this.getLatLng();
                        this.renderCheckSpeed();
                    })
                }
            }

        }


        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            console.log('App has come to the foreground!');

            if (Platform.OS == 'android') {
                this.backgroundState = false;
                this.stopServiceAndroid();

            } else {
                clearTimeout(this.intervalID);
            }

            //this.init();
        }

        this.setState({ appState: nextAppState });
    }

    renderCheckSpeed = () => {
        let checkSpeed = this.state.checkSpeed;
        let overspeed = this.state.speed;
        let acceleration = this.state.acceleration;
        let breakdiff = this.state.breakdiff;
        return (
            <CheckSpeed overspeed={overspeed} acceleration={acceleration} breakdiff={breakdiff} navigation={this.props.navigation} />
        )
    }

    checkPermission = () => {
        if (Platform.OS == 'android') {
            this.getLocation();
        } else {
            this.getLatLng();
        }
    }

    getLocation = () => {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
            .then((data) => {
                console.log('locationData', data);
                this.getLatLng();

            }).catch((err) => {
                console.log('locationErr', err);
            });
    }

    getLatLng = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                console.log('location updates latitude:- ', position.coords.latitude);
                console.log('longitude:- ', position.coords.longitude);
                //this.getLocationDetails(position.coords.latitude, position.coords.longitude)
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                if (this.state.isOn) {
                    NetworkUtils.isNetworkAvailable().then((response) => {
                        console.log('networkStatus', response);
                        //if (response) {
                        this.isNetworkConnected = true;
                        this.connectSocket();
                        //if (this.state.offlineLocs.length) {
                        //this.offlineTrackingData(this.state.offlineLocs);
                        //}
                        //} else {
                        //   this.isNetworkConnected = false;
                        //  this.socket.off('connect');
                        //   this.socket.off('disconnect');
                        //this.socket.close();
                        //    let locs = [];
                        //    locs.push({ latitude: this.latitude, longitude: this.longitude, time: moment().format('h:mm:ss a') });
                        //   this.setState({ offlineLocs: [...this.state.offlineLocs, ...locs] }, () => {
                        //         console.log('offlineLocs', this.state.offlineLocs);
                        //   });

                        //if(this.state.appState === 'active')
                        /* if (Platform.OS == 'android' && this.backgroundState == true)
                            console.log('no need for interval');
                        else
                            this.intervalID = setTimeout(() => { this.checkPermission(); }, 100000); */
                        //  this.intervalID = setTimeout(() => { this.checkPermission(); }, 500000);
                        // }
                    });

                } else {
                    clearTimeout(this.intervalID);
                }
            },
            (error) => { console.log(error); },
            { enableHighAccuracy: false, timeout: 5000, useSignificantChanges: true }
        )
    }


    offlineTrackingData = (locations) => {
        let labourId = this.state.user_id;
        let society = this.state.society;
        let societyId = this.state.societyId;
        let orderId = dataModel.getOrderId();
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        let params = { "action": "add_offline_tracking", "labourId": labourId, "society": society, "locations": JSON.stringify(locations), "OrderID": orderId, "societyId": societyId };
        //let params = "&labourId=" + labourId + "&society=" + society + "&locations=" + JSON.stringify(locations) + "&OrderID=" + orderId + "&societyId=" + societyId + "&access_token=" + testToken;
        ApiRequest.httpsPost("add_offline_tracking", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ offlineLocs: [] });
            } else {

            }
        })
    }

    connectSocket = () => {
        let locId = dataModel.getOrderId();

        let dataObj = {
            "longitude": this.longitude,
            "latitude": this.latitude,
            "driverid": this.state.user_id,
            "locationid": locId
        };

        console.log('data', dataObj);

        this.socket.on('connect', () => console.log("Socket connected"));

        this.socket.emit("locationdetail", dataObj);

        /* this.socket.on('reconnect_attempt', () => {
            this.socket.io.opts.transports = ['polling', 'websocket'];
          }); */

        this.socket.on('connect_error', (err) => console.log("connect_error", err));
        this.socket.on('error', (err) => console.log("error", err));

        //this.socket.on('disconnect', () => {
        //console.log('socket disconected');
        //})

        if (this.state.isOn) {
            this.intervalID = setTimeout(() => { this.checkPermission(); }, 5000);
            /* if (Platform.OS == 'android' && this.backgroundState == true)
                console.log('no need for interval');
            else
                this.intervalID = setTimeout(() => { this.checkPermission(); }, 5000); */

        } else {
            clearTimeout(this.intervalID);
        }

    }

    toggleButton = () => {
        this.setState({ isOn: !this.state.isOn }, () => {
            this.setState({
                duty: this.state.isOn ? "On Duty" : "Off Duty"
            }, () => {
                this.connectSocket();
            })
        });
    }

    getData = async () => {
        let society = await AsyncStorage.getItem('isSocietyUser')
        let loginData = await AsyncStorage.getItem('loginData')
        let phone = await AsyncStorage.getItem('phone')
        let status = await AsyncStorage.getItem('status')
        let societyId = ""

        if (society) {
            this.setState({ society: society });
        }

        if (society == "1") {
            societyId = await AsyncStorage.getItem('societyID');
            this.setState({ societyId: societyId });
        }

        if (status == "1") {
            this.setState({ isOn: true, duty: 'On Duty' }, () => {
                this.connectSocket();
            })
        } else {
            this.setState({ isOn: false, duty: 'Off Duty' }, () => {
            })
        }

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({
                    user_id: this.state.userData[0].id,
                    checkSpeed: this.state.userData[0].checkSpeed,
                    speed: this.state.userData[0].speed,
                    acceleration: this.state.userData[0].acceleration,
                    breakdiff: this.state.userData[0].breakdiff,
                    username: this.state.userData[0].username,
                    profilePic: this.state.userData[0].profilepic
                }, () => {

                })
            })
        }

        let qrData = this.state.user_id + "-" + phone + "-" + societyId;
        console.log('qrCodeData', qrData);

        this.setState({ qrCodeData: qrData }, () => {
            dataModel.setQrCodeData(qrData);
        })
    }

    _signOutAsync = () => {
        this.props.removeUserToken()
            .then(() => {
                this.props.navigation.navigate('Login');
            })
            .catch(error => {
                this.setState({ error })
            })
    }

    removeAsyncValues = async () => {
        await AsyncStorage.removeItem('currency');
        await AsyncStorage.removeItem('phone');
        await AsyncStorage.removeItem('loginData');
    }

    onTabPressed = (tab) => {

        if (tab === "History") {
            this.setState({ isHistory: true, isOpenTask: false, isNotifications: false, isLogout: false, isSocietyUser: false });
        } else if (tab === "OpenTask") {
            this.setState({ isHistory: false, isOpenTask: true, isNotifications: false, isLogout: false, isSocietyUser: false });
        } else if (tab === "Notifications") {
            this.setState({ isHistory: false, isOpenTask: false, isNotifications: true, isLogout: false, isSocietyUser: false });
        } else if (tab === "Logout") {
            this.setState({ isHistory: false, isOpenTask: false, isNotifications: false, isLogout: true, isSocietyUser: false }, () => {
                //this.logoutAlert();
            });
        } else {
            this.setState({ isHistory: false, isOpenTask: false, isNotifications: false, isLogout: false, isSocietyUser: true });
        }
    }

    logoutAlert = () => {
        Alert.alert(
            this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.logout : Messages.En.logout,
            this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.areYouSure : Messages.En.areYouSure,
            [
                {
                    text: this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.no : Messages.En.no,
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.yes : Messages.En.yes,
                    onPress: () => this.logout()
                },
            ],
            { cancelable: false },
        );
    }

    logout = () => {
        this.removeAsyncValues();
        this._signOutAsync();
    }


    renderTabView() {
        return (
            <View style={{ height: 50, backgroundColor: 'white', paddingBottom: 10, paddingLeft: 10, paddingRight: 10, width: '100%', position: 'absolute', bottom: 0, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

                <TouchableOpacity style={{ alignItems: 'center', flex: 0.2, borderTopColor: this.state.isOpenTask ? Colors.appColor : 'transparent', borderTopWidth: 4 }} activeOpacity={50} onPress={() => this.onTabPressed("OpenTask")}>
                    <Icon style={{ color: this.state.isOpenTask ? Colors.appColor : Colors.darkGray }} size={24} name={'home-outline'} />
                    {/* <Image source={Icons.contract} style={{ height: 24, width: 24, resizeMode: 'contain', tintColor: this.state.isOpenTask ? Colors.appColor : Colors.darkGray }} /> */}
                    {this.state.isOpenTask ?
                        <Text style={{ color: Colors.appColor, fontSize: 10, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.home : Messages.En.home}</Text>
                        :
                        null
                    }

                </TouchableOpacity>

                <TouchableOpacity style={{ alignItems: 'center', flex: 0.2, borderTopColor: this.state.isHistory ? Colors.appColor : 'transparent', borderTopWidth: 4 }} activeOpacity={50} onPress={() => this.onTabPressed("History")}>
                    <Image source={Icons.history} style={{ height: 24, width: 24, resizeMode: 'contain', tintColor: this.state.isHistory ? Colors.appColor : Colors.darkGray }} />
                    {this.state.isHistory ?
                        <Text style={{ color: Colors.appColor, fontSize: 10, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.history : Messages.En.history}</Text>
                        :
                        null
                    }

                </TouchableOpacity>

                <TouchableOpacity style={{ alignItems: 'center', flex: 0.2, borderTopColor: this.state.isNotifications ? Colors.appColor : 'transparent', borderTopWidth: 4 }} activeOpacity={50} onPress={() => this.onTabPressed("Notifications")}>
                    <Image source={Icons.notification} style={{ height: 24, width: 24, resizeMode: 'contain', tintColor: this.state.isNotifications ? Colors.appColor : Colors.darkGray }} />
                    {this.state.isNotifications ?
                        <Text style={{ color: Colors.appColor, fontSize: 10, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.notifications : Messages.En.notifications}</Text>
                        :
                        null
                    }

                </TouchableOpacity>

                {this.state.society == "1" ?
                    <TouchableOpacity style={{ alignItems: 'center', flex: 0.2, borderTopColor: this.state.isSocietyUser ? Colors.appColor : 'transparent', borderTopWidth: 4 }} activeOpacity={50} onPress={() => this.onTabPressed("SocietyUser")}>
                        <Image source={Icons.loan} style={{ height: 24, width: 24, resizeMode: 'contain', tintColor: this.state.isSocietyUser ? Colors.appColor : Colors.darkGray }} />
                        {this.state.isSocietyUser ?
                            <Text style={{ color: Colors.appColor, fontSize: 10, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.assets : Messages.En.assets}</Text>
                            :
                            null
                        }

                    </TouchableOpacity>
                    :
                    null
                }

                <TouchableOpacity style={{ alignItems: 'center', flex: 0.2, borderTopColor: this.state.isLogout ? Colors.appColor : 'transparent', borderTopWidth: 4 }} activeOpacity={50} onPress={() => this.onTabPressed("Logout")}>
                    <Image source={Icons.switch} style={{ height: 24, width: 24, resizeMode: 'contain', tintColor: this.state.isLogout ? Colors.appColor : Colors.darkGray }} />
                    {this.state.isLogout ?
                        <Text style={{ color: Colors.appColor, fontSize: 10, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.logout : Messages.En.logout}</Text>
                        :
                        null
                    }

                </TouchableOpacity>
            </View>
        )
    }

    render() {
        let checkSpeed = this.state.checkSpeed;
        let overspeed = this.state.speed;
        let acceleration = this.state.acceleration;
        let breakdiff = this.state.breakdiff;
        let lat = this.latitude;
        let lng = this.longitude;
        if (this.state.isHistory) {
            return (
                <SafeAreaView style={{ flex: 1 }}>
                    {checkSpeed == "1" && <CheckSpeed overspeed={overspeed} acceleration={acceleration} breakdiff={breakdiff} lat={lat} lng={lng} navigation={this.props.navigation} />}
                    <Header
                        sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.history : Messages.En.history}
                        isQrCodeData={false}
                        backIcon={false}
                    />

                    <History navigation={this.props.navigation} />

                    {this.renderTabView()}

                    <QrCodeModal
                        qrCodeData={this.state.qrCodeData}
                        qrModalVisible={this.state.qrModalVisible}
                        backPress={() => this.setState({ qrModalVisible: false })}
                    />
                </SafeAreaView>

            )
        } else if (this.state.isOpenTask) {
            return (
                <SafeAreaView style={{ flex: 1 }}>
                    {checkSpeed == "1" && <CheckSpeed overspeed={overspeed} acceleration={acceleration} breakdiff={breakdiff} lat={lat} lng={lng} navigation={this.props.navigation} />}
                    <KeepAwake />
                    <Header
                        sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.tasks : Messages.En.tasks}
                        isQrCodeData={false}
                        backIcon={false}
                    />

                    <OpenTask
                        navigation={this.props.navigation}
                        isOn={this.state.isOn}
                        duty={this.state.duty}
                        toggleButton={() => this.toggleButton()}
                    />

                    {this.renderTabView()}

                    <QrCodeModal
                        qrCodeData={this.state.qrCodeData}
                        qrModalVisible={this.state.qrModalVisible}
                        backPress={() => this.setState({ qrModalVisible: false })}
                    />
                </SafeAreaView>
            )
        } else if (this.state.isNotifications) {
            return (
                <SafeAreaView style={{ flex: 1 }}>
                    {checkSpeed == "1" && <CheckSpeed overspeed={overspeed} acceleration={acceleration} breakdiff={breakdiff} lat={lat} lng={lng} navigation={this.props.navigation} />}
                    <Header
                        sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.notifications : Messages.En.notifications}
                        isQrCodeData={false}
                        backIcon={false}
                    />

                    <Notifications />

                    {this.renderTabView()}

                    <QrCodeModal
                        qrCodeData={this.state.qrCodeData}
                        qrModalVisible={this.state.qrModalVisible}
                        backPress={() => this.setState({ qrModalVisible: false })}
                    />
                </SafeAreaView>
            )
        } else if (this.state.isSocietyUser) {
            return (
                <SafeAreaView style={{ flex: 1 }}>
                    {checkSpeed == "1" && <CheckSpeed overspeed={overspeed} acceleration={acceleration} breakdiff={breakdiff} lat={lat} lng={lng} navigation={this.props.navigation} />}
                    <Header
                        sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.assetsInspection : Messages.En.assetsInspection}
                        isQrCodeData={false}
                        backIcon={false}
                    />

                    <AssetInspection navigation={this.props.navigation} />

                    {this.renderTabView()}

                    <QrCodeModal
                        qrCodeData={this.state.qrCodeData}
                        qrModalVisible={this.state.qrModalVisible}
                        backPress={() => this.setState({ qrModalVisible: false })}
                    />
                </SafeAreaView>
            )
        } else {
            return (
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
                        <Header
                            sName={'Profile'}
                            isQrCodeData={false}
                            backIcon={false}
                        />

                        <View style={{ backgroundColor: 'white', marginTop: 10, flex: 1, borderTopRightRadius: 15, borderTopLeftRadius: 15 }}>
                            <View style={{ height: 150, alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={this.state.profilePic != '' ? { uri: this.state.profilePic } : Icons.vendor} style={{ height: 80, width: 80, borderRadius: 40 }} />
                                <Text style={{ marginTop: 5, fontSize: 17, color: '#808080', fontWeight: 'bold' }}>{this.state.username}</Text>
                            </View>

                            <View style={{ height: 1, alignSelf: 'center', width: '96%', borderColor: '#d0d0d0', borderWidth: 1, borderStyle: 'dashed' }} />

                            <View style={{ marginTop: 10, marginLeft: 20, marginRight: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontSize: 17, color: '#808080', fontWeight: 'bold' }}>My QRCode</Text>
                                <TouchableOpacity onPress={() => this.setState({ qrModalVisible: true })}>
                                    <QRCode
                                        value={this.state.qrCodeData}
                                        size={35}
                                        bgColor='black'
                                        fgColor='white' />
                                </TouchableOpacity>
                            </View>

                            <View style={{ marginTop: 10, marginLeft: 20, marginRight: 20, flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.logoutAlert()}>
                                    <Text style={{ fontSize: 17, color: '#808080', fontWeight: 'bold' }}>Logout</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>

                    {this.renderTabView()}

                    <QrCodeModal
                        qrCodeData={this.state.qrCodeData}
                        qrModalVisible={this.state.qrModalVisible}
                        backPress={() => this.setState({ qrModalVisible: false })}
                    />
                </SafeAreaView>
            )
        }

    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
    removeUserToken: () => dispatch(removeUserToken()),
});
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);