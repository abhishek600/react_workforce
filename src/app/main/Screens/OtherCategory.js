import React, { Component } from 'react';
import { SafeAreaView, View, Text, TextInput, Keyboard } from 'react-native';
import { Colors, DataModel } from '@common';
import { Header, Loader, Button } from '@components';
import { connect } from 'react-redux';

class OtherCategory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cityId: this.props.navigation.getParam('cityId'),
            category: '',
        };
    }

    onContinuePress = () => {
        this.props.navigation.navigate('Vendors', { cityId: this.state.cityId });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <Header
                    sName={""}
                    backIcon={true}
                    isQrCodeData={false}
                    goBack={() => this.props.navigation.goBack(null)}
                />

                <TextInput
                    style={{ marginTop: 40, marginLeft: 20, marginRight: 20, height: 100, borderColor: '#808080', borderWidth: 1, borderRadius: 10, padding: 10 }}
                    placeholder={'Tell us about your category'}
                    placeholderTextColor={"#808080"}
                    underlineColorAndroid={'transparent'}
                    multiline={true}
                    onChangeText={(category) => this.setState({ category })}
                />

                <View style={{ marginTop: 40, marginLeft: 20, marginRight: 20 }}>
                    <Button
                        text={'Continue'}
                        onPress={() => this.onContinuePress()}
                    />
                </View>

            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(OtherCategory);