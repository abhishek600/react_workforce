import React, { Component } from 'react';
import { SafeAreaView, View, TouchableOpacity, Text, Image, Alert } from 'react-native';
import { connect } from 'react-redux';
import { Colors } from '@common';
import { Header, Button } from '@components';
import Icon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-picker';

class UploadDocument extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cityId: this.props.navigation.getParam('cityId'),
            image: null
        };
    }

    onContinuePress = () => {
        let image = this.state.image;

        if (image == null) {
            Alert.alert('Please upload Your Identification Document');
            return;
        }

        this.props.navigation.navigate('Vendor', { cityId: this.state.cityId })
    }

    launchCamera = () => {
        let options = {
            quality: 1,
            maxWidth: 150,
            maxHeight: 150,
            allowsEditing: false,
            noData: true,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                //alert(response.customButton);
            } else {
                let source = response;
                console.log('response', JSON.stringify(response));
                this.setState({ image: source });
            }
        });

    }

    GenerateRandomNumber = () => {
        var RandomNumber = Math.floor(Math.random() * 100) + 1;
        return RandomNumber + ".jpg";
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    sName={''}
                    backIcon={true}
                    goBack={() => this.props.navigation.goBack(null)}
                    isQrCodeData={false}
                />

                <Text style={{ marginLeft: 20, marginRight: 20, marginTop: 20, textAlign: 'center', fontWeight: 'bold', fontSize: 17 }}>Upload Identity</Text>
                <TouchableOpacity style={{ marginLeft: 20, marginRight: 20, marginTop: 30, height: 200, width: '90%', borderColor: '#808080', borderWidth: 1, borderRadius: 5, alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }} onPress={() => this.launchCamera()}>
                    {this.state.image == null ?
                        <Icon style={{ color: '#808080' }} size={60} name={'camera-sharp'} />
                        :
                        <Image source={{ uri: this.state.image.uri }} style={{ height: 200, width: 200 }} />
                    }
                </TouchableOpacity>

                <View style={{ marginLeft: 10, marginRight: 10, position: 'absolute', bottom: 30, height: 50, width: '100%', alignSelf: 'center' }}>
                    <Button
                        text={'Continue'}
                        onPress={() => this.onContinuePress()}
                    />
                </View>
            </SafeAreaView>
        )
    }

}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(UploadDocument);