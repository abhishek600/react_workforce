import React from 'react'
import { SafeAreaView, ImageBackground, TouchableOpacity, Image, View, Alert, ScrollView, Platform } from 'react-native'
import { Icons, Messages, DataModel } from '@common'
import { Button, Loader, Header, QrCodeModal } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker';

let dataModel;
class StartAssetInspection extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            photo1: null,
            photo2: null,
            assetId: this.props.navigation.getParam('assetId'),
            assetGroup: this.props.navigation.getParam('assetGroup'),
            startStopStatus: this.props.navigation.getParam('startStopStatus'),
            isMaintenance: this.props.navigation.getParam('isMaintenance'),
            userData: [],
            country: '',
            labourId: '',
            qrModalVisible: false,
        };

        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let loginData = await AsyncStorage.getItem('loginData')

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({
                    country: this.state.userData[0].country_id,
                    labourId: this.state.userData[0].id
                })
            });
        }
    }

    launchCamera = (num) => {
        let options = {
            quality: 1,
            maxWidth: 150,
            maxHeight: 150,
            allowsEditing: false,
            noData: true,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                let source = response;
                console.log('response', JSON.stringify(response));
                if (num === "1") {
                    this.setState({ photo1: source });
                } else {
                    this.setState({ photo2: source });
                }
            }
        });

    }

    GenerateRandomNumber = () => {
        var RandomNumber = Math.floor(Math.random() * 100) + 1;
        return RandomNumber + ".jpg";
    }

    submitAssetInspection = () => {
        let photo1 = this.state.photo1;
        let photo2 = this.state.photo2;
        if (photo1 == null && photo2 == null) {
            Alert.alert('Please select atleast 1 image');
            return;
        }

        let country = this.state.country;
        let labourId = this.state.labourId;
        let assetId = this.state.assetId;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        this.setState({ loading: true });

        let url = "https://townconnect.in/townlabour/webservice/master2.php?";
        let authUrl = "https://townconnect.in/townlabour/webservice/v1.2/workforce.php?access_token=" + authToken;

        let formdata = new FormData();

        formdata.append('action', 'submit_asset_inspection')
        if (photo1 != null) {
            formdata.append('image1', { uri: photo1.uri, name: photo1.fileName != null ? photo1.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo1.type })
        }
        if (photo2 != null) {
            formdata.append('image2', { uri: photo2.uri, name: photo2.fileName != null ? photo2.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo2.type })
        }

        formdata.append('submissionId', '')
        formdata.append('country', country)
        formdata.append('asset_group', '')
        formdata.append('assetId', assetId)
        formdata.append('note', '')
        formdata.append('labourId', labourId)
        formdata.append('StartStopStatus', "1")
        formdata.append('assetId_maintainance', '')

        fetch(authUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data;',
                Accept: "application/json"
            },
            timeout: 5000,
            body: formdata
        })
            .then(response => response.json())
            .then((responseJson) => {
                console.log('startAssetInspection', responseJson);
                if (responseJson.status === 200) {
                    this.setState({ loading: false });
                    this.props.navigation.goBack(null);
                } else {
                    this.setState({ loading: false });
                    Alert.alert(responseJson.message);
                }

            })
            .catch(error => console.log(error))
    }

    render() {
        let qrCodeData = dataModel.getQrCodeData();
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader
                    loading={this.state.loading} />
                <ImageBackground source={Icons.background} style={{ flex: 1 }}>
                    <ScrollView keyboardShouldPersistTaps="always">
                        <Header 
                        sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.start : Messages.En.start} 
                        qrCodeData={qrCodeData}
                        qrCodePress={() => this.setState({ qrModalVisible: true })}
                        onChatPress={() => this.props.navigation.navigate('Chat')}
                        backIcon={true} 
                        goBack={() => this.props.navigation.goBack(null)} 
                        />

                        <View style={{ alignSelf: 'center', marginTop: 20, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => this.launchCamera("1")}>
                                {this.state.photo1 == null ?
                                    <Image source={Icons.imageUpload} style={{ height: 150, width: 150, resizeMode: 'contain' }} />
                                    :
                                    <Image source={{ uri: this.state.photo1.uri }} style={{ height: 150, width: 150 }} />
                                }
                            </TouchableOpacity>
                            <View style={{ width: 10 }} />
                            <TouchableOpacity onPress={() => this.launchCamera("2")}>
                                {this.state.photo2 == null ?
                                    <Image source={Icons.imageUpload} style={{ height: 150, width: 150, resizeMode: 'contain' }} />
                                    :
                                    <Image source={{ uri: this.state.photo2.uri }} style={{ height: 150, width: 150 }} />
                                }
                            </TouchableOpacity>
                        </View>

                    </ScrollView>
                    <View style={{ position: 'absolute', bottom: 10, width: '100%' }}>
                        <Button
                            text={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.start : Messages.En.start}
                            onPress={() => this.submitAssetInspection()}
                        />
                    </View>
                </ImageBackground>
                <QrCodeModal
                    qrCodeData={qrCodeData}
                    qrModalVisible={this.state.qrModalVisible}
                    backPress={() => this.setState({ qrModalVisible: false })}
                />
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(StartAssetInspection);