import React, { Component } from 'react';
import { SafeAreaView, View, ImageBackground, StyleSheet, Image, Platform, TextInput, TouchableOpacity, Alert, KeyboardAvoidingView, Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { Header, Loader, Button } from '@components';
import { Colors, Icons, Messages, DataModel } from '@common';
import { ScrollView } from 'react-native-gesture-handler';
import { ApiRequest } from '@httpsRequest';

let dataModel;
class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            password: '',
            repeatPassword: '',
            isHidden: true,
            isHidden1: true,
            countryModalList: ["IN", "AE", "SO", "NG", "GH", "ZA", "MY", "KE", "RW", "BW", "CM", "ZM", "MX", "UG"],
            countryCode: 'IN',
            country: '',
        };
        dataModel = DataModel.getInstance();
    }

    onValidate = () => {
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;
        let email = this.state.email;
        let phone = this.state.phone;
        let password = this.state.password;
        let repeatPassword = this.state.repeatPassword;

        if (firstName == '') {
            Alert.alert('Please enter First Name');
        } else if (lastName == '') {
            Alert.alert('Please enter Last Name');
        } else if (email == '') {
            Alert.alert('Please enter Email');
        } else if (phone == '') {
            Alert.alert('Please enter Phone Number');
        } else if (phone.length <= 8) {
            Alert.alert('Phone Number is not Valid');
        } else if (password == '') {
            Alert.alert('Please enter Password');
        } else if (repeatPassword == '') {
            Alert.alert('Please enter Repeat Password');
        } else if (password != repeatPassword) {
            Alert.alert('Password and Repeat Password should be same');
        } else {
            this.register();
        }
    }

    register = () => {
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;
        let email = this.state.email;
        let phone = this.state.phone;
        let password = this.state.password;
        let repeatPassword = this.state.repeatPassword;
        let authToken = dataModel.getAuthToken();
        this.setState({ loading: true });

        let params = { "action": "workforceSignup", "firstname": firstName, "lastname": lastName, "email": email, "phone": phone, "password": password };
        ApiRequest.httpsPost("workforceSignup", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ loading: false });
                this.props.navigation.navigate('SelectCountry');
            } else {
                this.setState({ loading: false });
            }
        })
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <Loader
                    loading={this.state.loading}
                />

                <ImageBackground source={Icons.background} style={styles.container}>

                    <Header
                        sName={"Sign Up"}
                        isQrCodeData={false}
                        backIcon={true}
                        goBack={() => this.props.navigation.goBack(null)}
                    />

                    <ScrollView keyboardShouldPersistTaps="handled">
                        <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'padding' : null}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={Icons.logo} style={{ height: 200, width: 200, resizeMode: 'contain' }} />
                            </View>

                            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={[styles.input, { flex: 0.49 }]}>
                                    <TextInput
                                        style={{ flex: 1 }}
                                        placeholder={'First Name'}
                                        placeholderTextColor={Colors.darkGray}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(firstName) => this.setState({ firstName })}
                                    />
                                </View>

                                <View style={[styles.input, { flex: 0.49 }]}>
                                    <TextInput
                                        style={{ flex: 1 }}
                                        placeholder={'Last Name'}
                                        placeholderTextColor={Colors.darkGray}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(lastName) => this.setState({ lastName })}
                                    />
                                </View>
                            </View>

                            <View style={[styles.input, { marginLeft: 20, marginRight: 20, marginTop: 10 }]}>
                                <TextInput
                                    style={{ flex: 1 }}
                                    placeholder={'Email'}
                                    placeholderTextColor={Colors.darkGray}
                                    underlineColorAndroid="transparent"
                                    autoCapitalize={'none'}
                                    onChangeText={(email) => this.setState({ email })}
                                />
                            </View>

                            <View style={[styles.input, { marginLeft: 20, marginRight: 20, marginTop: 10 }]}>
                                <TextInput
                                    style={{ flex: 1 }}
                                    placeholder={'Phone'}
                                    placeholderTextColor={Colors.darkGray}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(phone) => this.setState({ phone })}
                                    maxLength={13}
                                    keyboardType={'numeric'}
                                />
                            </View>

                            <View style={[styles.input, { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20, marginTop: 10 }]}>
                                <TextInput
                                    style={{ flex: 1 }}
                                    placeholder={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.password : Messages.En.password}
                                    placeholderTextColor={Colors.darkGray}
                                    underlineColorAndroid="transparent"
                                    secureTextEntry={this.state.isHidden}
                                    blurOnSubmit={false}
                                    onChangeText={(password) => this.setState({ password })}
                                    onSubmitEditing={() => Keyboard.dismiss()}
                                />
                                <TouchableOpacity onPress={() => this.setState({ isHidden: !this.state.isHidden })}>
                                    <Image source={this.state.isHidden ? Icons.hidden : Icons.eye} style={{ width: 24, height: 24 }} />
                                </TouchableOpacity>
                            </View>

                            <View style={[styles.input, { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20, marginTop: 10 }]}>
                                <TextInput
                                    style={{ flex: 1 }}
                                    placeholder={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.repeatPassword : Messages.En.repeatPassword}
                                    placeholderTextColor={Colors.darkGray}
                                    underlineColorAndroid="transparent"
                                    secureTextEntry={this.state.isHidden1}
                                    blurOnSubmit={false}
                                    onChangeText={(repeatPassword) => this.setState({ repeatPassword })}
                                    onSubmitEditing={() => Keyboard.dismiss()}
                                />
                                <TouchableOpacity onPress={() => this.setState({ isHidden1: !this.state.isHidden1 })}>
                                    <Image source={this.state.isHidden1 ? Icons.hidden : Icons.eye} style={{ width: 24, height: 24 }} />
                                </TouchableOpacity>
                            </View>

                            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 30 }}>
                                <Button
                                    text={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.signup : Messages.En.signup}
                                    onPress={() => this.onValidate()}
                                />
                            </View>


                        </KeyboardAvoidingView>
                    </ScrollView>

                </ImageBackground>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    input: {
        marginLeft: 10,
        marginRight: 10,
        height: 40,
        padding: 1,
        paddingLeft: 9,
        borderRadius: 10,
        textAlign: 'left',
        borderBottomWidth: 2,
        borderBottomColor: Colors.appColor,
        ...Platform.select({
            ios: {
                marginBottom: 10
            }
        })
    },

});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);