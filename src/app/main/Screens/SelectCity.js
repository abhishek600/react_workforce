import React, { Component } from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, Image, Alert, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { Colors, Icons, Messages, DataModel } from '@common';
import { Loader } from '@components';
import { ApiRequest } from '@httpsRequest';
import Icon from 'react-native-vector-icons/Ionicons';

let dataModel;
class SelectCity extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            countryCode: this.props.navigation.getParam('country_code'),
            cityList: [],
        };
        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getCountryCity();
    }

    getCountryCity = () => {
        let country_code = this.state.countryCode;
        let authToken = dataModel.getAuthToken();
        this.setState({ loading: true });

        let params = { "action": 'get_country_city', "country_code": country_code };
        ApiRequest.httpsPost('get_country_city', params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                let arr = [];

                if (responseJson.result.length) {
                    for (let p in responseJson.result) {
                        arr.push({
                            cityId: responseJson.result[p].cityId,
                            cityName: responseJson.result[p].cityName,
                            is_selected: false
                        })
                    }
                }

                this.setState({
                    loading: false,
                    cityList: arr
                });
            } else {
                this.setState({ loading: false });
            }
        });
    }

    toggleIndex = (index) => {
        let data = this.state.cityList.slice(0);

        for (i = 0; i < data.length; i++) {
            data[i].is_selected = false;
        }

        data[index].is_selected = !data[index].is_selected;
        this.setState({ cityList: data });
    }

    onContinuePress = () => {
        let selectedCity = this.state.cityList.filter((v, i) => v.is_selected);

        if (!selectedCity.length) {
            Alert.alert('', "Please select city");
            return;
        }

        console.log('cityData', selectedCity);

        //AsyncStorage.setItem('countryData', JSON.stringify(selectedCountry));
        this.props.navigation.navigate('SelectCategory', { cityId: selectedCity[0].cityId });
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={{ padding: 10, flexDirection: 'row', borderBottomColor: '#F2F2F2', borderBottomWidth: 2, justifyContent: 'space-between', alignItems: 'center' }} onPress={() => this.toggleIndex(index)}>
                <Text style={{ fontSize: 15 }}>{item.cityName}</Text>
                <TouchableOpacity style={{ height: 20, width: 20, backgroundColor: this.state.cityList[index].is_selected ? Colors.appColor : 'transparent', alignItems: 'center', justifyContent: 'center', borderColor: 'black', borderWidth: 1 }} onPress={() => this.toggleIndex(index)}>
                    {this.state.cityList[index].is_selected ?
                        <Image source={Icons.tick} style={{ height: 16, width: 16, resizeMode: 'contain' }} />
                        :
                        null
                    }

                </TouchableOpacity>
            </TouchableOpacity>
        )
    }

    render() {
        if (!this.state.cityList.length) {
            return (
                <SafeAreaView style={{ flex: 1 }}>
                    <TouchableOpacity style={{ marginTop: 20, marginLeft: 20, height: 40, width: 40, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack(null)}>
                      <Icon style={{ color: 'black' }} size={30} name={'arrow-back'} />
                    </TouchableOpacity>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontWeight: 'bold' }}>No City Available</Text>
                    </View>
                </SafeAreaView>
            )
        }

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader
                    loading={this.state.loading}
                />
                <View>
                    <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableOpacity style={{ height: 40, width: 40, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center' }} onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{ color: 'black' }} size={30} name={'arrow-back'} />
                        </TouchableOpacity>
                        <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold', color: 'black' }}>{'Select City'}</Text>
                        <View />
                    </View>

                    <View style={{ height: '80%', marginLeft: 10, marginRight: 10, marginTop: 20, marginBottom: 10, borderWidth: 1, borderColor: Colors.gray, backgroundColor: 'white' }}>
                        <FlatList
                            data={this.state.cityList}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <TouchableOpacity style={{ height: 50, width: 140, marginTop: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 5, alignSelf: 'center', backgroundColor: Colors.appColor }} onPress={() => this.onContinuePress()}>
                        <Text style={{ fontSize: 15, color: 'white' }}>{'Continue'}</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(SelectCity);