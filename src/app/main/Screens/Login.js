import React from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, TouchableOpacity, Text, Image, View, TextInput, Alert, ScrollView, Modal, FlatList, Platform, Keyboard } from 'react-native'
import { Icons, Colors, Messages, DataModel, Config } from '@common'
import { Button, Loader } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { setLanguage, saveUserToken } from '../../actions/actions'
import CountryPicker from 'react-native-country-picker-modal'
import { ApiRequest } from '@httpsRequest'
import Icon from 'react-native-vector-icons/Ionicons';

let dataModel;
class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      isHidden: true,
      token: '',
      countryModalList: ["IN", "AE", "SO", "NG", "GH", "ZA", "MY", "KE", "RW", "BW", "CM", "ZM", "MX", "UG"],
      countryCode: 'IN',
      country: '',
      phone: '',
      password: '',
      isSocietyUser: false,
      isLanguageModalVisible: false,
      isDelivery: '0',
      userInfo: {},
      googleUserInfo: {},
      Languages: [
        {
          id: '1',
          language: 'English',
          isSelected: true
        },
        {
          id: '2',
          language: 'Russia',
          isSelected: false
        },
        {
          id: '3',
          language: 'German',
          isSelected: false
        },
        {
          id: '4',
          language: 'French',
          isSelected: false
        },
        {
          id: '5',
          language: 'Kinyarwanda',
          isSelected: false
        },
        {
          id: '6',
          language: 'Swahili',
          isSelected: false
        },
        {
          id: '7',
          language: 'Spanish',
          isSelected: false
        }
      ],
    };

    AsyncStorage.setItem('isSocietyUser', "0");
    dataModel = DataModel.getInstance();
  }

  componentDidMount() {
    /* GoogleSignin.configure({
      webClientId: Config.WEB_CLIENT_ID,
      offlineAccess: true,
      hostedDomain: '',
      loginHint: '',
      forceConsentPrompt: true,
    }); */

    this.getCountryCode();
  }

  getCountryCode = async () => {
    let token = await AsyncStorage.getItem('fcmWorkToken');
    let country = await AsyncStorage.getItem('country_code');

    if (token) {
      this.setState({ token: token });
    }

    if (country) {
      this.setState({ countryCode: country });
    }
  }

  onValidate = () => {
    let phone = this.state.phone;
    let password = this.state.password;

    if (phone == '') {
      Alert.alert(Messages.alertMessages.phone);
    } else if (phone.length <= 9) {
      Alert.alert(Messages.alertMessages.validPhone);
    } else if (password == '') {
      Alert.alert(Messages.alertMessages.password);
    } else {
      this.Login();
    }
  }

  /* getInfoFromToken = token => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id,name,email,first_name,last_name',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      { token, parameters: PROFILE_REQUEST_PARAMS },
      (error, user) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {

          this.setState({ userInfo: user }, () => {
            this.facebookLoginService(user.id, user.email, user.name);
            //this.props.navigation.navigate('SelectCountry');
          });
          console.log('FacebookData ', user);

        }

      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  }; */

  /* loginWithFacebook = () => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      login => {
        if (login.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            const accessToken = data.accessToken.toString();
            this.getInfoFromToken(accessToken);
          });
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      },
    );
  }; */

  /* _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const googleUserInfo = await GoogleSignin.signIn();
      let user = googleUserInfo.user;
      this.setState({ googleUserInfo: googleUserInfo, loggedIn: true }, () => {
        console.log('googleData', user);
        this.googleLoginService(user.email, user.name, user.id);
        //this.props.navigation.navigate('SelectCountry');
      });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('sign in cancelled')
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('play services not available');
      } else {
        // some other error happened
        console.log('Something else went wrong...', error.toString());
      }
    }
  }; */

  googleLoginService = (email, name, id) => {
    let deviceId = this.state.token;
    let authToken = dataModel.getAuthToken();

    this.setState({ loading: true });
    let params = { "action": 'gmailLogin', "name": name, "gmailId": id, "email": email, "deviceId": deviceId };
    ApiRequest.httpsPost('gmailLogin', params, authToken).then((responseJson) => {
      console.log("response", responseJson);
      if (responseJson == undefined) {
        this.setState({ loading: false });
        Alert.alert('Something went wrong');
      }

      if (responseJson.status === 200) {
        this.setState({ loading: false });
        this.props.navigation.navigate('SelectCountry');
      } else {
        this.setState({ loading: false });
      }
    });
  }

  facebookLoginService = (id, email, name) => {
    let deviceId = this.state.token;
    let authToken = dataModel.getAuthToken();

    this.setState({ loading: true });
    let params = { "action": 'fbLogin', "name": name, "fbId": id, "email": email, "deviceId": deviceId };
    ApiRequest.httpsPost('fbLogin', params, authToken).then((responseJson) => {
      console.log('response', responseJson);
      if (responseJson == undefined) {
        this.setState({ loading: false });
        Alert.alert('Something went wrong');
      }

      if (responseJson.status === 200) {
        this.setState({ loading: false });
        this.props.navigation.navigate('SelectCountry');
      } else {
        this.setState({ loading: false });
      }
    });
  }

  appleLoginService = (id, email, name) => {
    let deviceId = this.state.token;
    let authToken = dataModel.getAuthToken();

    this.setState({ loading: true });
    let params = { "action": 'appleLogin', "name": name, "appleIs": id, "email": email, "deviceId": deviceId };
    ApiRequest.httpsPost('appleLogin', params, authToken).then((responseJson) => {
      console.log('response', responseJson);
      if (responseJson == undefined) {
        this.setState({ loading: false });
        Alert.alert('Something went wrong');
      }

      if (responseJson.status === 200) {
        this.setState({ loading: false });
        this.props.navigation.navigate('SelectCountry');
      } else {
        this.setState({ loading: false });
      }
    });
  }

  onSocietyChecked = () => {
    this.setState({ isSocietyUser: !this.state.isSocietyUser }, () => {
      let isSociety = this.state.isSocietyUser ? "1" : "0";
      AsyncStorage.setItem('isSocietyUser', isSociety);
    })
  }

  onToogleLanguagePress = (index) => {
    let language = this.state.Languages.slice(0);

    for (let i = 0; i < language.length; i++) {
      language[i].isSelected = false;
    }
    language[index].isSelected = !language[index].isSelected;

    this.setState({ Languages: language });
  }

  setLanguage = async (value) => {
    await AsyncStorage.setItem('language', value);
  }

  onSavePressed = () => {
    let data = this.state.Languages.filter((v, i) => v.isSelected);

    this.setState({ isLanguageModalVisible: false })

    if (data[0].id === "1") {
      let x = this.props.state.setLanguageFlag ? Messages.En : Messages.En;
      this.props.setLanguage(x);
      this.setLanguage('En');
    } else if (data[0].id === "2") {
      let x = this.props.state.setLanguageFlag ? Messages.Ru : Messages.Ru;
      this.props.setLanguage(x);
      this.setLanguage('Ru');
    } else if (data[0].id === "3") {
      let x = this.props.state.setLanguageFlag ? Messages.Gr : Messages.Gr;
      this.props.setLanguage(x);
      this.setLanguage('Gr');
    } else if (data[0].id === "4") {
      let x = this.props.state.setLanguageFlag ? Messages.Fr : Messages.Fr;
      this.props.setLanguage(x);
      this.setLanguage('Fr');
    } else if (data[0].id === "5") {
      let x = this.props.state.setLanguageFlag ? Messages.Kn : Messages.Kn;
      this.props.setLanguage(x);
      this.setLanguage('Kn');
    } else if (data[0].id === "6") {
      let x = this.props.state.setLanguageFlag ? Messages.Sw : Messages.Sw;
      this.props.setLanguage(x);
      this.setLanguage('Sw');
    } else if (data[0].id === "7") {
      let x = this.props.state.setLanguageFlag ? Messages.Sp : Messages.Sp;
      this.props.setLanguage(x);
      this.setLanguage('Sp');
    }
    else {

    }

  }

  onCountryPress = (country) => {
    this.setState({ countryCode: country.cca2, callingCode: country.callingCode[0] })
  }

  _saveUserToken = () => {
    this.props.saveUserToken()
      .then(() => {
        this.props.navigation.push('HomeScreen');
      })
      .catch(error => {
        this.setState({ error })
      })
  };

  renderLanguageItem = ({ item, index }) => {
    return (
      <View>
        <TouchableOpacity style={{ height: 50, marginLeft: 20, marginRight: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} onPress={() => this.onToogleLanguagePress(index)}>
          <Text style={{ color: Colors.appColor }}>{item.language}</Text>
          <View style={{ flexDirection: 'row-reverse', alignItems: 'center' }}>
            <TouchableOpacity style={{ backgroundColor: this.state.Languages[index].isSelected ? Colors.appColor : 'transparent', height: 20, width: 20, borderColor: Colors.appColor, borderWidth: 2 }} onPress={() => this.onToogleLanguagePress(index)}>
              {this.state.Languages[index].isSelected ?
                <Image source={Icons.tick} style={{ height: 17, width: 17, resizeMode: 'contain' }} />
                :
                null
              }
            </TouchableOpacity>
          </View>
        </TouchableOpacity>

        <View style={{ borderBottomColor: Colors.appColor, borderBottomWidth: 1, marginLeft: 10, marginRight: 10 }}></View>
      </View>
    )
  }

  /* async appleLogin() {
    try {
      // performs login request
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: AppleAuthRequestOperation.LOGIN,
        requestedScopes: [
          AppleAuthRequestScope.EMAIL,
          AppleAuthRequestScope.FULL_NAME,
        ],
      });

      //const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);

      /* if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
       let id = appleAuthRequestResponse.authorizationCode;
       let email = appleAuthRequestResponse.email;
       let fullName = appleAuthRequestResponse.fullName;
       let name = fullName.givenName + " " + fullName.familyName;

       this.appleLoginService(id, email, name);
     }  */

  /*     if (appleAuthRequestResponse['realUserStatus']) {
        let id = appleAuthRequestResponse.authorizationCode;
        let email = appleAuthRequestResponse.email;
        let fullName = appleAuthRequestResponse.fullName;
        let name = fullName.givenName + " " + fullName.familyName;

        //this.props.navigation.navigate('SelectCountry');
        this.appleLoginService(id, email, name);
      }

    } catch (error) {
      if (error.code === AppleAuthError.CANCELED) {
        console.log('Error => ', error);
      }
      if (error.code === AppleAuthError.FAILED) {
        console.log('Failed', 'Touch ID wrong');
      }
      if (error.code === AppleAuthError.INVALID_RESPONSE) {
        console.log('Invalid_Response', 'Touch ID wrong');
      }
      if (error.code === AppleAuthError.NOT_HANDLED) {
      }
      if (error.code === AppleAuthError.UNKNOWN) {
        console.log('Unknown', 'Touch ID wrong');
      }
    }
  } */

  // Function which is used for hitting the login api
  Login = () => {
    Keyboard.dismiss();
    
    let phone = this.state.phone;
    let password = this.state.password;
    let isDelivery = this.state.isDelivery;
    let isSocietyUser = this.state.isSocietyUser ? "1" : "0";
    let token = this.state.token;
    let authToken = dataModel.getAuthToken();
    let testToken = dataModel.getTestToken();

    this.setState({ loading: true });

    let params = { "action": "login", "deviceId": token, "phone": phone, "password": password, "society": isSocietyUser, "isDelivery": isDelivery, "deviceType": "1" };
    //let params = "&deviceId=" + token + "&phone=" + phone + "&password=" + password + "&society=" + isSocietyUser + "&isDelivery=" + isDelivery + "&deviceType=1&access_token=" + testToken;
    ApiRequest.httpsPost("login", params, authToken).then((responseJson) => {
      console.log('response', responseJson);
      if (responseJson.status === 200) {
        this.setState({ loading: false });
        console.log("country", responseJson.result[0].country_id);
        switch (responseJson.result[0].country_id) {
          case '1':
            AsyncStorage.setItem('currency', "Rs");
            break;
          case '2':
            AsyncStorage.setItem('currency', "Rp");
            break;
          case '3':
            AsyncStorage.setItem('currency', "$");
            break;
          case '4':
            AsyncStorage.setItem('currency', "AED");
            break;
          case '5':
            AsyncStorage.setItem('currency', "₦");
            break;
          case '6':
            AsyncStorage.setItem('currency', "GH¢");
            break;
          case '7':
            AsyncStorage.setItem('currency', "R");
            break;
          case '8':
            AsyncStorage.setItem('currency', "RM");
            break;
          case '9':
            AsyncStorage.setItem('currency', "KSh");
            break;
          case '10':
            AsyncStorage.setItem('currency', "FCFA");
            break;
          case '11':
            AsyncStorage.setItem('currency', "ZMW");
            break;
          case '12':
            AsyncStorage.setItem('currency', "FRw");
            break;
          case '14':
            AsyncStorage.setItem('currency', "MXN");
            break;
          case '15':
            AsyncStorage.setItem('currency', "USh");
            break;
          default:
        }
        if (this.state.isSocietyUser) {
          AsyncStorage.setItem('societyID', responseJson.result[0].societyId);
        }
        AsyncStorage.setItem('phone', phone);
        AsyncStorage.setItem('status', responseJson.result[0].onlinestatus);
        AsyncStorage.setItem('loginData', JSON.stringify(responseJson.result));
        this._saveUserToken();
      } else {
        this.setState({ loading: false });
        Alert.alert('email and password is not correct.');
      }
    })

  }

  render() {
    let borderColor = this.state.isSocietyUser ? 'transparent' : Colors.darkGray;
    let backgroundColor = this.state.isSocietyUser ? Colors.appColor : 'transparent';

    return (
      <View style={{ flex: 1 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginRight: 10 }}>

          <View style={{ height: 150, width: '40%', backgroundColor: Colors.appColor, borderBottomRightRadius: 150, overflow: 'hidden' }} />

          <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.setState({ isLanguageModalVisible: true })}>
            <Text style={{ color: Colors.appColor, fontSize: 16, fontWeight: '500' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.changeLanguage : Messages.En.changeLanguage}</Text>
            <Image source={Icons.language} style={{ height: 24, width: 24,  marginLeft: 5, resizeMode: 'contain' }} />
          </TouchableOpacity>
        </View>
        <SafeAreaView style={styles.container}>
          <Loader
            loading={this.state.loading} />

          <View style={styles.container}>
            <ScrollView keyboardShouldPersistTaps="always">


              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Image source={Icons.logo} style={{ height: 140, width: 250, resizeMode: 'contain' }} />
              </View>

              <Text style={{ marginTop: 20, marginLeft: 10, color: Colors.appColor, fontWeight: 'bold', fontSize: 20 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.login : Messages.En.login}</Text>

              <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10 }}>
                <CountryPicker
                  countryCode={this.state.countryCode}
                  withFlag={true}
                  withCallingCode={true}
                  withCallingCodeButton={true}
                  withCountryNameButton={true}
                  countryCodes={this.state.countryModalList}
                  preferredCountries={this.state.countryModalList}
                  onSelect={(country) => this.onCountryPress(country)}
                />

                <TextInput
                  style={[styles.input, { flex: 1 }]}
                  underlineColorAndroid="transparent"
                  onChangeText={(phone) => this.setState({ phone })}
                  maxLength={13}
                  keyboardType={'numeric'}
                  returnKeyType={'done'}
                />
              </View>

              <View style={[styles.input, { flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }]}>
                <TextInput
                  style={{ flex: 1 }}
                  placeholder={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.password : Messages.En.password}
                  underlineColorAndroid="transparent"
                  secureTextEntry={this.state.isHidden}
                  onChangeText={(password) => this.setState({ password })}
                />

                <TouchableOpacity onPress={() => this.setState({ isHidden: !this.state.isHidden })}>
                  <Image source={this.state.isHidden ? Icons.hidden : Icons.eye} style={{ width: 24, height: 24 }} />
                </TouchableOpacity>
              </View>

              {/* <View style={{ marginTop: 10, alignItems: 'center' }}>
                <Text style={{ color: Colors.appColor, fontWeight: 'bold' }} onPress={() => this.props.navigation.navigate('SignUp')}>Create an Account?</Text>
              </View> */}

              <View style={{ marginTop: 10, padding: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <TouchableOpacity style={{ height: 20, width: 20, borderColor: borderColor, backgroundColor: backgroundColor, borderWidth: 2, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onSocietyChecked()}>
                    {this.state.isSocietyUser ?
                      <Image source={Icons.tick} style={{ height: 18, width: 18, borderRadius: 9 }} />
                      :
                      null
                    }
                  </TouchableOpacity>
                  <Text style={{ marginLeft: 10, color: Colors.appColor, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.societyUser : Messages.En.societyUser}</Text>
                </View>

                <Text style={{ marginLeft: 10, color: Colors.appColor, fontWeight: 'bold' }} onPress={() => this.props.navigation.navigate('ForgotPassword')}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.forgotPassword : Messages.En.forgotPassword}?</Text>
              </View>

              <View style={{ marginTop: 10 }} />

              <Button
                text={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.login : Messages.En.login}
                onPress={() => this.onValidate()}
              />

              {/* <Text style={{ marginTop: 50, alignSelf: 'center', color: 'black', fontSize: 18, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.signinWith : Messages.En.signinWith}</Text> */}

              {/* Platform.OS == 'ios' ?
              <AppleButton
                buttonStyle={AppleButton.Style.BLACK}
                buttonType={AppleButton.Type.SIGN_IN}
                onPress={() => this.appleLogin()}
                style={{ width: '60%', height: 45, margin: 10, borderRadius: 20, overflow: 'hidden', alignSelf: 'center', marginTop: 30 }}
              />
              :
              null */
              }

              {/* <TouchableOpacity style={{ flexDirection: 'row', marginTop: 10, padding: 5, alignItems: 'center', justifyContent: 'center', alignSelf: 'center', backgroundColor: Colors.fbButtonColor, borderRadius: 20, height: 40, width: '60%' }} onPress={() => this.loginWithFacebook()}>
              <Image source={Icons.facebook} style={{ height: 18, width: 18, resizeMode: 'contain' }} />
              <Text style={{ color: 'white', marginLeft: 2, fontSize: 15, fontWeight: 'bold', width: 160 }}>Login with Facebook</Text>
              <View />
            </TouchableOpacity>

            <TouchableOpacity style={{ flexDirection: 'row', marginTop: 20, padding: 5, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', backgroundColor: Colors.googleButtonColor, borderRadius: 20, height: 40, width: '60%' }} onPress={() => this._signIn()}>
              <Image source={Icons.google} style={{ height: 18, width: 18, resizeMode: 'contain' }} />
              <Text style={{ color: 'white', marginLeft: 2, fontSize: 15, fontWeight: 'bold', width: 160 }}>Login with Google+</Text>
              <View />
            </TouchableOpacity> */}

            </ScrollView>

          </View>

          <Modal
            animationType={"fade"}
            transparent={true}
            visible={this.state.isLanguageModalVisible}
            onRequestClose={() => { console.log("Modal has been closed.") }}>
            {/*All views of Modal*/}
            <View style={styles.modalBackground}>
              <View style={styles.modal}>

                <View style={{ height: 50, width: '100%', backgroundColor: Colors.white, borderTopLeftRadius: 10, borderTopRightRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ color: Colors.appColor, fontSize: 15, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.changeLanguage : Messages.En.changeLanguage}</Text>
                </View>

                <FlatList
                  data={this.state.Languages}
                  renderItem={this.renderLanguageItem}
                  keyExtractor={item => item.id}
                />

                <Button
                  text={'SAVE'}
                  onPress={() => this.onSavePressed()}
                />
                <View style={{ height: 10 }} />
              </View>
            </View>

          </Modal>
        </SafeAreaView>
        <View style={{ flexDirection: 'row-reverse' }}>
          <View style={{ height: 150, width: '40%', backgroundColor: Colors.appColor, borderTopLeftRadius: 150, overflow: 'hidden' }} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  input: {
    marginLeft: 10,
    marginRight: 10,
    height: 40,
    padding: 1,
    paddingLeft: 9,
    borderRadius: 10,
    textAlign: 'left',
    borderBottomWidth: 2,
    borderBottomColor: Colors.appColor,
    ...Platform.select({
      ios: {
        marginBottom: 10
      }
    })
  },

  modal: {
    backgroundColor: 'white',
    height: 420,
    width: 350,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',

  },

  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },

  pacman: {
    width: 0,
    height: 0,
    borderTopWidth: 60,
    borderTopColor: "red",
    borderLeftColor: "red",
    borderLeftWidth: 60,
    borderRightColor: "transparent",
    borderRightWidth: 60,
    borderBottomColor: "red",
    borderBottomWidth: 60,
    borderTopLeftRadius: 60,
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
    borderBottomLeftRadius: 60,
    transform: [{ rotate: "210deg" }],
  },
});


const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
  setLanguage: (data) => {
    dispatch(setLanguage(data));
  },

  saveUserToken: () => dispatch(saveUserToken()),
});


export default connect(mapStateToProps, mapDispatchToProps)(Login);

