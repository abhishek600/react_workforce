import React, { Component } from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { Colors, DataModel, Icons, Messages } from '@common';
import { Header, Loader, Button } from '@components';
import { connect } from 'react-redux';
import { ApiRequest } from '@httpsRequest';

let dataModel;
class Vendors extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            cityId: this.props.navigation.getParam('cityId'),
            vendorList: [],
            errorMsg: 'Thanks for reaching out to us and showing your interest to become a vendor with us. We will reach out to you in 5 business days to discuss further.',
            successMsg: '',
        };
        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getCityVendors();
    }

    getCityVendors = () => {
        let cityId = this.state.cityId;
        let authToken = dataModel.getAuthToken();
        this.setState({ loading: true });

        let params = { "action": 'get_city_vendors', "cityId": cityId };
        ApiRequest.httpsPost("get_city_vendors", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                let arr = [];

                if (responseJson.result.length) {
                    for (let p in responseJson.result) {
                        arr.push({
                            vendorName: responseJson.result[p].vendorName,
                            vendoraddress: responseJson.result[p].vendoraddress,
                            is_selected: false
                        });
                    }
                    this.setState({
                        loading: false,
                        vendorList: arr
                    });
                }

            } else if (responseJson.status === 400) {
                this.setState({
                    loading: false,
                    //errorMsg: responseJson.message
                });
            } else {
                this.setState({
                    loading: false,
                    //errorMsg: responseJson.message
                });
            }
        });
    }

    toggleIndex = (index) => {
        let data = this.state.vendorList.slice(0);

        for (let i = 0; i < data.length; i++) {
            data[i].is_selected = false;
        }

        data[index].is_selected = !data[index].is_selected;

        this.setState({ vendorList: data });
    }

    onContinuePress = () => {
        let data = this.state.vendorList.filter((v, i) => v.is_selected);

        if (!data.length) {
            Alert.alert('Please select Vendor');
            return;
        }

        this.props.navigation.navigate('ShowMessage')
    }

    renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={[styles.shadow, { alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: 15, backgroundColor: 'white', borderRadius: 15, overflow: 'hidden', padding: 10, flexDirection: 'row', justifyContent: 'space-between' }]} onPress={() => this.toggleIndex(index)}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={Icons.vendor} style={{ height: 80, width: 80 }} />
                    <View style={{ marginLeft: 15, width: '65%' }}>
                        <Text>{item.vendorName}</Text>
                        <Text style={{ marginTop: 2 }}>{item.vendoraddress}</Text>
                    </View>
                </View>

                <View style={{ height: 22, width: 22, borderRadius: 11, alignItems: 'center', justifyContent: 'center', backgroundColor: this.state.vendorList[index].is_selected ? Colors.appColor : '#dddddd' }}>
                    <Image source={Icons.tick} style={{ height: 18, width: 18, resizeMode: 'contain' }} />
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        if (this.state.loading) {
            return (
                <Loader
                    loading={this.state.loading}
                />
            )
        }

        if (!this.state.vendorList.length) {
            return (
                <SafeAreaView style={{ flex: 1 }}>
                    <Header
                        sName={''}
                        backIcon={true}
                        isQrCodeData={false}
                        goBack={() => this.props.navigation.goBack(null)}
                    />

                    <View style={{ flex: 0.8, alignItems: 'center', justifyContent: 'center', marginLeft: 20, marginRight: 20 }}>
                        <Text style={{ fontSize: 17, fontWeight: 'bold' }}>{this.state.errorMsg}</Text>
                    </View>

                    <View style={{ marginLeft: 20, marginRight: 20 }}>
                        <Button
                            text={'Go to Home'}
                            onPress={() => this.props.navigation.navigate('HomeScreen')}
                        />
                    </View>

                </SafeAreaView>
            )
        }

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    sName={''}
                    backIcon={true}
                    isQrCodeData={false}
                    goBack={() => this.props.navigation.goBack(null)}
                />

                <View style={{ alignItems: 'center', marginTop: 20 }}>
                    <Text style={{ fontSize: 17, fontWeight: 'bold' }}>{this.state.vendorList.length ? 'Select Vendor' : ''}</Text>
                </View>

                {this.state.vendorList.length ?
                    <FlatList
                        data={this.state.vendorList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this.renderItem}
                    />
                    :
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 15 }}>{this.state.errorMsg}</Text>
                    </View>
                }

                {this.state.vendorList.length ?
                    <Button
                        text={'Continue'}
                        onPress={() => this.props.navigation.navigate('ShowMessage')}
                    />
                    :
                    null
                }

            </SafeAreaView>
        )
    }
}

const styles = {
    shadow: {
        shadowColor: 'rgba(0, 0, 0, 0.45)',
        shadowOffset: { width: 0, height: 1.0 },
        shadowRadius: 3,
        shadowOpacity: 0.8,
        borderColor: '#dddddd',
        borderWidth: 3,
        elevation: 1
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(Vendors);