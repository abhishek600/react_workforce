import React, { Component, PureComponent } from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, TouchableOpacity, Text, Image, View, FlatList, Platform } from 'react-native'
import { Icons, Colors, DataModel } from '@common'
import { Loader } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { ApiRequest } from '@httpsRequest'

let dataModel;
class AssetInspection extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            country: '',
            labourId: '',
            societyId: '',
            userData: [],
            assetData: [],
        };
        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let societyId = await AsyncStorage.getItem('societyID')
        let loginData = await AsyncStorage.getItem('loginData')

        if (societyId) {
            this.setState({ societyId: societyId });
        }

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({
                    country: this.state.userData[0].country_id,
                    labourId: this.state.userData[0].id
                }, () => { this.getAssets(); })
            });
        }
    }

    getAssets = () => {
        let country = this.state.country;
        let labourId = this.state.labourId;
        let societyId = this.state.societyId;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        this.setState({ loading: true });

        let params = { "action": "get_asset_list", "country": country, "laboutId": labourId, "societyid": societyId };
        //let params = "&country=" + country + "&laboutId=" + labourId + "&societyid=" + societyId + "&access_token=" + testToken;
        ApiRequest.httpsPostMaster("get_asset_list", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                this.setState({ loading: false, assetData: responseJson.result });
            } else {
                this.setState({ loading: false });
                //Alert.alert(responseJson.message);
            }
        })

    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, marginBottom: Platform.OS == 'android' ? 50 : 0 }}>
                <Loader
                    loading={this.state.loading} />
                <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
                    <View style={{ flex: 1, marginTop: 10, backgroundColor: 'white', borderTopLeftRadius: 15, borderTopRightRadius: 15 }}>
                        <TouchableOpacity style={{ height: 50, width: "60%", alignSelf: 'center', marginTop: 10, marginLeft: 20, marginRight: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.appColor }} onPress={() => this.props.navigation.navigate('ScanQRCode')}>
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>Scan QRCode</Text>
                        </TouchableOpacity>
                        {this.state.assetData.length ?
                            <FlatList
                                data={this.state.assetData}
                                renderItem={({ item, index }) => <AssetList item={item} onItemPressed={() => this.props.navigation.navigate(item.StartStopStatus === "0" ? 'StartAssetInspection' : 'StopAssetInspection', { assetId: item.assetid, assetGroup: item.asset_group, startStopStatus: item.StartStopStatus, isMaintenance: item.is_maintenance })} />}
                                keyExtractor={(item, index) => index.toString()}
                            />
                            :
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <Text>No Data Found</Text>
                            </View>
                        }
                    </View>

                </View>
            </SafeAreaView>
        )
    }

}

class AssetList extends PureComponent {

    render() {
        const { item, onItemPressed } = this.props;
        return (
            <TouchableOpacity style={styles.item} onPress={onItemPressed}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Text style={{ fontWeight: 'bold' }}>{item.asset_name}</Text>
                        <Text>{item.asset_barcode}</Text>
                    </View>

                    {item.StartStopStatus === "0" ?
                        <Image source={Icons.start} style={{ height: 60, width: 60, resizeMode: 'contain' }} />
                        :
                        <Image source={Icons.stop} style={{ height: 60, width: 60, resizeMode: 'contain' }} />
                    }
                </View>
            </TouchableOpacity>
        )
    }

}

const styles = StyleSheet.create({
    item: {

        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 1,
        elevation: 1,
        overflow: 'hidden',
        backgroundColor: 'white',
        borderColor: Colors.gray,
        borderWidth: 1,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 2,
        marginTop: 15,
        padding: 10,
    },

    separator: {
        height: 10
    },
});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(AssetInspection);