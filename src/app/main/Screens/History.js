import React from 'react'
import { SafeAreaView, ImageBackground, TouchableOpacity, Text, View, Alert, FlatList, Platform } from 'react-native'
import { Icons, Colors, Messages, DataModel } from '@common'
import { Loader } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { ApiRequest } from '@httpsRequest'
import moment from 'moment'

let dataModel;
class History extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            society: '',
            country: '',
            labourId: '',
            page_start: 0,
            userData: [],
            currency: '',
            historyData: [],
        };
        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.getData();
    }

    getData = async () => {
        let society = await AsyncStorage.getItem('isSocietyUser')
        let loginData = await AsyncStorage.getItem('loginData')

        if (society) {
            this.setState({ society: society });
        }

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({
                    country: this.state.userData[0].country_id,
                    labourId: this.state.userData[0].id,
                    currency: this.state.userData[0].currency
                }, () => { this.getHistory() })
            });
        }
    }

    getHistory = () => {
        let society = this.state.society;
        let country = this.state.country;
        let labourId = this.state.labourId;
        let pageStart = this.state.page_start;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        this.setState({ loading: true });

        let params = { "action": "compltedTask", "country": country, "labourId": labourId, "society": society, "page_start": pageStart };
        //let params = "&country=" + country + "&labourId=" + labourId + "&society=" + society + "&page_start=" + pageStart + "&access_token=" + testToken;
        ApiRequest.httpsPost("compltedTask", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                let d = [];
                responseJson.result.map((v, i) => {
                    let x = {};

                    x.assignId = v.assignId;
                    x.username = v.username;
                    x.schedule_datetime = v.schedule_datetime;
                    x.execute_datetime = v.execute_datetime;
                    x.address = v.address;
                    x.mobile = v.mobile;
                    x.order_id = v.order_id;
                    x.city = v.city;
                    x.services = v.services;
                    x.beforeStartImages = v.beforeStartImages;
                    x.beforeStopImages = v.beforeStopImages;
                    x.StartStopStatus = v.StartStopStatus;
                    x.taskPrice = v.taskPrice;
                    x.payment_status = v.payment_status;
                    x.ios = v.ios;
                    x.updated_price = v.updated_price;
                    x.android = v.android;
                    x.is_fleet = v.is_fleet;
                    x.inventoryName = v.inventoryName;
                    x.inventoryquantity = v.inventoryquantity;
                    x.pickup_address_location = v.pickup_address_location;
                    x.dropoff_address_location = v.dropoff_address_location;
                    x.inventorylist = v.inventorylist;
                    x.community = v.community;
                    x.showMoreInfo = false;

                    d.push(x);

                    if (i == responseJson.result.length - 1) {
                        let data = d.reverse();
                        this.setState({ loading: false, historyData: this.state.page_start === 0 ? data : [...this.state.historyData, ...data] });
                    }
                });

            } else {
                this.setState({ loading: false });
                //Alert.alert(responseJson.message);
            }
        })

    }

    loadMoreData = () => {
        this.setState({ page_start: this.state.page_start + 1 }, () => this.getHistory())
    }

    onItemPressed = (startImageUrl, endImageUrl) => {
        this.props.navigation.navigate('HistoryDetail', { startImage: startImageUrl, endImage: endImageUrl })
    }

    showMoreInfo = (index) => {
        let data = this.state.historyData.slice(0);

        data[index].showMoreInfo = !data[index].showMoreInfo;
        this.setState({ historyData: data });
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, marginBottom: Platform.OS == 'android' ? 50 : 0 }}>
                <Loader
                    loading={this.state.loading} />
                <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
                    <View style={{ flex: 1, backgroundColor: 'white', marginTop: 10, borderTopRightRadius: 15, borderTopLeftRadius: 15 }}>
                        <View style={{ marginTop: 10 }} />
                        {this.state.historyData.length ?
                            <FlatList
                                data={this.state.historyData}
                                initialNumToRender={10}
                                renderItem={({ item, index }) => <HistoryList item={item} language={this.props.state.setLanguageFlag} showMoreInfo={() => this.showMoreInfo(index)} onItemPressed={() => this.onItemPressed(item.beforeStartImages[0], item.beforeStopImages[0])} />}
                                keyExtractor={(item, index) => index.toString()}
                                onEndReached={this.loadMoreData}
                                onEndReachedThreshold={0.3}
                            />
                            :
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <Text>No Data Found</Text>
                            </View>
                        }

                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

class HistoryList extends React.PureComponent {

    getDate = (time) => {
        let date = time.split(' ');

        if (moment(date[0]).format('DD MMM YYYY') != 'Invalid date')
            return moment(date[0]).format('DD MMM YYYY');
        else
            return "";
    }

    getTime = (time) => {
        let t = time.split("-");

        if (t[3] != undefined)
            return t[3].trim();
        else
            return "";
    }

    getInventoryName = (inventoryList) => {
        let inventoryName = "";
        let x = "";

        inventoryList.map((v, i) => {
            if (i == inventoryList.length - 1) {
                x = "";
            } else {
                x = ",";
            }

            if (v.inventoryName != null && v.inventoryName != "") {
                inventoryName = inventoryName + v.inventoryName + x;
            }

        });
        return inventoryName;
    }

    getInventoryQuantity = (inventoryList) => {
        let inventoryQuantity = "";
        let x = "";

        inventoryList.map((v, i) => {
            if (i == inventoryList.length - 1) {
                x = "";
            } else {
                x = ",";
            }

            inventoryQuantity = inventoryQuantity + v.inventoryquantity + x;

        });
        return inventoryQuantity;
    }

    render() {
        const { item, showMoreInfo, onItemPressed, language } = this.props;
        return (
            <TouchableOpacity style={{ marginTop: 5, marginBottom: 5, marginLeft: 10, marginRight: 10, backgroundColor: 'white', borderRadius: 10, shadowOpacity: 2, shadowRadius: 1, shadowOffset: { height: 1, width: 0 }, shadowColor: '#808080', elevation: 2 }} onPress={onItemPressed}>

                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 10 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>Community</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{item.community}</Text>
                </View>

                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.name : Messages.En.name}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{item.username}</Text>
                </View>

                {item.inventorylist.length ?
                    <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                        <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.productName : Messages.En.productName}</Text>
                        <Text style={{ color: '#808080', fontWeight: '500' }}>{this.getInventoryName(item.inventorylist)}</Text>
                    </View>
                    :
                    null
                }

                {item.inventorylist.length ?
                    <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                        <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.quantity : Messages.En.quantity}</Text>
                        <Text style={{ color: '#808080', fontWeight: '500' }}>{this.getInventoryQuantity(item.inventorylist)}</Text>
                    </View>
                    :
                    null
                }

                {item.services != "" ?
                    <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                        <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.task : Messages.En.task}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text></Text>
                            <Text style={{ width: 180, color: '#808080', fontWeight: '500' }}>{item.services}</Text>
                        </View>
                    </View>
                    :
                    null
                }

                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.date : Messages.En.date}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{this.getDate(item.execute_datetime)}</Text>
                </View>

                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.time : Messages.En.time}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{this.getTime(item.execute_datetime)}</Text>
                </View>

                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.phone : Messages.En.phone}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{item.mobile}</Text>
                </View>

                {item.showMoreInfo ?
                    <View>
                        {item.address != "" && item.address != null ?
                            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                                <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.address : Messages.En.address}</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text></Text>
                                    <Text style={{ width: 190, color: '#808080', fontWeight: '500' }}>{item.address}</Text>
                                </View>
                            </View>
                            :
                            null
                        }

                        {item.pickup_address_location != "" ?
                            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                                <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>Pickup Address</Text>
                                <Text style={{ width: 190, color: '#808080', fontWeight: '500' }}>{item.pickup_address_location}</Text>
                            </View>
                            :
                            null
                        }

                        {item.dropoff_address_location != "" ?
                            <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                                <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>Drop Address</Text>
                                <Text style={{ width: 190, color: '#808080', fontWeight: '500' }}>{item.dropoff_address_location}</Text>
                            </View>
                            :
                            null
                        }

                        <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                            <Text style={{ width: 120, color: Colors.appColor, fontWeight: '500' }}>{language ? language.price : Messages.En.price}</Text>
                            <Text style={{ color: '#808080', fontWeight: '500' }}>Rs {item.taskPrice} {item.payment_status != "" ? '(' + item.payment_status + ')' : null}</Text>
                        </View>

                    </View>
                    :
                    null
                }

                {/* <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 3 }}>
                    <Text style={{ width: 100, color: Colors.appColor, fontWeight: '500' }}>{language ? language.payment : Messages.En.payment}</Text>
                    <Text style={{ color: '#808080', fontWeight: '500' }}>{item.payment_status}</Text>
                </View> */}

                <View style={{ height: 1, marginTop: 10, width: '100%', borderColor: '#dddddd', borderWidth: 1, borderStyle: 'dashed' }} />
                <TouchableOpacity style={{ height: 30, alignItems: 'center', justifyContent: 'center' }} onPress={showMoreInfo}>
                    <Text style={{ color: '#808080', fontWeight: '400' }}>More Info</Text>
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
}


const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(History);