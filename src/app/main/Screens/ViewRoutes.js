import React, { Component } from 'react'
import { SafeAreaView, Text, Image, View, Dimensions, Platform, Alert } from 'react-native'
import { Icons, Messages, Config, DataModel } from '@common'
import { Header } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { ApiRequest } from '@httpsRequest';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922 
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

let dataModel;
Geocoder.init(Config.google_api_key);
class ViewRoutes extends Component {

    constructor(props) {
        super(props);

        this.state = {
            initialRegion: { latitude: 30.688803, longitude: 76.7379394, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA },
            initialLat: 0,
            initialLng: 0,
            society: '',
            labourId: '',
            userData: [],
            todayTasksData: [],
            coordinates: [],
        };

        this.mapView = null;

        if (Platform.OS == 'android') {
            this.getLocation();
        } else {
            this.getLatLng();
        }

        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        this.checkPermission();
        this.getData();
    }

    componentWillUnmount() {
        Geolocation.clearWatch(this.watchID);
    }

    checkPermission = () => {
        if (Platform.OS == 'android') {
            this.getLocation();
        } else {
            this.getLatLng();
        }
    }

    getData = async () => {
        let society = await AsyncStorage.getItem('isSocietyUser')
        let loginData = await AsyncStorage.getItem('loginData')
        let societyId = ""

        if (society) {
            this.setState({ society: society });
        }

        if (society == "1") {
            societyId = await AsyncStorage.getItem('societyID');
        }

        if (loginData) {
            this.setState({ userData: JSON.parse(loginData) }, () => {
                this.setState({
                    labourId: this.state.userData[0].id
                }, () => { this.getTodayTasks(societyId) })
            });
        }
    }

    getTodayTasks = (society_id) => {
        let labourId = this.state.labourId;
        let authToken = dataModel.getAuthToken();
        let testToken = dataModel.getTestToken();

        let params = { "action": "todaystaskpin", "labourId": labourId, "society": society_id };
        //let params = "&labourId=" + labourId + "&society=" + society_id + "&access_token=" + testToken;
        ApiRequest.httpsPost("todaystaskpin", params, authToken).then((responseJson) => {
            console.log('response', responseJson);
            if (responseJson.status === 200) {
                let picLocs = [];
                let dropLocs = [];

                responseJson.result.map((v, i) => {
                    picLocs.push({ latitude: v.pick_lat, longitude: v.pick_lng });
                    dropLocs.push({ latitude: v.drop_lat, longitude: v.drop_lng });

                    if (i == responseJson.result.length - 1) {
                        this.setState({
                            todayTasksData: responseJson.result,
                            coordinates: [...this.state.coordinates, ...picLocs, ...dropLocs]
                        }, () => {
                            console.log('coordinates', this.state.coordinates);
                        });
                    }
                });
            } else {

            }
        })

    }

    getLocation = () => {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
            .then((data) => {
                console.log('locationData', data);
                this.getLatLng();

            }).catch((err) => {
                console.log('locationErr', err);
            });
    }

    getLatLng = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                let lat = position.coords.latitude;
                let lng = position.coords.longitude;
                let initialRegion = { latitude: lat, longitude: lng, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA };
                console.log('latlng', JSON.stringify({ "latitude": lat, "longitude": lng }));
                //Alert.alert('currentLocation', JSON.stringify(initialRegion));
                this.setState({ initialLat: lat, initialLng: lng, initialRegion });
                //this.intervalID = setTimeout(() => { this.checkPermission() }, 5000);
            },
            (error) => { console.log(error); },
            { enableHighAccuracy: false, timeout: 5000 }
        );

        this.watchId = Geolocation.watchPosition((position) => {
            let lat1 = position.coords.latitude;
            let lng1 = position.coords.longitude;
            let initialRegion = { latitude: lat1, longitude: lng1, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA };
            //Alert.alert('updateLocation', JSON.stringify(initialRegion));
            this.setState({ initialLat: lat1, initialLng: lng1, initialRegion });
            //this.intervalID = setTimeout(() => { this.checkPermission() }, 5000);
        },
        (error) => { console.log(error); },
        { enableHighAccuracy: false, distanceFilter: 100 }
        );
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.routes : Messages.En.routes}
                    backIcon={true}
                    goBack={() => this.props.navigation.goBack(null)}
                    isQrCodeData={false}
                />

                <MapView
                    //provider={PROVIDER_DEFAULT}
                    region={this.state.initialRegion}
                    style={{ height: '100%', width: '100%' }}
                    ref={c => this.mapView = c}
                    zoomEnabled={true}
                >
                    {(this.state.coordinates.length >= 1) && (
                        <Marker
                            coordinate={{ latitude: this.state.initialLat, longitude: this.state.initialLng }}
                        >
                            <View>
                                <Image source={Icons.bike} style={{ height: 50, width: 50, resizeMode: 'contain' }} />
                            </View>
                        </Marker>
                    )}

                    {this.state.todayTasksData.map((item, index) =>
                        <MapView.Marker key={index.toString()}
                            coordinate={{ latitude: Platform.OS == 'ios' ? Number(item.pick_lat) : parseFloat(item.pick_lat), longitude: Platform.OS == 'ios' ? Number(item.pick_lng) : parseFloat(item.pick_lng) }}
                        >
                            <Text style={{ width: 100 }}>#{item.orderId}</Text>
                            <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: 'white', borderColor: '#90EE90', borderWidth: 3, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: 'black', fontSize: 10 }}>Pickup</Text>
                            </View>
                        </MapView.Marker>
                    )}

                    {this.state.todayTasksData.map((item, index) =>
                        <MapView.Marker key={index.toString()}
                            coordinate={{ latitude: Platform.OS == 'ios' ? Number(item.drop_lat) : parseFloat(item.drop_lat), longitude: Platform.OS == 'ios' ? Number(item.drop_lng) : parseFloat(item.drop_lng) }}
                        >
                            <Text style={{ width: 100 }}>#{item.orderId}</Text>
                            <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: 'white', borderColor: '#fed8b1', borderWidth: 3, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: 'black', fontSize: 10 }}>Drop</Text>
                            </View>
                        </MapView.Marker>
                    )}

                    {(this.state.coordinates.length >= 1) && (
                        <MapViewDirections
                            origin={this.state.initialRegion}
                            waypoints={(this.state.coordinates.length > 1) ? this.state.coordinates.slice(1, -1) : null}
                            destination={this.state.coordinates[this.state.coordinates.length - 1]}
                            apikey={"AIzaSyBNi4H4h7NUKCMFJUyEzUgPrfRhcRKhSnQ"}
                            strokeWidth={5}
                            strokeColor="#03A9F4"
                            optimizeWaypoints={true}
                            onStart={(params) => {
                                console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                            }}
                            onReady={result => {
                                console.log(`Distance: ${result.distance} km`)
                                console.log(`Duration: ${result.duration} min.`)

                                this.mapView.fitToCoordinates(result.coordinates, {
                                    edgePadding: {
                                        right: (width / 20),
                                        bottom: (height / 20),
                                        left: (width / 20),
                                        top: (height / 20),
                                    }
                                });
                            }}
                            onError={(errorMessage) => {
                                // console.log('GOT AN ERROR');
                            }}
                        />
                    )}

                </MapView>

            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(ViewRoutes);