import React from 'react'
import { SafeAreaView, ImageBackground, StyleSheet, TouchableOpacity, Text, Image, View, TextInput, Alert, ScrollView, Platform, Keyboard } from 'react-native'
import { Icons, Colors, Messages, DataModel } from '@common'
import { Button, Loader, Header, QrCodeModal } from '@components'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { Dropdown } from 'react-native-material-dropdown';
import ImagePicker from 'react-native-image-picker';
import { updateTasks } from '../../actions/actions';
import { ApiRequest } from '@httpsRequest';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

let dataModel;
class StartTask extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      photo1: null,
      photo2: null,
      photo3: null,
      photo4: null,
      photo5: null,
      photo6: null,
      loading: false,
      qrModalVisible: false,
      assignId: this.props.navigation.getParam('assignId'),
      is_fleet: this.props.navigation.getParam('is_fleet'),
      covid: this.props.navigation.getParam('covid'),
      order_id: this.props.navigation.getParam('order_id'),
      assetValue: 'Select',
      description: '',
      client: '',
      branch: '',
      department: '',
      asset: [],
      society: '',
      country: '',
      userData: [],
      timeValue: '30 Minutes',
      meterReading: '',
      workforceFlow: '',
      quantity: '',
      timeId: 0,
      metersData: 'KM',
      meters: [{
        value: 'KM',
      },
      {
        value: 'ML',
      }],
      time: [{
        value: '30 Minutes',
      },
      {
        value: '1 Hour',
      },
      {
        value: '2 Hour',
      },
      {
        value: '3 Hour',
      },
      {
        value: '4 Hour',
      },
      {
        value: '5 Hour',
      },
      {
        value: 'More than 5 Hour',
      }],
    };

    dataModel = DataModel.getInstance();
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    let society = await AsyncStorage.getItem('isSocietyUser')
    let loginData = await AsyncStorage.getItem('loginData')

    if (society) {
      this.setState({ society: society });
    }

    if (loginData) {
      this.setState({ userData: JSON.parse(loginData) }, () => {
        this.setState({
          country: this.state.userData[0].country_id,
          workforceFlow: this.state.userData[0].workforceflow1
        }, () => {
          this.getUserAssets();
        })
      });
    }
  }

  getUserAssets = () => {
    let assignId = this.state.assignId;
    let authToken = dataModel.getAuthToken();
    let testToken = dataModel.getTestToken();

    let params = { "action": "get_user_asset_list", "assignId": assignId };
    //let params = "&assignId=" + assignId + "&access_token=" + testToken;
    ApiRequest.httpsPostMaster("get_user_asset_list", params, authToken).then((responseJson) => {
      console.log('response', responseJson);
      if (responseJson.status === 200) {
        this.setState({ loading: false, asset: responseJson.data })
      } else {
        this.setState({ loading: false });
        //Alert.alert(responseJson.message);
      }
    })

  }

  launchCamera = (num) => {
    let photo3 = this.state.photo3;
    let photo4 = this.state.photo4;
    let photo5 = this.state.photo5;
    let photo6 = this.state.photo6;

    if (num == "999") {
      if (photo3 != null && photo4 != null && photo5 != null && photo6 != null) {
        Alert.alert("You can't select more than 6 images");
        return;
      }
    }

    let options = {
      quality: 1,
      maxWidth: 120,
      maxHeight: 120,
      allowsEditing: false,
      noData: true,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        //alert(response.customButton);
      } else {
        let source = response;
        console.log('response', JSON.stringify(response));

        if (num == "999") {
          if (photo3 == null && photo4 == null && photo5 == null && photo6 == null) {
            this.setState({ photo3: source });
          } else if (photo3 != null && photo4 == null && photo5 == null && photo6 == null) {
            this.setState({ photo4: source });
          } else if (photo3 != null && photo4 != null && photo5 == null && photo6 == null) {
            this.setState({ photo5: source });
          } else if (photo3 != null && photo4 != null && photo5 != null && photo6 == null) {
            this.setState({ photo6: source });
          } else {

          }
        } else {
          if (num == "1") {
            this.setState({ photo1: source });
          } else if (num == "2") {
            this.setState({ photo2: source });
          } else if (num == "3") {
            this.setState({ photo3: source });
          } else if (num == "4") {
            this.setState({ photo4: source });
          } else if (num == "5") {
            this.setState({ photo5: source });
          } else {
            this.setState({ photo6: source });
          }
        }

      }
    });

  }

  GenerateRandomNumber = () => {
    var RandomNumber = Math.floor(Math.random() * 100) + 1;
    return RandomNumber + ".jpg";
  }

  onStartTaskPress = () => {
    let photo1 = this.state.photo1;
    let photo2 = this.state.photo2;
    let photo3 = this.state.photo3;
    let photo4 = this.state.photo4;
    let photo5 = this.state.photo5;
    let photo6 = this.state.photo6;
    let covid = this.state.covid;

    if (covid != '2') {
      if (photo1 == null && photo2 == null) {
        Alert.alert('Please select atleast 1 image');
        return;
      }
    }

    let timeId = "";
    if (this.state.is_fleet == "0") {
      timeId = this.state.timeId + 1;
      console.log('timeId', timeId);
    }

    let society = this.state.society;
    let country = this.state.country;
    let description = this.state.description;
    let assignId = this.state.assignId;
    let quantity = this.state.quantity;
    let client = this.state.client;
    let branch = this.state.branch;
    let department = this.state.department;
    let authToken = dataModel.getAuthToken();
    let testToken = dataModel.getTestToken();
    let assetId = "";
    this.state.asset.map((v, i) => {
      if (v.assetName == this.state.assetValue) {
        assetId = v.assetId;
      }
    })

    this.setState({ loading: true });

    let url = "https://townconnect.in/townlabour/webservice/Api.php?";
    let authUrl = "https://townconnect.in/townlabour/webservice/v1.2/workforce.php?access_token=" + authToken;

    let formdata = new FormData();

    formdata.append('action', 'startStop')
    formdata.append('sign_start', null)
    formdata.append('sign_stop', null)

    if (photo1 != null) {
      formdata.append('image1', { uri: photo1.uri, name: photo1.fileName != null ? photo1.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo1.type })
    }

    if (photo2 != null) {
      formdata.append('image2', { uri: photo2.uri, name: photo2.fileName != null ? photo2.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo2.type })
    }

    if (photo3 != null) {
      formdata.append('imagestartoptional1', { uri: photo3.uri, name: photo3.fileName != null ? photo3.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo3.type })
    }

    if (photo4 != null) {
      formdata.append('imagestartoptional2', { uri: photo4.uri, name: photo4.fileName != null ? photo4.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo4.type })
    }

    if (photo5 != null) {
      formdata.append('imagestartoptional3', { uri: photo5.uri, name: photo5.fileName != null ? photo5.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo5.type })
    }

    if (photo6 != null) {
      formdata.append('imagestartoptional4', { uri: photo6.uri, name: photo6.fileName != null ? photo6.fileName : this.GenerateRandomNumber(), type: Platform.OS == 'android' ? 'image/jpeg' : photo6.type })
    }

    formdata.append('no_items', quantity)
    formdata.append('unit_type', this.state.metersData)
    formdata.append('isReschdule', "0")
    formdata.append('reschdule_time', '')
    formdata.append('mstop_reading', '')
    formdata.append('m_reading', this.state.meterReading)
    formdata.append('assetId', assetId)
    formdata.append('rating', '')
    formdata.append('description', description)
    formdata.append('reason', '')
    formdata.append('country', country)
    formdata.append('verifyOTP', '')
    formdata.append('assignId', assignId)
    formdata.append('StartStopStatus', "0")
    formdata.append('society', society)
    formdata.append('time', timeId)
    formdata.append('clientName', client)
    formdata.append('clientBranch', branch)
    formdata.append('clientDepartment', department)

    fetch(authUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data;',
        Accept: "application/json"
      },
      timeout: 5000,
      body: formdata
    })
      .then(response => response.json())
      .then((responseJson) => {
        console.log('startTaskResponse', responseJson);
        if (responseJson.status === 200) {
          dataModel.setAssignId(assignId);
          this.setState({ loading: false });
          let x = this.props.state.updateTasksFlag ? this.props.state.updateTasksFlag + 1 : 1;
          this.props.updateTasks(x);
          if (covid == '2') {
            this.props.navigation.navigate('MedicalForm', { order_id: this.state.order_id });
          } else {
            this.props.navigation.goBack(null);
          }
        } else {
          this.setState({ loading: false });
          //Alert.alert(responseJson.message);
        }

      })
      .catch(error => console.log(error))
  }

  render() {
    let qrCodeData = dataModel.getQrCodeData();
    let is_fleet = this.state.is_fleet;
    let workforceFlow = this.state.workforceFlow;
    let dropdownData = this.state.asset;

    let dropdown = [{
      value: 'Select'
    }];

    if (dropdownData.length) {
      for (let p in dropdownData) {

        dropdown.push({
          value: dropdownData[p].assetName
        });
      }
    }

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Loader
          loading={this.state.loading} />

        <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
          <Header
            sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.startTask : Messages.En.startTask}
            isQrCodeData={false}
            backIcon={true}
            goBack={() => this.props.navigation.goBack(null)}
          />

          <View style={{ flex: 1, backgroundColor: 'white', marginTop: 10, borderTopRightRadius: 15, borderTopLeftRadius: 15 }}>
            <ScrollView keyboardShouldPersistTaps="always">
              <ScrollView horizontal={true} style={{ marginLeft: 20, marginRight: 20 }}>
                <View style={{ alignItems: 'center', marginTop: 20, flexDirection: 'row' }}>
                  <TouchableOpacity style={{ height: 120, width: 120, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("1")}>
                    {this.state.photo1 == null ?
                      <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                      :
                      <Image source={{ uri: this.state.photo1.uri }} style={{ height: 100, width: 100 }} />
                    }
                  </TouchableOpacity>
                  <View style={{ width: 10 }} />
                  <TouchableOpacity style={{ height: 120, width: 120, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("2")}>
                    {this.state.photo2 == null ?
                      <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                      :
                      <Image source={{ uri: this.state.photo2.uri }} style={{ height: 100, width: 100 }} />
                    }
                  </TouchableOpacity>

                  {this.state.photo1 != null && this.state.photo2 != null ?
                    <TouchableOpacity style={{ height: 120, width: 120, marginLeft: 10, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("3")}>
                      {this.state.photo3 == null ?
                        <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                        :
                        <Image source={{ uri: this.state.photo3.uri }} style={{ height: 100, width: 100 }} />
                      }
                    </TouchableOpacity>
                    :
                    null
                  }

                  {this.state.photo1 != null && this.state.photo2 != null && this.state.photo3 != null ?
                    <TouchableOpacity style={{ height: 120, width: 120, marginLeft: 10, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("4")}>
                      {this.state.photo4 == null ?
                        <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                        :
                        <Image source={{ uri: this.state.photo4.uri }} style={{ height: 100, width: 100 }} />
                      }
                    </TouchableOpacity>
                    :
                    null
                  }

                  {this.state.photo1 != null && this.state.photo2 != null && this.state.photo3 != null && this.state.photo4 != null ?
                    <TouchableOpacity style={{ height: 120, width: 120, marginLeft: 10, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("5")}>
                      {this.state.photo5 == null ?
                        <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                        :
                        <Image source={{ uri: this.state.photo5.uri }} style={{ height: 100, width: 100 }} />
                      }
                    </TouchableOpacity>
                    :
                    null
                  }

                  {this.state.photo1 != null && this.state.photo2 != null && this.state.photo3 != null && this.state.photo4 != null && this.state.photo5 != null ?
                    <TouchableOpacity style={{ height: 120, width: 120, marginLeft: 10, borderRadius: 10, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.launchCamera("6")}>
                      {this.state.photo6 == null ?
                        <MaterialCommunityIcons style={{ color: 'white' }} size={100} name={'camera-plus'} />
                        :
                        <Image source={{ uri: this.state.photo6.uri }} style={{ height: 100, width: 100 }} />
                      }
                    </TouchableOpacity>
                    :
                    null
                  }

                  {/* <Icon style={{ color: 'limegreen', marginLeft: 10 }} size={50} name={'add-circle-sharp'} onPress={() => this.launchCamera("999")} /> */}
                </View>

              </ScrollView>

              {workforceFlow == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 30 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{'Client'}</Text>
                  <TextInput
                    style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    onChangeText={(client) => this.setState({ client })}
                  />
                </View>
                :
                null
              }

              {workforceFlow == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{'Branch'}</Text>
                  <TextInput
                    style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    onChangeText={(branch) => this.setState({ branch })}
                  />
                </View>
                :
                null
              }

              {workforceFlow == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{'Department'}</Text>
                  <TextInput
                    style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    onChangeText={(department) => this.setState({ department })}
                  />
                </View>
                :
                null
              }

              {is_fleet == "0" || is_fleet == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.observations : Messages.En.observations}</Text>
                  <TextInput
                    style={[styles.input1, { borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    multiline={true}
                    blurOnSubmit={false}
                    onSubmitEditing={() => Keyboard.dismiss()}
                    onChangeText={(description) => this.setState({ description })}
                  />
                </View>
                :
                null
              }

              {this.state.assetValue != "Select" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500', marginTop: 10 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.unitType : Messages.En.unitType} :</Text>
                  <View style={[styles.input1, { height: 40, marginTop: 1, borderRadius: 10, justifyContent: 'center', backgroundColor: 'white' }]}>
                    <Dropdown
                      value={this.state.metersData}
                      containerStyle={{ marginBottom: 12 }}
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      fontSize={12}
                      data={this.state.meters}
                      onChangeText={(value, index) => this.setState({ metersData: value })}
                    />
                  </View>
                </View>
                :
                null
              }

              {this.state.assetValue != "Select" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 20 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500' }}>Unit(Meter Reading)</Text>
                  <TextInput
                    style={[styles.input, { height: 40 }]}
                    underlineColorAndroid="transparent"
                    keyboardType={'numeric'}
                    returnKeyType={'done'}
                    onChangeText={(meterReading) => this.setState({ meterReading })}
                  />
                </View>
                :
                null
              }

              {workforceFlow != "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500', marginTop: 10 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.selectAsset : Messages.En.selectAsset}</Text>
                  <View style={[styles.input1, { height: 40, marginTop: 1, borderRadius: 10, justifyContent: 'center', backgroundColor: 'white' }]}>
                    <Dropdown
                      value={this.state.assetValue}
                      containerStyle={{ marginBottom: 12 }}
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      fontSize={12}
                      data={dropdown}
                      onChangeText={(value, index) => this.setState({ assetValue: value })}
                    />
                  </View>
                </View>
                :
                null
              }

              {is_fleet == "0" && workforceFlow != "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500', marginTop: 10 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.selectTime : Messages.En.selectTime}</Text>
                  <View style={[styles.input1, { height: 40, marginTop: 1, borderRadius: 10, justifyContent: 'center', backgroundColor: 'white' }]}>
                    <Dropdown
                      value={this.state.timeValue}
                      containerStyle={{ marginBottom: 12 }}
                      inputContainerStyle={{ borderBottomColor: 'transparent' }}
                      fontSize={12}
                      data={this.state.time}
                      onChangeText={(value, index) => this.setState({ timeValue: value, timeId: index })}
                    />
                  </View>
                </View>
                :
                null
              }

              {is_fleet == "0" ?
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 30, marginLeft: 10, marginRight: 10 }}>
                  <View style={{ flex: 1, height: 1, backgroundColor: Colors.darkGray }} />
                  <View>
                    <Text style={{ width: 130, textAlign: 'center', color: Colors.darkGray, fontWeight: '500' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.customerRating : Messages.En.customerRating}</Text>
                  </View>
                  <View style={{ flex: 1, height: 1, backgroundColor: Colors.darkGray }} />
                </View>
                :
                null
              }

              {is_fleet == "1" ?
                <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
                  <Text style={{ color: Colors.darkGray, fontWeight: '500', marginTop: 10 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.quantity : Messages.En.quantity}</Text>
                  <TextInput
                    style={[styles.input1, { height: 40, marginTop: 1, borderRadius: 10, backgroundColor: 'white' }]}
                    underlineColorAndroid="transparent"
                    keyboardType={'numeric'}
                    returnKeyType={'done'}
                    onChangeText={(quantity) => this.setState({ quantity })}
                  />
                </View>
                :
                null
              }

              <View style={{ height: 20 }} />

            </ScrollView>
            <TouchableOpacity style={{ height: 50, backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onStartTaskPress()}>
              <Text style={{ color: 'white', fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.startTaskCaps : Messages.En.startTaskCaps}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <QrCodeModal
          qrCodeData={qrCodeData}
          qrModalVisible={this.state.qrModalVisible}
          backPress={() => this.setState({ qrModalVisible: false })}
        />
      </SafeAreaView>
    )
  }

}

const styles = StyleSheet.create({
  input1: {
    marginTop: 10,
    textAlignVertical: "top",
    padding: 10,
    textAlign: 'left',
    borderBottomColor: Colors.darkGray,
    borderBottomWidth: 1,
    borderRadius: 2,
    ...Platform.select({
      ios: {
        marginBottom: 10
      }
    })
  },

  input: {
    marginTop: 10,
    textAlignVertical: "top",
    padding: 10,
    textAlign: 'left',
    borderBottomColor: Colors.darkGray,
    borderBottomWidth: 1,
    borderRadius: 2,
    ...Platform.select({
      ios: {
        marginBottom: 10
      }
    })
  },

});

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
  updateTasks: (data) => {
    dispatch(updateTasks(data));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(StartTask);