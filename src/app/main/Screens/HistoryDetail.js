import React from 'react'
import { SafeAreaView, ImageBackground, Text, Image, View } from 'react-native'
import { Icons, Colors, Messages, DataModel } from '@common'
import { Header, QrCodeModal } from '@components'
import { connect } from 'react-redux'

let dataModel;
class HistoryDetail extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            startImageUrl: this.props.navigation.getParam('startImage'),
            endImageUrl: this.props.navigation.getParam('endImage'),
            qrModalVisible: false,
        };

        dataModel = DataModel.getInstance();
    }

    render() {
        let qrCodeData = dataModel.getQrCodeData();
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: Colors.appColor }}>
                    <Header
                        sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.images : Messages.En.images}
                        isQrCodeData={false}
                        backIcon={true}
                        goBack={() => this.props.navigation.goBack(null)}
                    />

                    <View style={{ flex: 1, marginTop: 10, backgroundColor: 'white', borderTopLeftRadius: 15, borderTopRightRadius: 15 }}>
                        <View style={{ marginLeft: 20, marginLeft: 20, marginTop: 40 }}>
                            <Text style={{ color: Colors.appColor, fontSize: 17, fontWeight: 'bold' }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.startTaskImages : Messages.En.startTaskImages}</Text>
                            <View style={{ alignItems: 'center', marginTop: 10 }}>
                                <Image source={this.state.startImageUrl ? { uri: this.state.startImageUrl } : Icons.ic_place_holder} style={{ height: 150, width: 150 }} />
                            </View>
                            <Text style={{ color: Colors.appColor, fontSize: 17, fontWeight: 'bold', marginTop: 20 }}>{this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.endTaskImages : Messages.En.endTaskImages}</Text>
                            <View style={{ alignItems: 'center', marginTop: 10 }}>
                                <Image source={this.state.endImageUrl ? { uri: this.state.endImageUrl } : Icons.ic_place_holder} style={{ height: 150, width: 150 }} />
                            </View>
                        </View>
                    </View>
                </View>
                <QrCodeModal
                    qrCodeData={qrCodeData}
                    qrModalVisible={this.state.qrModalVisible}
                    backPress={() => this.setState({ qrModalVisible: false })}
                />
            </SafeAreaView>
        )
    }
}


const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(HistoryDetail);