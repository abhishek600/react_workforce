import React, { Component } from 'react';
import { SafeAreaView, View, Platform } from 'react-native';
import { Messages, Colors } from '@common';
import { connect } from 'react-redux';
import { Loader, Header } from '@components';
import AdminChat from './AdminChat'
import { isIphoneX } from './is-iphone-x'

class Chat extends Component {

    state = {
        loading: false,
    }

    render() {
        let h = isIphoneX() ? 50 : 25;
        return (
            <View style={{flex: 1}}>
              <View style={{height: Platform.OS == 'android' ? 0 : h, backgroundColor: Colors.background}} />
            
            <SafeAreaView style={{flex: 1, zIndex: -2}}>
                  <Loader
                    loading={this.state.loading} 
                    />
                 
                 <View style={{flex: -1}}>
                 <Header
                    sName={this.props.state.setLanguageFlag ? this.props.state.setLanguageFlag.admin : Messages.En.admin}
                    backIcon={true}
                    isQrCodeData={false}
                    goBack={() => this.props.navigation.goBack(null)}
                 />
                 </View>
                 
               
               <View style={{zIndex: -2, flex: 1}}>
               <AdminChat />
               </View> 
               
            </SafeAreaView>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({

});
export default connect(mapStateToProps, mapDispatchToProps)(Chat);