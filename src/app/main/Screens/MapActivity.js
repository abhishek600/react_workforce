import React, { Component } from 'react'
import { SafeAreaView, Image, View, Platform, Dimensions, Alert } from 'react-native'
import { Icons, Config, DataModel } from '@common'
import { Header, QrCodeModal } from '@components'
import { connect } from 'react-redux'
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MapView, { Marker, PROVIDER_DEFAULT } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

Geocoder.init(Config.google_api_key);
let dataModel;
class MapActivity extends Component {
    intervalID;

    constructor(props) {
        super(props);

        this.state = {
            initialRegion: { latitude: 30.688803, longitude: 76.7379394, latitudeDelta: 0.0922, longitudeDelta: 0.0421 },
            initialLat: 0,
            initialLng: 0,
            destLat: this.props.navigation.getParam('latitude'),
            destLng: this.props.navigation.getParam('longitude'),
            name: this.props.navigation.getParam('name'),
            qrModalVisible: false,
        };

        this.mapView = null;

        if (Platform.OS == 'android') {
            this.getLocation();
        } else {
            this.getLatLng();
        }

        dataModel = DataModel.getInstance();
    }

    componentDidMount() {
        if (Platform.OS == 'android') {
            this.getLocation();
        } else {
            this.getLatLng();
        }

    }

    componentWillUnmount() {
        //clearTimeout(this.intervalID);
        Geolocation.clearWatch(this.watchId);
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state, callback) => {
            return;
        };
    }

    getLocation = () => {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
            .then((data) => {
                console.log('locationData', data);
                this.getLatLng();

            }).catch((err) => {
                console.log('locationErr', err);
            });
    }

    getLatLng = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                let lat = position.coords.latitude;
                let lng = position.coords.longitude;
                let initialRegion = { latitude: lat, longitude: lng, latitudeDelta: 0.0922, longitudeDelta: 0.0421 };
                //Alert.alert('currentLocation', JSON.stringify(initialRegion));
                this.setState({ initialLat: lat, initialLng: lng, initialRegion });

            },
            (error) => { console.log(error); },
            { enableHighAccuracy: false, timeout: 5000 }
        );

        this.watchId = Geolocation.watchPosition((position) => {
            let lat1 = position.coords.latitude;
            let lng1 = position.coords.longitude;
            let initialRegion = { latitude: lat1, longitude: lng1, latitudeDelta: 0.0922, longitudeDelta: 0.0421 };
            //Alert.alert('updateLocation', JSON.stringify(initialRegion));
            this.setState({ initialLat: lat1, initialLng: lng1, initialRegion });

        },
            (error) => { console.log(error); },
            { enableHighAccuracy: false, distanceFilter: 100 }
        );
    }

    render() {
        let initialLat = Platform.OS == 'ios' ? Number(this.state.initialLat) : parseFloat(this.state.initialLat);
        let initialLng = Platform.OS == 'ios' ? Number(this.state.initialLng) : parseFloat(this.state.initialLng);
        let destLat = Platform.OS == 'ios' ? Number(this.state.destLat) : parseFloat(this.state.destLat);
        let destLng = Platform.OS == 'ios' ? Number(this.state.destLng) : parseFloat(this.state.destLng);
        let qrCodeData = dataModel.getQrCodeData();
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    sName={this.state.name}
                    backIcon={true}
                    isQrCodeData={false}
                    goBack={() => this.props.navigation.goBack(null)} />

                <MapView
                    //provider={PROVIDER_DEFAULT}
                    region={this.state.initialRegion}
                    style={{ height: '100%', width: '100%' }}
                    ref={c => this.mapView = c}
                >

                    {/** Start Marker */}
                    <Marker
                        coordinate={{ latitude: initialLat, longitude: initialLng }}
                    >
                        <View>
                            <Image source={Icons.bike} style={{ height: 50, width: 50, resizeMode: 'contain' }} />
                            <Image source={Icons.start} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
                        </View>
                    </Marker>

                    {/** End Marker */}
                    <Marker
                        coordinate={{ latitude: destLat, longitude: destLng }}
                    >
                        <View>
                            <Image source={Icons.stop} style={{ height: 30, width: 30, resizeMode: 'contain' }} />
                        </View>
                    </Marker>

                    <MapViewDirections
                        origin={{ latitude: initialLat, longitude: initialLng }}
                        destination={{ latitude: destLat, longitude: destLng }}
                        apikey={"AIzaSyBNi4H4h7NUKCMFJUyEzUgPrfRhcRKhSnQ"}
                        strokeWidth={5}
                        strokeColor="#03A9F4"
                        onStart={(params) => {
                            console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                        }}
                        onReady={result => {
                            console.log(`Distance: ${result.distance} km`)
                            console.log(`Duration: ${result.duration} min.`)

                            this.mapView.fitToCoordinates(result.coordinates, {
                                edgePadding: {
                                    right: (width / 20),
                                    bottom: (height / 20),
                                    left: (width / 20),
                                    top: (height / 20),
                                }
                            });
                        }}
                        onError={(errorMessage) => {
                            // console.log('GOT AN ERROR');
                        }}
                    />
                </MapView>

                <QrCodeModal
                    qrCodeData={qrCodeData}
                    qrModalVisible={this.state.qrModalVisible}
                    backPress={() => this.setState({ qrModalVisible: false })}
                />
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({ state: state });
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(MapActivity);