const rootReducer = (state={
    token: {},
    loading: true,
    error: null,
    },action)=>{

    switch(action.type){
        case "GET_TOKEN":
            return {...state, token: action.token};
        case "SAVE_TOKEN":
            return {...state, token: action.token};
        case "REMOVE_TOKEN":
            return {...state, token: action.token}; 
        case 'LOADING':
            return { ...state, loading: action.isLoading };
        case 'ERROR':
            return { ...state, error: action.error };           
        case "SET_LANGUAGE":
            return {...state, setLanguageFlag: action.payload};
        case "UPDATE_TASKS":
            return {...state, updateTasksFlag: action.payload};    

        default:
            return state;    
    }
}

export default rootReducer