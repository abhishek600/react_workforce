import AsyncStorage from '@react-native-community/async-storage';

export function setLanguage(data){
    return {
        type: 'SET_LANGUAGE',
        payload: data, 
    }
}

export function updateTasks(data){
    return {
        type: 'UPDATE_TASKS',
        payload: data,
    }
}


export const getToken = (token) => ({
    type: 'GET_TOKEN',
    token,
});

export const saveToken = token => ({
    type: 'SAVE_TOKEN',
    token
});

export const removeToken = () => ({
    type: 'REMOVE_TOKEN',
});

export const loading = bool => ({
    type: 'LOADING',
    isLoading: bool,
});

export const error = error => ({
    type: 'ERROR',
    error,
});

export const getUserToken = () => dispatch => 
    AsyncStorage.getItem('workForceToken')
    .then((data) => {
        dispatch(loading(false));
        dispatch(getToken(data));
    })
    .catch((err) => {
        dispatch(loading(false));
        dispatch(error(err.message || 'ERROR'));
})

export const saveUserToken = (data) => dispatch =>
AsyncStorage.setItem('workForceToken', 'abc123')
    .then((data) => {
        dispatch(loading(false));
        dispatch(saveToken('token saved'));
    })
    .catch((err) => {
        dispatch(loading(false));
        dispatch(error(err.message || 'ERROR'));
})

export const removeUserToken = () => dispatch =>
AsyncStorage.removeItem('workForceToken')
    .then((data) => {
        dispatch(loading(false));
        dispatch(removeToken(data));
    })
    .catch((err) => {
        dispatch(loading(false));
        dispatch(error(err.message || 'ERROR'));
})