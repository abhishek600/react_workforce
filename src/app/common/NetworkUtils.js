import NetInfo from "@react-native-community/netinfo";


export default class NetworkUtils {

    static isNetworkAvailable() {
         return NetInfo.fetch().then(state => { 
             return state.isConnected 
            });
        
    }

}