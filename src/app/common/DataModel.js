export default class DataModel {

    static myInstance = null;

    qrCodeData = "";
    orderId = "";
    authToken = "";
    testToken = "";
    assignId = "";

    static getInstance() {
        if (DataModel.myInstance == null) {
            DataModel.myInstance = new DataModel();
        }

        return this.myInstance;
    }

    setQrCodeData(data) {
        this.qrCodeData = data;
    }

    getQrCodeData() {
        return this.qrCodeData;
    }

    setOrderId(data) {
        this.orderId = data;
    }

    getOrderId() {
        return this.orderId;
    }

    setAuthToken(token) {
        this.authToken = token;
    }

    getAuthToken() {
        return this.authToken;
    }

    setTestToken(token) {
        this.testToken = token;
    }

    getTestToken() {
        return this.testToken;
    }

    setAssignId(assignId) {
        this.assignId = assignId;
    }

    getAssignId() {
        return this.assignId;
    }

}