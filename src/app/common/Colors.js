const Colors = {
    appColor: '#e7601c',
    darkGray: '#808080',
    background: '#FAFAFA',
    gray: '#D7D7D7',
    lightGray: '#e7e7e7',
    fbButtonColor: '#4C7ABF',
    googleButtonColor: '#DF4930',
};

export default Colors