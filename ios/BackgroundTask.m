//
//  BackgroundTask.m
//  WorkForce
//
//  Created by abhishek on 28/09/20.
//

#import <Foundation/Foundation.h>

#import "BackgroundTask.h"
#import <React/RCTUtils.h>


/*
 Sample:
 
 BackgroundTask.beginBackgroundTask("gpsTracking", () => {})
 
 */

@implementation BackgroundTask


RCT_EXPORT_MODULE();

#pragma mark - Public API

RCT_EXPORT_METHOD(beginBackgroundTask:(NSString *)taskName jsCallback:(RCTResponseSenderBlock)jsCallback)
{
  UIApplication *application = RCTSharedApplication();
  
  __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithName:taskName expirationHandler:^{
    // Clean up any unfinished task business by marking where you
    // stopped or ending the task outright.
    [application endBackgroundTask:bgTask];
    bgTask = UIBackgroundTaskInvalid;
  }];
  
  // Start the long-running task and return immediately.
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    printf("Running in the background\n");
    // Call the JS code
    jsCallback(@[]);
    printf("Back from the JS\n");
    
    [application endBackgroundTask:bgTask];
    bgTask = UIBackgroundTaskInvalid;
  });
}

@end
