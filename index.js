/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Root from './src/app/main/Root';
import {name as appName} from './app.json';
//import RNFirebaseBackgroundMessage from './src/app/main/bgMessaging';

if (__DEV__) {
    import('./utils/ignoreWarning');
}

AppRegistry.registerComponent(appName, () => Root);
//AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => RNFirebaseBackgroundMessage);

